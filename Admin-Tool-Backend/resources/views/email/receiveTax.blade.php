@component('mail::message')
# WE'VE RECEIVED YOUR TAX EXEMPTION REQUEST!

<p><strong>Request ID</strong> #{{$details['requestId']}}</p>
<p><strong>Tracking Number</strong>: {{$details['trackingNumber']}}</p>
<hr>

Hi, {{$details['name']}},<br><br>
Your tax exemption request was submitted successfully
Your tracking number is <strong>{{$details['trackingNumber']}}</strong>.<br>
You can track your tax exemption at <a href="{{$details['trackingUrl']}}">Track My Tax Exemption</a><br><br>
Should you have any enquiries, kindly WhatsApp us at 011- 24407539 (Admin)

@component('mail::signature')
@endcomponent

@endcomponent
