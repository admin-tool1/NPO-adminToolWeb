@component('mail::message')
# Dear {{$details['name']}}
# Your tax exemption request has been rejected

<p><strong>Request ID</strong> #{{$details['requestId']}}</p>
<p><strong>Tracking Number</strong>: {{$details['trackingNumber']}}</p>
<hr>

We are sorry to announce that your tax exemption request is rejected due to {!! $details['reason'] !!}<br>
If you have further questions please contact us at 011-24407539 (Admin) or email us at cintasyriamalaysia@gmail.com

@component('mail::signature')
@endcomponent

@endcomponent
