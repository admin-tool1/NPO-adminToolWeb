<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Cinta Syria Malaysia')
<img src={{asset('/storage/header-csm.png')}} alt="Cinta Syria Malaysia Logo" style="max-width: 30%;">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
