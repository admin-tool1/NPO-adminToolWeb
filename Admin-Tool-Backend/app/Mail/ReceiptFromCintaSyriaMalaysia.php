<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReceiptFromCintaSyriaMalaysia extends Mailable
{
    use Queueable, SerializesModels;

    public $pathFile;
    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pathFile, $details)
    {
        $this->pathFile=$pathFile;
        $this->details=$details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.sendReceipt')
                    ->attach($this->pathFile, [
                                'as' =>  'ReceiptFromCSM.pdf'
                            ]);
    }
}
