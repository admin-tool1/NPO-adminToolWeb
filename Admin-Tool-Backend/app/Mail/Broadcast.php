<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Broadcast extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $pathFile;
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$pathFile,$details)
    {
        $this->name=$name;
        $this->pathFile=$pathFile;
        $this->details=$details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->markdown('email.broadcast');

        if($this->pathFile[0]!=null) {
            for($i=0;$i<count($this->pathFile);$i++){
                $email->attach($this->pathFile[$i]['getRealPath'], [
                    'as' => $this->pathFile[$i]['name'],
                ]);
            }
        }
        return $email;
    }
}
