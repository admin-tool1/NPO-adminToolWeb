<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsFaq extends Model
{
    protected $table = 'tax_exemptions_faqs';
    protected $primaryKey ='id';
    protected $fillable = [
        'question',
        'answer',
      ];
}
