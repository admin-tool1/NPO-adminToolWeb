<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsTrack extends Model
{
    protected $table = 'tax_exemptions_tracks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'trackingNumber',
        'taxId',
      ];    
}
