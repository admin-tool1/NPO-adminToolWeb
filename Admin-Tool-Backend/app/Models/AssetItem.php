<?php

namespace App\Models;

use App\Models\AssetLog;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use App\Models\AssetStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssetItem extends Model
{
    use HasFactory;

    protected $table = "asset_items";

    protected $fillable = [
        'status_id',
        'qrcode_id',
        'code',
        'year',
        'location',
        'remark',
        'detail_id',
        'item_count',
    ];

    // one item has ONLY ONE detail
    public function AssetDetail()
    {
        return $this->belongsTo(AssetDetail::class, 'detail_id');
    }

    // one item has ONLY ONE qr code
    public function AssetQrcode()
    {
        return $this->belongsTo(AssetQrcode::class, 'qrcode_id');
    }

    // one item has ONLY ONE qr code
    public function AssetStatus()
    {
        return $this->belongsTo(AssetStatus::class, 'status_id');
    }

    // one item has MANY logs
    public function AssetLog()
    {
        return $this->hasMany(AssetLog::class, 'item_id');
    }

    public static function getAssetItem()
    {
        $records = DB::table('asset_items')->select(
            // 'category_id',
            // 'status_id',
            // 'qrcode_id',
            'description',
            'year',
            'unit_price',
            'quantity',
            'brand',
            'location',
            'remark'
        )->get()->toArray();
        return $records;
    }
}
