<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsTerm extends Model
{
    protected $table = 'tax_exemptions_terms';
    protected $primaryKey ='id';
    protected $fillable = [
        'term',
      ];
}
