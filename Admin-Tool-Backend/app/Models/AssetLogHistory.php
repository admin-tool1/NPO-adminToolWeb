<?php

namespace App\Models;

use App\Models\AssetLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssetLogHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'remark',
        'log_id',
        'status_id',
    ];

    // one log history belongs to ONLY ONE log
    public function AssetLog()
    {
        return $this->belongsTo(AssetLog::class, 'log_id');
    }
}
