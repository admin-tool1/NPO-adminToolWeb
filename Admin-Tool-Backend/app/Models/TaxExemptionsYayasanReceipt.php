<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsYayasanReceipt extends Model
{
    protected $table = 'tax_exemptions_yayasan_receipts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'file_name',
        'file_path',
        'taxId',
      ];
}
