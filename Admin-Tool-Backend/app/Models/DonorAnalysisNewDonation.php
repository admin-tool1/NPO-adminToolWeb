<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisNewDonation extends Model
{
    protected $table = 'donor_analysis_new_donations';
    protected $primaryKey ='id';
    protected $fillable = [
        'donorId',
        'taxId',
      ];

      public function TaxExemption()
      {
          return $this->belongsTo(TaxExemption::class , 'taxId');
      }

      public function TaxExemptionsDonationReceipt()
      {
          return $this->hasMany(TaxExemptionsDonationReceipt::class , 'taxId');
      }
  
}
