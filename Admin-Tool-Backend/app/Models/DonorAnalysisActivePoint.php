<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisActivePoint extends Model
{
    protected $table = 'donor_analysis_active_points';
    protected $primaryKey ='id';
    protected $fillable = [
        'point',
        'group',
        'isActive',
      ];
}
