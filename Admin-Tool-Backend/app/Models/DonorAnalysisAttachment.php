<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisAttachment extends Model
{
    protected $table = 'donor_analysis_attachments';
    protected $primaryKey ='id';
    protected $fillable = [
        'filePath',
        'broadcastLogId'
      ];
}
