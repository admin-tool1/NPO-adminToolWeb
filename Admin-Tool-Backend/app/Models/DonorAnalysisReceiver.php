<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisReceiver extends Model
{
    protected $table = 'donor_analysis_receivers';
    protected $primaryKey ='id';
    protected $fillable = [
        'name',
        'email',
        'broadcastLogId',
      ];
}
