<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsPostage extends Model
{
    protected $table = 'tax_exemptions_postages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'postStatus'
      ];
}
