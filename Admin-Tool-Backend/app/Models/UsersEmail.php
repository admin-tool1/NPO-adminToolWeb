<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersEmail extends Model
{
    protected $table = 'users_emails';
    protected $primaryKey = 'id';
    protected $fillable = [
        'users_id',
        'email',
        'password',
      ];  
      
      public function User()
      {
          return $this->belongsTo(User::class , 'users_id');
      }
}
