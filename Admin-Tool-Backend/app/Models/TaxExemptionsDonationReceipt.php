<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsDonationReceipt extends Model
{ 
    protected $table = 'tax_exemptions_donation_receipts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'file_path',
        'amount',
        'taxId',
        'references',
      ];
}
