<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisRfmPoint extends Model
{
    protected $table = 'donor_analysis_rfm_points';
    protected $primaryKey ='id';
    protected $fillable = [
        'minimumMonth',
        'minimumFrequently',
        'minimumMonetary',
        'mark',
      ];
}
