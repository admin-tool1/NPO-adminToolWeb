<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisMonthState extends Model
{
    protected $table = 'donor_analysis_month_states';
    protected $primaryKey ='id';
    protected $fillable = [
        'stateId',
        'quantity',
      ];

    public function AddressesState()
    {
        return $this->belongsTo(AddressesState::class , 'stateId');
    }
}
