<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'category',
        'prefix',
        'department_id',
    ];

    // one category belongs to ONE department only
    public function AssetDepartment()
    {
        return $this->belongsTo(AssetDepartment::class, 'department_id');
    }
}
