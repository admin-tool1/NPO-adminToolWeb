<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisActiveDonor extends Model
{
    protected $table = 'donor_analysis_active_donors';
    protected $primaryKey ='id';
    protected $fillable = [
        'donorId',
        'totalDaysFromLastUpdated',
        'dateLastUpdateAsDonation',
        'frequently',
        'monetary',
        'recentlyPoint',
        'frequentlyPoint',
        'monetaryPoint',
        'activeGroup',
        'totalMark',
      ];
}
