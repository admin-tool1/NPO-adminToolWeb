<?php

namespace App\Exports;

use App\Models\TaxExemption;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class TaxExemptionsMultisheetsExportExcel implements  WithMultipleSheets
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $totalIdPerSheets;
    private $from;
    private $to;

    public function __construct(Array $totalIdPerSheets)
    {
        $this->totalIdPerSheets = $totalIdPerSheets;
    }

    public function sheets(): array
    {
        for ($currentRow =0; $currentRow < count( $this->totalIdPerSheets); $currentRow++)
        {
            $sheets[] = new  TaxExemptionsExportExcel($this->totalIdPerSheets[$currentRow]);
        }
        return $sheets;
    }
}
