<?php

namespace App\Exports;

use App\Models\TaxExemption;
use App\Models\Addresses;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use \Maatwebsite\Excel\Sheet;

class TaxExemptionsExportPDF implements FromView,WithEvents
{
    private $totalPDF;

    public function __construct(Array $totalPDF)
    {
        $this->totalPDF = $totalPDF;
    }

    public function view(): View
    {       

        return view('exports.taxExemptionPDF', [
            'taxs' => TaxExemption::with('TaxExemptionsDonationReceipt','TaxExemptionsDonor')
            ->whereIn('tax_exemptions.id', $this->totalPDF)
            ->join('addresses', 'addresses.id', '=' , 'tax_exemptions.addressId')
            ->join('addresses_states', 'addresses_states.id', '=' , 'addresses.stateId')
            ->select('tax_exemptions.*',
            'addresses.address',
            'addresses.postalCode',
            'addresses.city',
            'addresses_states.state',)
            ->get()
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getPageSetup()
                ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

                $event->sheet->getDelegate()->getStyle('A:G')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->sheet->getDelegate()->getStyle('A:G')->getAlignment()->setIndent(1.5);

                $event->sheet->getDelegate()->getStyle('A1:G1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(18);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(25);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(14);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(8);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(8);

                // // Set A1:D4 range to wrap text in cells
                $event->sheet->getDelegate()->getStyle('B:J')->getAlignment()->setWrapText(true);
            }
        ];
    }
}
