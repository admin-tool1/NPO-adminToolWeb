<?php

namespace App\Exports;

use App\Models\TaxExemption;
use App\Models\Addresses;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use \Maatwebsite\Excel\Sheet;

class ContentBroadcastPDF implements FromView
{
    private $content;

    public function __construct(String $content)
    {
        $this->content = $content;
    }

    public function view(): View
    {       

        return view('exports.contentBroadcastPDF', [
            'content' => $this->content
        ]);
    }
}
