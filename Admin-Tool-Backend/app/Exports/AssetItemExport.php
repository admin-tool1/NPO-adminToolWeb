<?php

namespace App\Exports;

// use App\Models\AssetItem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetItemExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'department',
            'category',
            'description',
            'year',
            'quantity',
            'unit_price',
            'brand',
            'location',
            'remark',
            'single_use',
        ];
    }

    public function collection()
    {
        // return AssetItem::all();
        return collect();
    }
}
