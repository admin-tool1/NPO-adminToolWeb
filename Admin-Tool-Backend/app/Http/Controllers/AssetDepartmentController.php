<?php

namespace App\Http\Controllers;

use App\Models\AssetItem;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use App\Models\AssetCategory;
use App\Models\AssetDepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AssetDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $withCategory = AssetDepartment::with('AssetCategory')->orderBy('department', 'asc')->get();
        return $withCategory;
        // return AssetDepartment::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'department' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            AssetDepartment::create($request->all());
            return response()->json(['status' => 200, 'message' => 'Department added successfully']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return AssetDepartment::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'department' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $department = AssetDepartment::find($id);
            $department->update($request->all());
            return response()->json(['status' => 200, 'message' => 'Department updated successfully']);
        }
        // return $department;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $src_arr = array();
        $categories = AssetCategory::where('department_id', $id)->get();
        if (count($categories) != 0) {
            foreach ($categories as $category) {
                $details = AssetDetail::where('category_id', $category->id)->get();
                foreach ($details as $detail) {
                    $assets = AssetItem::where('detail_id', $detail->id)->get();
                    foreach ($assets as $asset) {
                        $file_name = $asset->AssetQrcode->file_name;
                        $path = 'public/qrcodes/' . $file_name;

                        if (Storage::exists($path)) {
                            array_push($src_arr, $asset->AssetQrcode);
                        }
                    }
                }
            }
        }
        $res = AssetDepartment::destroy($id);
        foreach ($src_arr as $src) {
            AssetQrcode::destroy($src->id);
            Storage::delete('public/qrcodes/' . $src->file_name);
        }
        if ($res) {
            return response()->json(['message' => 'Department successfully deleted!'], 200);
        } else {
            return response()->json(['errors' => 'Department was not deleted. Try Again!'], 404);
        }
    }

    public function categoriesByDept($id)
    {
        return AssetCategory::orderBy('category', 'asc')->where('department_id', $id)->get();
    }
}
