<?php

namespace App\Http\Controllers;

use App\Models\AssetItem;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use App\Models\AssetCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;
use Illuminate\Support\Facades\Validator;

class AssetCategoryController extends Controller
{
    public function index()
    {
        return AssetCategory::orderBy('category', 'asc')->get();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'prefix' => 'required',
            'department_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            AssetCategory::create($request->all());
            return response()->json(['status' => 200, 'message' => 'Category added successfully']);
        }
    }

    public function show($id)
    {
        return AssetCategory::with('AssetDepartment')->get()->find($id);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'prefix' => 'required',
            'department_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $asset = AssetCategory::find($id);
            $asset->update($request->all());
            return response()->json(['status' => 200, 'message' => 'Category updated successfully']);
        }
    }

    public function destroy($id)
    {
        $src_arr = array();
        $details = AssetDetail::where('category_id', $id)->get();
        if (count($details) != 0) {
            foreach ($details as $detail) {
                $assets = AssetItem::where('detail_id', $detail->id)->get();
                foreach ($assets as $asset) {
                    $file_name = $asset->AssetQrcode->file_name;
                    $path = 'public/qrcodes/' . $file_name;

                    if (Storage::exists($path)) {
                        array_push($src_arr, $asset->AssetQrcode);
                    }
                }
            }
        }
        $res = AssetCategory::destroy($id);
        foreach ($src_arr as $src) {
            AssetQrcode::destroy($src->id);
            Storage::delete('public/qrcodes/' . $src->file_name);
        }
        if ($res) {
            return response()->json(['message' => 'Category successfully deleted!'], 200);
        } else {
            return response()->json(['errors' => 'Category was not deleted. Try Again!'], 404);
        }
    }

    public function search($name)
    {
        return AssetCategory::where('name', 'like', '%' . $name . '%')->get();
    }

    public function getDetails()
    {
        $asset = AssetCategory::all();
        $category = $asset->category_id;
        return $category;
    }

    public function acronym(Request $request)
    {
        $request->validate([
            'category' => 'required',
        ]);
        $acronym = "";
        if (str_word_count($request->category) == 1) {
            // Create acronym from first 3 letters
            $acronym = substr($request->category, 0, 3);
        } else {
            // Create acronym from first letter of a phrase
            $acronym = implode('', array_diff_assoc(str_split(ucwords($request->category)), str_split(strtolower($request->category))));
        }

        // Random generate acronym
        if (AssetCategory::where('prefix', $acronym)->first()) {
            $acronym = "";
            // Remove whitespace then split the string into an array of characters.
            $string = str_replace(' ', '', $request->category);
            $chars = str_split($string, 1);
            $length = count($chars);
            $used = array();
            for ($i = 0; $i < 3; $i++) {
                $index = rand(0, $length - 1);
                if (!in_array($index, $used)) {
                    $acronym .= $chars[$index];
                    array_push($used, $index);
                }
            }
        }
        return strtoupper($acronym);
    }
}
