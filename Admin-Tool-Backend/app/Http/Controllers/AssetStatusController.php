<?php

namespace App\Http\Controllers;

use App\Models\AssetItem;
use App\Models\AssetStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AssetStatusController extends Controller
{
    public function index()
    {
        return AssetStatus::with('StatusColor')->get();
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'status'  => 'required',
            'color_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            AssetStatus::create($request->all());
            return response()->json(['status' => 200, 'message' => 'Asset status created successfully!']);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status'  => 'required',
            'color_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $res = AssetStatus::find($id);
            $res->update($request->all());
            return response()->json(['status' => 200, 'message' => 'Asset status updated successfully!']);
        }
    }

    public function destroy($id)
    {
        //  status = "Available"
        if ($id == 1) {
            return response()->json(['errors' => 'The status cannot be deleted because it is a default value for available item(s)'], 400);
        }
        //  status = "In Use"
        if ($id == 2) {
            return response()->json(['errors' => 'The status cannot be deleted because it is a default value for item(s) in use'], 400);
        }
        //  status = "Returned"
        if ($id == 3) {
            return response()->json(['errors' => 'The status cannot be deleted because it is a default value for returned item(s)'], 400);
        }
        //  status = "Overdue"
        if ($id == 4) {
            return response()->json(['errors' => 'The status cannot be deleted because it is a default value for overdue item(s)'], 400);
        }
        //  status = "Missing"
        if ($id == 5) {
            return response()->json(['errors' => 'The status cannot be deleted because it is a default value for missing item(s)'], 400);
        }
        //  status = "Used"
        if ($id == 6) {
            return response()->json(['errors' => 'The status cannot be deleted because it is a default value for single-use item(s)'], 400);
        }
        if ($id != 1 || $id != 2 || $id != 3 || $id != 4 || $id != 5) {
            if (AssetItem::where('status_id', $id)->first()) {
                return response()->json(['errors' => 'The status cannot be deleted because it is being used by item(s)'], 400);
            }
        }
        return AssetStatus::destroy($id);
    }
}
