<?php

namespace App\Http\Controllers;
use App\Models\TaxExemptionsStatus;
use App\Models\TaxExemption;

use Illuminate\Http\Request;

class TaxExemptionsStatusController extends Controller
{
    public function index(){

        $Statuses = TaxExemptionsStatus::with('StatusColor')->get();

        for($i=0; $i<count($Statuses);$i++){
            $status[$i]["No"] = $i+1;
            $status[$i]["Id"] = $Statuses[$i]->id;
            $status[$i]["Status"] = $Statuses[$i]->status;
            $status[$i]["Color"] = $Statuses[$i]->StatusColor->color;
            $status[$i]["ColorId"] = $Statuses[$i]->StatusColor->id;
        }

        return $response = [
            "status" => $status,
        ];
        
    }

    public function update(Request $request, $id){
        $fields = $request->validate([
            'status'  =>'required',
            'colorId' => 'required',
        ]);       
        $color = TaxExemptionsStatus::find($id);
        $color->status = $fields['status'];
        $color->save();
        $color->colorId = $fields['colorId'];
        $color->save();

        return $color;

    }

    public function add(Request $request){
        $fields = $request->validate([
            'status'  =>'required',
            'colorId' => 'required',
        ]);        
        
        $data = TaxExemptionsStatus::create($request->all());
        return $data;
    }

    public function delete($id) {
        
        //couldnot delete statusId ($id) == 1 as it is a default value when request tax and also exporting PDF
        if($id == 1){
            return response()->json(['error' => 'The status could not be deleted because it default value for request tax exemption and export PDF'], 401);
        }

        //couldnot delete status Id == 3, as it is a default value for sendReceiptYayasan tax when decline tax
        if($id == 3){
            return response()->json(['error' => 'The status could not be deleted because it default value for send receipt from Yayasan'], 401);
        }

        //couldnot delete status Id == 4, as it is a default value for rejected tax when decline tax
        if($id == 4){
            return response()->json(['error' => 'The status could not be deleted because it default value for decline tax exemption'], 401);
        }

        //couldnot delete status Id == 5,6,7, as it is a default value for receipt postage payment
        if($id == 5 || $id == 6 || $id == 7) {
            return response()->json(['error' => 'The status could not be deleted because it default value for postage payment receipt status'], 401);
        }

        else if($tax = TaxExemption::where('statusId', $id)->first()){
            return response()->json(['error' => 'The status could not be deleted because it being use in the tax list'], 401);
        }
        
        return TaxExemptionsStatus::destroy($id);
    }

    public function loadStatus(){
        $Statuses = TaxExemptionsStatus::with('StatusColor')->get();
        // $wordCount = Wordlist::where('id', '<=', $correctedComparisons)->count()
        for($i=0; $i<4;$i++) {
            $Status[$i]["status"] = $Statuses[$i]->status;
            $Status[$i]["color"] = $Statuses[$i]->StatusColor->color;
            $Status[$i]["count"] = TaxExemption::where("statusId",$Statuses[$i]->id)->count();
        }
        return $response =[
            "status" => $Status
        ];
    }
}
