<?php

namespace App\Http\Controllers;
use App\Models\OfficialLettersType;

use Illuminate\Http\Request;

class OfficialLettersTypeController extends Controller
{
    public function index(){
        
        $data = OfficialLettersType::select('official_letters_types.id', 'official_letters_types.type')->get();
        return $response = [
            'types'=>$data,
        ];
    }
}
