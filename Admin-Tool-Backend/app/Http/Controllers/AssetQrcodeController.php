<?php

namespace App\Http\Controllers;

use App\Models\AssetQrcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AssetQrcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AssetQrcode::all();
    }

    /**
     * Generate QR code for the specified resource.
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        // $request->validate([
        //     'year' => 'required',
        // ]);
        $prefix = 'TEST';
        $number = str_pad(99, 3, '0', STR_PAD_LEFT);
        $fullCode = 'CSM-' . $prefix . '-' . 2022 . '-' . $number . '-9';
        $file_name = $fullCode . '.png';
        $arr = array('full_code' => $fullCode);
        $content = json_encode($arr);
        $destinationPath = Storage::path('public/qrcodes/' . $file_name);
        QrCode::format('png')
            ->size(400)
            ->generate($content, $destinationPath);
        $file_path = env('APP_URL') . Storage::url('qrcodes/' . $file_name);
        $data = ['file_name' => $file_name, 'file_path' => $file_path];

        /* Store $file_name name in DATABASE from HERE*/
        $new = AssetQrcode::create($data);

        return $new->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return AssetQrcode::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $qrcode = AssetQrcode::find($id);
        $qrcode->update($request->all());
        return $qrcode;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file_name = AssetQrcode::find($id)->file_name;
        $src = 'public/qrcodes/' . $file_name;
        if (Storage::exists($src)) {
            AssetQrcode::destroy($id);
            Storage::delete($src);
            return response("\n deleted", 200);
        } else {
            return response('failed', 404);
        }
    }

    public function download($id)
    {
        $file_name = AssetQrcode::find($id)->file_name;
        $file = Storage::disk('public')->get("qrcodes\\" . $file_name);

        return response($file, 200, ['Content-Type' => 'image/png', 'Content-Disposition' => 'attachment; filename="' . $file_name . '"']);
    }
}
