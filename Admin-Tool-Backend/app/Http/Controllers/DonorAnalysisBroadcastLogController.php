<?php

namespace App\Http\Controllers;
use App\Mail\Broadcast;
use App\Models\DonorAnalysisBroadcastLog;
use App\Models\DonorAnalysisAttachment;
use App\Models\DonorAnalysisContentFile;
use App\Models\DonorAnalysisReceiver;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ContentBroadcastPDF;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Http\Request;

class DonorAnalysisBroadcastLogController extends Controller
{
    public function index(){
        $data = DonorAnalysisBroadcastLog::with("DonorAnalysisReceiver", "DonorAnalysisContentFile", "DonorAnalysisAttachment")
                ->get();

        $broadcastLog=null;
        if(count($data)>0){
            for($i=0; $i<count($data);$i++){
                $broadcastLog[$i]["No"] = $i+1;
                $broadcastLog[$i]["Id"] = $data[$i]["id"];
                $broadcastLog[$i]["Type"] = $data[$i]["type"];
                $broadcastLog[$i]["SentDate"] = Carbon::parse($data[$i]->created_at)->format('d-m-Y');
                $broadcastLog[$i]["Content"] = asset($data[$i]->DonorAnalysisContentFile->filePath);

                $receiver = $data[$i]->DonorAnalysisReceiver;
                
                for($j=0;$j<count($receiver);$j++){
                    $broadcastLog[$i]["Receiver"][$j]["No"] =$j+1;
                    $broadcastLog[$i]["Receiver"][$j]["Name"] = $receiver[$j]->name;
                    $broadcastLog[$i]["Receiver"][$j]["Email"] = $receiver[$j]->email;
                }

                $attachs = $data[$i]->DonorAnalysisAttachment;

                for($k=0;$k<count($attachs);$k++){
                    $broadcastLog[$i]["Attachment"][$k]["url"] = asset($attachs[$k]->filePath);
                }
                
            }
        }

        return $response =[
            "broadcast" => $broadcastLog
        ];
    }

    public function sendBroadcastEmail(Request $request){
        $fields = $request->validate([
            'content' => 'string|required',
            'filePath' => 'required',
            'email' =>'required',
            'name' =>'required',
            'type' => 'string|required'
        ]);

        $result = DB::transaction (function() use ($fields , $request){

            if($contains = Str::contains($request->type, 'best')){
                $type="best donor";
            }
    
            else if($contains = Str::contains($request->type, 'high')){
                $type="high monetary";
            }
    
            else if($contains = Str::contains($request->type, 'low')){
                $type="low monetary";
            }

            $broadcastId = DB::table('donor_analysis_broadcast_logs')->insertGetId([
                "type"=>$type,
                "created_at" => Carbon::now(),
            ]);

            $fileName = 'broadcastContent_'.time().'.pdf';
            $filePath = '/storage/broadcast-content/' . $fileName;
            $data = Excel::store(new ContentBroadcastPDF($fields['content']), $fileName , 'broadcast', \Maatwebsite\Excel\Excel::DOMPDF );

            DonorAnalysisContentFile::create([
                "filePath"=>$filePath,
                "broadcastLogId"=>$broadcastId,
            ]);

            $sendFile[] = null;
            if($request->filePath !=null) {
                $files = $request->filePath;
                for($i=0;$i<count($files);$i++){
                    $sendFile[$i]['getRealPath'] = $files[$i]->getRealPath();
                    $sendFile[$i]['name'] = $files[$i]->getClientOriginalName();
                    $attachementName = time().'_'.$files[$i]->getClientOriginalName();
                    $files[$i]->storeAs('public/attach-broadcast', $attachementName);
                    $attachPath = '/storage/attach-broadcast/' . $attachementName; 
    
                    DonorAnalysisAttachment::create([
                        "filePath" => $attachPath,
                        "broadcastLogId" => $broadcastId,
                        "created_at"=> Carbon::now(),
                    ]);
                };
            }
           

            $emails=$request->email;
            $names=$request->name;

            for($i = 0 ; $i<count($emails);$i++){
                $details =[
                    'content' => $fields['content'],
                    'name'=>$names[$i],
                ];


                DonorAnalysisReceiver::create([
                    "name"=>$names[$i],
                    "email"=>$emails[$i],
                    "broadcastLogId" => $broadcastId,
                    "created_at"=> Carbon::now(),
                ]);
                Mail::to($emails[$i])->send(new Broadcast($names[$i],$sendFile, $details));

            }
            
            return "Succesfully Email";
        });
    
        return $response = [
            'message' =>$result,
        ];
    }
}
