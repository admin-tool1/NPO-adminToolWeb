<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaxExemptionsPostage;
use App\Models\TaxExemptionsPostageReceipt;
use App\Models\TaxExemptionsTrackHistory;

class TaxExemptionsPostageController extends Controller
{
    public function index(){
        
        $data = TaxExemptionsPostage::all();
        return $data;
    }

    public function uploadPostageReceipt(Request $request){
            
        $postageFile = $request->postagePath;
        $postageFileName  = $postageFile->getClientOriginalName();
        // // $extension = $file->getClientOriginalExtension();
        $postageReceipt = time().'_'.$postageFileName;  
        $postageFile->storeAs('public/donor-receipt', $postageReceipt);
        $postageFilePath = '/storage/donor-receipt/' . $postageReceipt;

        $data = TaxExemptionsPostageReceipt::where('taxId',$request->taxId)->first();
        $data->file_path=$postageFilePath;
        $data->statusId= 7;
        $data->save();

        TaxExemptionsTrackHistory::create([
            'remark' => "Your postage payment receipt has been received.",
            'taxId'=> $request->taxId,
            'statusId'=> $data->statusId,
        ]);
        return $data;
    }

    public function validation(Request $request, $id){

        $fields = $request->validate([
            'status' => 'required',
        ]);

        $data = TaxExemptionsPostageReceipt::where('taxId',$id)->first();
        if($fields['status']=="true"){
            $data->isApprove =1;
            $data->statusId= 6;

            TaxExemptionsTrackHistory::create([
                'remark' => "The payment of postage has been approved",
                'taxId'=> $id,
                'statusId'=> $data->statusId,
            ]);
        }
        else {
            $data->isApprove=0;
            $data->statusId=5;
            $data->file_path=null;
            
            TaxExemptionsTrackHistory::create([
                'remark' => "The payment of postage is not approved. Please upload a new receipt",
                'taxId'=> $id,
                'statusId'=> $data->statusId,
            ]);

        }
       
        $data->save();

        return $data;
    }
}
