<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\TaxExemptionsType;


class TaxExemptionsTypeController extends Controller
{
    public function index(){

        $data = TaxExemptionsType::all();

        return response($data, 201);
    }
}
