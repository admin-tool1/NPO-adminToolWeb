<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddressesState;

class AddressesStateController extends Controller
{
    public function index(){

        $data = AddressesState::all();
        
        return $data;
    }

    public function addState(Request $Request){

        $fields = $request->validate([
        'state' => 'string|required',
        ]);
        
        $data = AddressesState::create([
        'state' => $fields['state'],
        ]);
        
        return $data;
    }
}
