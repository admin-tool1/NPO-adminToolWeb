<?php

namespace App\Http\Controllers;

use App\Mail\AssetPenaltyNotice;
use Carbon\Carbon;
use App\Models\User;
use App\Models\AssetLog;
use App\Models\AssetItem;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use App\Models\AssetStatus;
use App\Models\AssetLogHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AssetLogController extends Controller
{
    public function index()
    {
        $logs = AssetLog::all();
        $all = array();
        foreach ($logs as $log) {
            $staff = User::find($log->staff_id);
            $item = AssetItem::find($log->item_id);
            $detail = AssetDetail::find($item->detail_id);
            $array = [
                'staff_name' => $staff->name,
                'asset_item' => $detail->description,
                'single_use' => $detail->single_use,
                'purpose' => $log->purpose,
                'requested_at' => $log->requested_at,
                'requested_qty' => $log->requested_qty,
                'returned_at' => $log->returned_at,
                'returned_qty' => $log->returned_qty,
            ];
            array_push($all, $array);
        }
        return $all;
    }

    public function request(Request $request)
    {
        $currentUser = auth()->user();
        $hasMultipleUse = false;

        foreach ($request->items as $item) {
            $asset = AssetItem::with('AssetDetail')->where('code', $item['item_code'])->first();
            $detail = $asset->AssetDetail;
            if ($detail->single_use === 0) {
                $hasMultipleUse = true;
            }
        }

        if ($hasMultipleUse == true) {
            $validator = Validator::make($request->all(), [
                'items' => 'required', // an array, each item contains item_code, requested_qty
                'purpose' => 'required',
                'returned_at' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'items' => 'required', // an array, each item contains item_code, requested_qty
                'purpose' => 'required',
            ]);
        }

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            foreach ($request->items as $item) {
                $code = $item['item_code'];
                $requested_qty = $item['requested_qty'];

                $asset = AssetItem::with('AssetDetail')->where('code', $code)->first();

                /** GET DETAIL INFORMATION TO DETERMINE TYPE OF USE */
                $detail = $asset->AssetDetail;

                $data = [];
                /** SINGLE USE ITEM REQUEST */
                if ($detail->single_use === 1) {
                    if ($requested_qty <= $detail->total_quantity) {
                        /** Asset Details Table */
                        $t_qty = $detail->total_quantity - $requested_qty;
                        $detail->update(['total_quantity' => $t_qty]);

                        /** Asset Items Table */
                        // if ($t_qty < $detail->total_quantity) {
                        //     AssetItem::where('id', $asset->id)->update(array('status_id' => 2));
                        // }
                    }
                    $data = [
                        'staff_id' => $currentUser->id,
                        'item_id' => $asset->id,
                        'purpose' => $request->purpose,
                        'requested_qty' => $requested_qty,
                        'requested_at' => Carbon::now()->toDateString(),
                    ];
                    $log = AssetLog::create($data);
                    AssetLogHistory::create([
                        'remark' =>   'Quantity of ' . $requested_qty . ' used ' . $currentUser->name,
                        'log_id' => $log->id,
                        'status_id' => 6,
                    ]);
                }

                /** MULTIPLE USE ITEM REQUEST */
                else {
                    AssetItem::where('id', $asset->id)->update(array('status_id' => 2));
                    $data = [
                        'staff_id' => $currentUser->id,
                        'item_id' => $asset->id,
                        'purpose' => $request->purpose,
                        'requested_qty' => $requested_qty,
                        'requested_at' => Carbon::now()->toDateString(),
                        'returned_at' => $request->returned_at,
                    ];
                    $log = AssetLog::create($data);
                    AssetLogHistory::create([
                        'remark' => 'Item borrowed by ' . $currentUser->name,
                        'log_id' => $log->id,
                        'status_id' => 2,
                    ]);
                }
            }
            return response()->json(['status' => 200, 'message' => 'Request logged successfully']);
        }
    }

    public function return(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'log_ids' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $quantity_to_return = 1;

        foreach ($request->log_ids as $id) {
            $log = AssetLog::with('AssetItem')->findOrFail($id);

            $log->AssetItem->update(array('status_id' => 3));

            $log->update([
                'returned_qty' => $quantity_to_return,
                'returned_at' => Carbon::now()->toDateString()
            ]);

            AssetLogHistory::create([
                'remark' => 'Item returned by ' . auth()->user()->name,
                'log_id' => $id,
                'status_id' => 3,
            ]);
        }
            return response()->json(['status' => 200, 'message' => 'Return logged successfully']);
        }
    }

    public function show($code)
    {
        $asset = AssetItem::where('code', $code)->first();
        $currentUser = auth()->user();
        $logs = AssetLog::where([
            ['item_id', '=', $asset->id],
            ['staff_id', '=', $currentUser->id],
            ['returned_qty', '=', null]
        ])->get();
        $res = array();
        foreach ($logs as $log) {
            $array = [
                'log_id' => $log->id,
                'item_id' => $asset->id,
                'item_code' => $asset->code,
                'purpose' => $log->purpose,
                'requested_at' => $log->requested_at,
                'requested_qty' => $log->requested_qty,
                'returned_at' => $log->returned_at,
                'returned_qty' => $log->returned_qty,
            ];
            array_push($res, $array);
        }
        return $res;
    }

    public function showByStaff()
    {
        $currentUser = auth()->user();
        $logs = AssetLog::with('AssetLogHistory')->where([['staff_id', '=', $currentUser->id]])->orderBy('id', 'desc')->get();
        $res = array();
        foreach ($logs as $log) {
            $asset = AssetItem::with('AssetDetail')->where('id', $log->item_id)->first();
            $detail = $asset->AssetDetail;
            $history = $log->AssetLogHistory->last();
            $status = "";
            $remark = "";
            $color_id = "";
            if ($history) {
                $status_res = AssetStatus::find($history->status_id);
                $remark = $history->remark;
                $color_id = $status_res->color_id;
                $status = $status_res->status;
            }
            $array = [
                'log_id' => $log->id,
                'item_id' => $asset->id,
                'item_code' => $asset->code,
                'purpose' => $log->purpose,
                'requested_at' => $log->requested_at,
                'requested_qty' => $log->requested_qty,
                'returned_at' => $log->returned_at,
                'returned_qty' => $log->returned_qty,
                'description' => $detail->description,
                'single_use' => $detail->single_use,
                'remark' => $remark,
                'color_id' => $color_id,
                'status' => $status,
            ];
            array_push($res, $array);
        }
        return $res;
    }

    public function showToReturn()
    {
        $currentUser = auth()->user();
        $logs = AssetLog::with('AssetLogHistory')->where('staff_id', $currentUser->id)->orderBy('returned_at', 'asc')->get();

        $res = array();
        foreach ($logs as $log) {
            $item = AssetItem::find($log->item_id);
            $detail = AssetDetail::find($item->detail_id);
            $history = $log->AssetLogHistory->last();
            if ($detail->single_use == false) {
                if ($log->returned_qty == 0) {
                    $array = [
                        // 'staff_name' => $staff->name,
                        'item_description' => $detail->description,
                        'item_code' => $item->code,
                        'purpose' => $log->purpose,
                        'requested_at' => $log->requested_at,
                        // 'requested_qty' => $log->requested_qty,
                        'returned_at' => $log->returned_at,
                        // 'returned_qty' => $log->returned_qty,
                        'status_id' => $item->status_id,
                        'remark' => $history->remark
                    ];
                    array_push($res, $array);
                }
            }
        }
        return $res;
    }

    public function update(Request $request, $id)
    {
        $log = AssetLog::find($id);
        $log->update($request->all());
        return $log;
    }

    public function checkDue()
    {
        $logs = AssetLog::with('AssetItem', 'AssetLogHistory')->whereNotNull('returned_at',)->orderBy('returned_at', 'desc')->get();

        /** check status */
        foreach ($logs as $log) {

            /** check date is past and no returned qty */
            if ($log->returned_at < today() && $log->returned_qty == null) {
                $item = $log->AssetItem;
                $history = $log->AssetLogHistory->last();

                /** check single _use or not  status == returned*/
                if ($item->status_id != 3 && $item->status_id != 4 && $item->status_id != 5) {

                    /** update status if not returned */
                    $item->update(array('status_id' => 4));
                    AssetLogHistory::create([
                        'remark' => 'Item is overdue',
                        'log_id' => $log->id,
                        'status_id' => 4,
                    ]);
                } else {
                    /** single_use, do nothing */
                }
            }
        }
    }

    public function checkLogStatus()
    {
        $this->checkDue();

        $logs = AssetLog::with('AssetItem', 'AssetLogHistory')->whereNotNull('returned_at',)->orderBy('returned_at', 'desc')->get();

        /** return logs with items returned and overdue */
        $res = array();
        foreach ($logs as $log) {
            $item = $log->AssetItem;
            $history = $log->AssetLogHistory->last();
            if ($history != null) {
                // || ($log->returned_at < today() && $log->returned_qty == null)
                if ($history->status_id == 3 || ($history->status_id == 4 && $history->remark == "Item is overdue") || ($history->status_id == 5 && $history->remark == "Item is missing")) {
                    $data = [
                        'log_id' => $log->id,
                        'staff_name' => User::find($log->staff_id)->name,
                        'item_id' => $item->id,
                        'item_code' => $item->code,
                        'purpose' => $log->purpose,
                        'requested_at' => $log->requested_at,
                        'requested_qty' => $log->requested_qty,
                        'returned_at' => $log->returned_at,
                        'returned_qty' => $log->returned_qty,
                        'status_id' => $item->status_id,
                    ];
                    array_push($res, $data);
                }
            }
        }
        return $res;
    }

    public function verifyReturnedItem(Request $request)
    {
        $request->validate([
            'log_id' => 'required',
        ]);
        $status = "";
        $log = AssetLog::with('AssetItem')->get()->find($request->log_id);
        $item = $log->AssetItem;
        $detail = AssetDetail::find($item->detail_id);

        if ($request->verify === true) {
            $status = 1;
            AssetLogHistory::create([
                'remark' => 'Returned item verified.',
                'log_id' => $request->log_id,
                'status_id' =>  $status,
            ]);
        } else {
            $status = 5;
            AssetLogHistory::create([
                'remark' => 'Item is missing',
                'log_id' => $request->log_id,
                'status_id' =>  5,
            ]);
            $updated_qty = $detail->total_quantity - 1;
            $detail->update(array('total_quantity' => $updated_qty));
        }

        $res = $item->update(array('status_id' => $status));
        return response()->json(['message' => $res], 200);
    }

    public function sendPenaltyNotice(Request $request)
    {
        $request->validate([
            'is_charged' => 'required',
            'log_id' => 'required',
        ]);

        $log = AssetLog::with('AssetItem', 'User')->find($request->log_id);
        $item = $log->AssetItem;

        $remark = '';

        if ($request->is_charged === true) {
            // $staff_email = User::where('name', $request->item['StaffName'])->first()->email;
            // Mail::to($staff_email)->send(new AssetPenaltyNotice($details));
            // AssetLogHistory::create([
            //     'remark' => 'Penalty notice sent to ' . $request->item['StaffName'],
            //     'log_id' => $request->item['LogId'],
            //     'status_id' => $request->item['StatusId'],
            // ]);
            // return response()->json(["message" => true], 200);
            $log->update(['returned_at' => null, 'returned_qty' => null]);
            $remark = 'To be charged';
        } else if ($request->is_charged === false) {
            $remark = 'No charges';
        }
        $res = AssetLogHistory::create([
            'remark' => $remark,
            'log_id' => $request->log_id,
            'status_id' =>  $item->status_id,
        ]);
        // $item->update(array('remark' => $remark));
        if ($res) {
            return response()->json(["message" => "Succesfully send notice"], 200);
        } else {
            return response()->json(["message" => "Send notice unsuccesful"], 404);
        }
    }

    public function destroy($id)
    {
        return AssetLog::destroy($id);
    }

    public function stockPredictor()
    {
        /** Retrieve start and end date for last 12 months  */
        function getDateRangeForLast12Months()
        {
            /** first and last data for last 12 months */
            $firstDateForLast12Months = today()->startOfMonth()->subMonthsNoOverflow(12)->format('Y-m-d');
            $lastDateForLast12Months = today()->subMonthsNoOverflow(1)->endOfMonth()->format('Y-m-d');

            return array($firstDateForLast12Months, $lastDateForLast12Months);
        }

        /** Get logs within daterange*/
        function getLogsForLast12Months($dateRange)
        {
            $logs = AssetLog::whereBetween('requested_at', $dateRange)->get();
            return $logs;
        }

        /** Get asset details and items */
        function getDetailsAndItems()
        {
            $result = array();
            $assets = AssetDetail::all();

            for ($i = 0; $i < count($assets); $i++) {
                $item_ids = AssetItem::where('detail_id', $assets[$i]->id)->select('id')->get();
                $items = array();
                foreach ($item_ids as $item_id) {
                    array_push($items, $item_id->id);
                }
                array_push($result, array(
                    "id" => $assets[$i]->id,
                    // "min_quantity" => $assets[$i]->min_quantity,
                    // "single_use" => $assets[$i]->single_use,
                    "items" => $items
                ));
            };

            return $result;
        }

        /** Calculate total of quantity used within dateRange for each asset item */
        function averageUsagePerMonth($dateRange, $logs, $item)
        {
            /** last 12 months */
            $startDate = $dateRange[0];
            $endDate = $dateRange[1];
            $sum =  0;

            foreach ($logs as $log) {
                if ($item == $log->item_id) {
                    $requestedDate = $log->requested_at;
                    if (($requestedDate >= $startDate) && ($requestedDate <= $endDate)) {
                        // print  "log: " . $log->requested_qty . "\n";
                        $sum = $sum + $log->requested_qty;
                        // print  "sum: " . $sum . "\n";
                    }
                }
            }

            $average = $sum / 12;
            // print "Average: " . $average . "\n";
            // print "Average: " . ceil($sum / 12) . "\n\n";
            return ceil($average);
        }



        function updateAssetsMinQty()
        {
            $dates = getDateRangeForLast12Months();
            $logs = getLogsForLast12Months($dates);
            $details = getDetailsAndItems();

            foreach ($details as $d) {
                // $detail = AssetDetail::where('id', $d['id'])->first();
                // $desc = $detail->description;
                // print "Description: " . $desc . "\n";

                $total_ave = 0;
                $final_ave = 0;
                // print "# item: " . count($d['items']) . "\n";
                foreach ($d['items'] as $item_id) {
                    // print "item: " . $item_id . "\n";
                    $ave = averageUsagePerMonth($dates, $logs, $item_id);
                    // print "ave: " . $ave . "\n";
                    $total_ave = $total_ave + $ave;
                }

                if (count($d['items'])) {
                    $final_ave = $total_ave / count($d['items']);
                }

                AssetDetail::where('id', $d['id'])->update(array('min_quantity' => $final_ave));

                // print "\n\n";
            }
        }
        return updateAssetsMinQty();
    }
}
