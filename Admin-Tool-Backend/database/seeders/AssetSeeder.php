<?php

namespace Database\Seeders;

// use Illuminate\Support\Str;
use App\Models\Asset;
use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $assets = [
            [
                'name' => 'Books', 
                'code' => 'BK001',
                'price' => rand(5,30),
                'category_id'=> 3,
                'status_id'=> 1, 
                'qrcode_id' => 1,
            ],
        ];
        foreach ($assets as $asset) {
            Asset::create($asset);
        }
        // DB::table('assets')->insert([
        //     'name' => Str::random(5),
        //     'slug' => Str::random(5),
        //     'price' => rand(5,30),
        // ]);
        // \App\Models\Asset::factory(1)->create();
    }
}
