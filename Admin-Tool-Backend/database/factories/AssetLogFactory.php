<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\AssetLog;
use App\Models\AssetItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssetLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AssetLog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $start = $this->faker->dateTimeBetween($startDate = '-1 year', $endDate = '-30 days');
        $reqDate = $this->faker->dateTimeBetween($start, $start->format('Y-m-d H:i:s') . '+1 days');
        $reqQty = $this->faker->unique(true)->numberBetween(10, 20);
        return [
            'staff_id' => 2,
            'item_id' => $this->faker->unique()->numberBetween($min = 105, $max = 107),
            'purpose' => $this->faker->text(),
            'requested_qty' => $reqQty,
            // 'returned_qty' => $retQty, 
            'requested_at' => $reqDate,
            // 'returned_at' => $endDate,

        ];
    }
}
