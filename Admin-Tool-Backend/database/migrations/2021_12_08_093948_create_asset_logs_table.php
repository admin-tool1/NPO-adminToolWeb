<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('staff_id');
            $table->unsignedBigInteger('item_id');
            $table->string('purpose');
            $table->integer('requested_qty');
            $table->integer('returned_qty')->nullable();
            $table->date('requested_at', $precision = 0);
            $table->date('returned_at', $precision = 0)->nullable();

            $table->foreign('staff_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('asset_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_logs');
    }
}
