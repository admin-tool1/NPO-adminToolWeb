<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorAnalysisMonthBestDonorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_analysis_month_best_donors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('activeDonorId');            
            $table->foreign('activeDonorId')->references('id')->on('donor_analysis_active_donors')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_analysis_month_best_donors');
    }
}
