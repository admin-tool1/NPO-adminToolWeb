<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorAnalysisNewDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_analysis_new_donations', function (Blueprint $table) {
            $table->id();
            $table->longText('donorId');
            $table->unsignedBigInteger('taxId');
            $table->foreign('taxId')->references('id')->on('tax_exemptions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_analysis_new_donations');
    }
}
