<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsStatus;

class CreateTaxExemptionsStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_statuses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('status');
            $table->unsignedBigInteger('colorId')->default(1);             
            $table->foreign('colorId')->references('id')->on('status_colors')->onDelete('cascade');
        });


        Schema::table('tax_exemptions_statuses', function (Blueprint $table) {
            $data = [
                ["status" => "New Request", "colorId" => 5],
                ["status" => "In Progress", "colorId" => 6],
                ["status" => "Completed", "colorId" => 3],
                ["status" => "Rejected", "colorId" => 4],
                ["status" => "Not Paid", "colorId" => 4],
                ["status" => "Paid", "colorId" => 3],
                ["status" => "Waiting Approval", "colorId" => 8],
            ];

            TaxExemptionsStatus::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_statuses');
    }
}
