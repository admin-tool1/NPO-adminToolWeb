<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsFaq;

class CreateTaxExemptionsFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_faqs', function (Blueprint $table) {
            $table->id();
            $table->string("question");
            $table->text("answer");
            $table->timestamps();
        });

        
        Schema::table('tax_exemptions_faqs', function (Blueprint $table) {
            $data = [
                [
                    "question" => "Can I request for a tax exemption receipt for my contribution?", 
                    "answer"=>"<p>Yes, you can. Provided that the minimum contribution must be RM 500.00</p>"
                ],
                [
                    "question" => "How do I apply for a tax exemption receipt?",
                    "answer"=>"<p>Application can be made through our official website: www.cintasyriamalaysia@gmail.com</p><p>Any inquiries you may contact us via WhatsApp at 011-24407539 (Admin)</p>"
                ],
                [
                    "question" => "Does CSM set a time period for a donor to apply a tax exemption receipt? If so, how long is the period?",
                    "answer"=>"<p>Yes, we do. Donor is advised to apply for a tax exemption receipt immediately after the donation is made or after the end of the campaign within the duration of 3 months</p>"
                ],
                [
                    "question" => "How long does it take for me to get the tax exemption receipt?",
                    "answer"=>"<p>The process takes one(1) month from the date of application</p>"
                ],
                [
                    "question" => "What is the method of sending the receipt?",
                    "answer"=>"<p>The receipt will be sent to the applicant by email or by post upon request with additional cost</p>"
                ],
                [
                    "question" => "How much is the postage charged to the applicant?",
                    "answer"=>"<p>The cost of posting is RM 5.00 and payment can be channeled to:</p><p>Persatuan Cinta Syria Malaysia</p><p>Bank Rakyat</p><p>11-394-100122-0</p>"
                ],
            ];

            TaxExemptionsFaq::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_faqs');
    }
}
