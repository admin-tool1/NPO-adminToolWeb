<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxExemptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->string('phoneNumber');
            $table->string('email');
            $table->double('amount',8,2);
            $table->unsignedBigInteger('typeId');
            $table->unsignedBigInteger('statusId');   
            $table->unsignedBigInteger('postId');
            $table->unsignedBigInteger('donorId');  
            $table->unsignedBigInteger('addressId');   
            $table->unsignedBigInteger('postageAddressId')->nullable()->constrained();
            $table->timestamps();           
            $table->foreign('typeId')->references('id')->on('tax_exemptions_types')->nullable()->constrained();
            $table->foreign('statusId')->references('id')->on('tax_exemptions_statuses')->constrained();
            $table->foreign('postId')->references('id')->on('tax_exemptions_postages')->nullable()->constrained();
            $table->foreign('donorId')->references('id')->on('tax_exemptions_donors')->constrained();
            $table->foreign('addressId')->references('id')->on('addresses')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions');
    }
}
