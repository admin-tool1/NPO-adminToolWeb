<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\OfficialLettersType;

class CreateOfficialLettersTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('official_letters_types', function (Blueprint $table) {
            $table->id();
            $table->String("type");
            $table->timestamps();
        });

        Schema::table('official_letters_types', function (Blueprint $table) {
            $data = [
                ["type" => "Surat Jemputan"],
                ["type" => "Surat Kebenaran Penggunaan Logo"],
                ["type" => "Surat Kebenaran Mohon Sumbangan"],
                ["type" => "Surat Kerjasama"],
                ["type" => "Surat Kunjungan Hormat"],
                ["type" => "Surat Panggilan AGM"],
                ["type" => "Surat Penerangan"],
                ["type" => "Surat Penghargaan"],
                ["type" => "Surat Permohonan"],
                ["type" => "Surat Permohonan Dana"],
                ["type" => "Surat Permohonan Jemputan Misi"],
                ["type" => "Surat Permohonan Program Bersama"],
                ["type" => "Surat Persetujuan"],
                ["type" => "Surat Pinjam Aset"],
                ["type" => "Surat Sokongan"],
                ["type" => "Surat Tajaan"],
                ["type" => "Surat Wakil"],
            ];

            OfficialLettersType::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('official_letters_types');
    }
}
