<?php

use App\Models\AssetStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->unsignedBigInteger('color_id')->nullable();
            $table->timestamps();

            $table->foreign('color_id')->references('id')->on('status_colors');
        });

        Schema::table('asset_statuses', function (Blueprint $table) {
            $data = [
                ['status' => 'Available', 'color_id' => 3],
                ['status' => 'In Use', 'color_id' => 5],
                ['status' => 'Returned', 'color_id' => 6],
                ['status' => 'Overdue', 'color_id' => 4],
                ['status' => 'Missing', 'color_id' => 4],
            ];
            AssetStatus::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_statuses');
    }
}
