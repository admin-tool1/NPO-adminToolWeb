<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxExemptionsYayasanReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_yayasan_receipts', function (Blueprint $table) {
            $table->id();
            $table->string('file_name');   
            $table->string('file_path');
            $table->unsignedBigInteger('taxId');
            $table->foreign('taxId')->references('id')->on('tax_exemptions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_yayasan_receipts');
    }
}
