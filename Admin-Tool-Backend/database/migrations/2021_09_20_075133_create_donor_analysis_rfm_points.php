<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\DonorAnalysisRfmPoint;
use Carbon\Carbon;

class CreateDonorAnalysisRfmPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_analysis_rfm_points', function (Blueprint $table) {
            $table->id();
            $table->integer('minimumMonth');
            $table->integer('minimumFrequently');
            $table->double('minimumMonetary',8,2);
            $table->integer('mark');
            $table->timestamps();
        });

        Schema::table('donor_analysis_rfm_points', function (Blueprint $table) {
            $data = [
                ["minimumMonth" => 1, "minimumFrequently" => 5, "minimumMonetary" => 800, "mark" => 100, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["minimumMonth" => 7, "minimumFrequently" => 4, "minimumMonetary" => 500, "mark" => 80, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["minimumMonth" => 13, "minimumFrequently"=> 3, "minimumMonetary" => 300, "mark" => 60, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["minimumMonth" => 25, "minimumFrequently"=> 2, "minimumMonetary" => 100, "mark" => 40, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["minimumMonth" => 49, "minimumFrequently"=> 1, "minimumMonetary" => 0, "mark" => 20, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
            ];

            DonorAnalysisRfmPoint::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_analysis_rfm_points');
    }
}
