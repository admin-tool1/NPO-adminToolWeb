// Javascript Version

// import React, { useRef, useEffect, Component } from "react";
import React, { Component } from "react";
import SunEditor from "suneditor-react";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File

class EmailForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <SunEditor
          onChange={this.props.handleContent}
          setContents=""
          setOptions={{
            height: 200,
            buttonList: [
              ["undo", "redo"],
              ["bold", "underline", "italic", "strike"],
              ["outdent", "indent"],
              ["align", "horizontalRule", "list"],
              ["link"],
              ["fullScreen", "codeView"],
              ["preview"],
            ],
          }}
        />
      </div>
    );
  }
}
export default EmailForm;
