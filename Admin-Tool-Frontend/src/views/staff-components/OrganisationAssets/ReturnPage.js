import React, { Component } from "react";
import { CRow, CCol } from "@coreui/react";
import List from "./ReturnList";
import Form from "./ReturnForm";

class ReturnAssets extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
      <CRow className="justify-content-center">
          <CCol xl={6}>
            <List />
          </CCol>
        </CRow>
        <CRow className="justify-content-center">
          <CCol xl={6}>
            <Form />
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default ReturnAssets;
