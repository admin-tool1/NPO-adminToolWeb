import { CBadge, CDataTable } from "@coreui/react";
import React, { Component } from "react";
import Loader from "src/containers/Loader";
import api from "src/services/api";

class ReturnList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      listRequested: [],
      listColor: [],
    };
  }

  componentDidMount() {
    this.getRequestedItem();
    this.loadColor();
  }

  /** get all item requested - history of request */

  getRequestedItem() {
    this.setState({ isLoading: true });
    api.get("/api/getStaffItem").then((res) => {
      this.setState({
        listRequested: res.data,
        isLoading: false,
      });
    });
  }

  loadColor() {
    api.get("/api/statusColor").then((res) => {
      this.setState({
        listColor: res.data,
      });
    });
  }

  render() {
    const { isLoading, listRequested } = this.state;
    const tableItems = [];
    const tableFields = [
      // { key: "No" },
      { key: "description" },
      { key: "item_code" },
      { key: "status" },
      { key: "remark" },
      // { key: "quantity" },
      // { key: "requested" },
      // { key: "returned" },
      // { key: "requested_qty" },
      { key: "date_requested" },
      { key: "date_returned" },
      { key: "purpose" },
      // { key: "returned_qty" },
    ];

    let number = 0;
    let statusName = "";
    let colorName = "";
    if (listRequested) {
      listRequested.forEach((item) => {
        number = number + 1;

        this.state.listColor.forEach((color) => {
          if (parseInt(item.color_id) === parseInt(color.id)) {
            statusName = item.status;
            colorName = color.color;
          }
        });

        tableItems.push({
          No: number,
          log_id: item.log_id,
          item_code: item.item_code,
          purpose: item.purpose,
          quantity: item.requested_qty,
          date_requested: item.requested_at,
          date_returned: item.returned_at,
          quantity_returned: item.returned_qty,
          description: item.description,
          single_use: item.single_use,
          statusName: statusName,
          colorName: colorName,
          remark: item.remark,
        });
        statusName = "";
        colorName = "";
      });
    }

    return (
      <div>
        <h4>Request History</h4>
        <hr />
        {isLoading === true && <Loader />}
        {isLoading === false && (
          <CDataTable
            items={tableItems}
            fields={tableFields}
            sorter
            hover
            pagination
            itemsPerPage={10}
            itemsPerPageSelect
            tableFilter
            scopedSlots={{
              date_returned: (item) => (
                <td
                  style={
                    parseInt(item.single_use) === 1
                      ? { color: "none" }
                      : item.quantity_returned === null
                      ? { color: "#db2127" }
                      : { color: "none" }
                  }
                >
                  {parseInt(item.single_use) === 1
                    ? "-"
                    : item.quantity_returned === null
                    ? item.date_returned + " (Expected)"
                    : item.date_returned}
                  &nbsp;
                </td>
              ),
              status: (item) => (
                <td>
                  {item.statusName !== "Available" && (
                    <CBadge color={item.colorName}>{item.statusName}</CBadge>
                  )}{" "}
                </td>
              ),
            }}
          />
        )}
      </div>
    );
  }
}

export default ReturnList;
