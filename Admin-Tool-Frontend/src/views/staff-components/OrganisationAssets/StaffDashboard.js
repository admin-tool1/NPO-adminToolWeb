import React, { Component } from "react";

class StaffDashboard extends Component {
  constructor() {
    super();
    this.state = {
      isRefresh: localStorage.getItem("refreshAfterLogin"),
    };
  }
  componentDidMount() {
    localStorage.getItem("token");
    this.loadPage();
  }

  loadPage() {
    if (this.state.isRefresh === "false") {
      window.location.reload();
      localStorage.setItem("refreshAfterLogin", true);
    }
  }
  render() {
    return (
      <>
       Dashboard is here!
      </>
    );
  }
}

export default StaffDashboard;
