import {
  CBadge,
  CButton,
  CCol,
  CCollapse,
  CDataTable,
  // CInputCheckbox,
  CRow,
} from "@coreui/react";
import React, { Component } from "react";
import Loader from "src/containers/Loader";
import api from "src/services/api";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";

const iconClose = <CIcon content={freeSet.cilChevronTop} />;
const iconOpen = <CIcon content={freeSet.cilChevronBottom} />;

class ReturnList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      collapse: false,
      listToReturn: [],
      statuses: [],
      checkedItems: new Map(),
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  componentDidMount() {
    this.getToReturnItem();
    this.loadStatuses();
  }

  /** get items to return only */

  getToReturnItem() {
    api.get("/api/getToReturnItem").then((res) => {
      this.setState({
        listToReturn: res.data,
      });
    });
  }

  loadStatuses() {
    api.get("/api/getAssetStatus").then((res) => {
      this.setState({
        statuses: res.data,
      });
    });
  }

  handleSelect(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState((prevState) => ({
      checkedItems: prevState.checkedItems.set(item, isChecked),
    }));
  }

  handleToggle = (e) => {
    this.setState({ collapse: !this.state.collapse });
    e.preventDefault();
  };

  render() {
    var statusName = "";
    var colorName = "";
    const tableItems = [];
    if (this.state.listToReturn) {
      this.state.listToReturn.forEach((item) => {
        this.state.statuses.forEach((status) => {
          if (parseInt(status.id) === parseInt(item.status_id)) {
            statusName = status.status;
            colorName = status.status_color.color;
          }
        });
        tableItems.push({
          item_code: item.item_code,
          item_description: item.item_description,
          purpose: item.purpose,
          date_requested: item.requested_at,
          due_date: item.returned_at,
          status_id: item.status_id,
          status: statusName,
          status_color: colorName,
          remark: item.remark,
        });
      });
    }

    const tableFields = [
      // { key: " " },
      // { key: "id" },
      // { key: "item_description" },
      { key: "item_code" },
      // { key: "requested_qty" },
      // { key: "date_requested" },
      { key: "due_date" },
      { key: "remark" },
    ];

    return (
      <div className="container">
        <div className="justify-content-center">
          <CRow>
            <CCol>
              <h4>Assets To Return</h4>
            </CCol>
            <CButton onClick={this.handleToggle.bind(this)}>
              {this.state.collapse === true && <div>{iconOpen}</div>}
              {this.state.collapse === false && <div>{iconClose}</div>}
            </CButton>
          </CRow>
        </div>

        {this.state.isLoading === true && <Loader />}
        {this.state.isLoading === false && (
          <div>
            <CCollapse show={this.state.collapse}>
              <CDataTable
                items={tableItems}
                fields={tableFields}
                hover
                pagination
                itemsPerPage={5}
                // itemsPerPageSelect
                // tableFilter
                scopedSlots={{
                  due_date: (item) => (
                    <td
                      style={
                        item.status === "Missing" || item.status === "Overdue"
                          ? { color: "#db2127" }
                          : { color: "none" }
                      }
                    >
                      {item.due_date}
                    </td>
                  ),
                  remark: (item) => (
                    <td>
                      <CBadge color={item.status_color} className="p-1">
                        {item.status}
                      </CBadge>
                      <br />
                      {item.remark}
                    </td>
                  ),
                  //   " ": (item) => (
                  //     <td>
                  //       <CInputCheckbox
                  //         id="inline-checkbox1"
                  //         name={item.item_code}
                  //         value={item.item_code}
                  //         style={{
                  //           marginLeft: "0rem",
                  //         }}
                  //         onChange={this.handleSelect}
                  //       />
                  //     </td>
                  //   ),
                }}
              />
            </CCollapse>
          </div>
        )}
        <hr />
      </div>
    );
  }
}

export default ReturnList;
