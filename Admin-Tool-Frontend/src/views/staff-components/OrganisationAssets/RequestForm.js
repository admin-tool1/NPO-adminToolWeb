import React, { Component } from "react";
import {
  CForm,
  CButton,
  CCol,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalHeader,
  CModalBody,
  CBadge,
  CDataTable,
  CInputCheckbox,
  CFormText,
  CInvalidFeedback,
  CRow,
  CInputGroupAppend,
  CInputGroup,
} from "@coreui/react";
import api from "src/services/api";
import swal from "sweetalert2";
import QrReader from "react-qr-reader";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";

const currentDate = () => {
  const today = new Date();
  const dd = String(today.getDate() + 1).padStart(2, "0");
  const mm = String(today.getMonth() + 1).padStart(2, "0"); // January is 0
  const yyyy = today.getFullYear();
  return yyyy + "-" + mm + "-" + dd;
};

class RequestAssets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorList: [],
      staff_email: "",
      item_description: "",
      item_code: "",
      purpose: "",
      borrowDate: "",
      requested_qty: "",
      returned_date: "",
      qr_reader: false,
      readerModal: false,
      codeInput: false,
      item_status: "",
      item_status_color: "",
      detail_id: "",
      item_status_id: "",
      requested_items: [],
      item_count: "",
      isLoading: false,
      tableData: [],
      item_single_use: "",
      hasMultipleUse: false,
      purposeValid: false,
      requestItemsValid: false,
      isChecked_1: false,
      isChecked_2: false,
      requestFormValid: false,
      errors: {},
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleRequest = this.handleRequest.bind(this);
    this.handleErrorQr = this.handleErrorQr.bind(this);
    this.handleReadQr = this.handleReadQr.bind(this);
    this.setQrReader = this.setQrReader.bind(this);
    // this.setReaderModal = this.setReaderModal.bind(this);
    this.setCodeInput = this.setCodeInput.bind(this);
    this.getAsset = this.getAsset.bind(this);
    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.updateList = this.updateList.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
  }

  componentDidMount() {
    this.getStaffId();
    this.loadColor();
  }

  getStaffId() {
    let value = localStorage.getItem("users");
    let json = JSON.parse(value);
    let staffEmail = json.email;
    this.setState({ staff_email: staffEmail });
  }

  loadColor() {
    api.get("/api/statusColor").then((res) => {
      this.setState({
        colorList: res.data,
      });
    });
  }

  // getItemId() {
  //   this.setState({ item_id: this.props.location.state.item_id });
  // }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
    this.validateField(event.target.name, event.target.value);
  }

  handleRequest(e) {
    e.preventDefault();
    this.validateField();
    if (this.state.requestFormValid) {
      swal.fire({
        title: "Processing...",
        showConfirmButton: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      var data = {
        staff_email: this.state.staff_email,
        purpose: this.state.purpose,
        returned_at: this.state.returned_date,
        items: this.state.requested_items,
      };
      // console.log(data);
      api
        .post("/api/logRequest", data)
        .then((res) => {
          if (res.data.status === 200) {
            swal
              .fire({
                title: "Request Sent!",
                text: "Please submit Return Form after use. \n Thank you. ",
                icon: "success",
                confirmButtonColor: "#5bc0de",
              })
              .then(() => {
                window.location.reload();
              });
          }
        })
        .catch((error) => {
          swal.fire({
            title: "Error",
            text: error.response.data.message,
            icon: "error",
          });
        });
    }
  }

  setQrReader() {
    this.setState({ qr_reader: true });
    this.setReaderModal(false);
  }

  handleErrorQr(error) {
    console.log(error);
  }

  handleReadQr(result) {
    if (result) {
      let res = JSON.parse(result);
      this.setState({ item_code: res.full_code });
      this.getAsset();
    } else {
      console.log("Not found");
    }
  }

  setReaderModal(isClose) {
    this.setState({
      readerModal: !this.state.readerModal,
      item_code: "",
      detail_id: "",
      requested_qty: "",
    });
    if (isClose) {
      this.setState({
        codeInput: false,
        qr_reader: false,
      });
    }
  }

  setCodeInput() {
    this.setState({
      codeInput: true,
    });
    this.setReaderModal(false);
  }

  getAsset() {
    api.get("/api/assetItem/" + this.state.item_code).then((res) => {
      // console.log(res.data);
      this.setState({
        detail_id: res.data.detail_id,
        item_status_id: res.data.status_id,
      });
      api.get("/api/assetDetail/" + res.data.detail_id).then((res) => {
        // console.log(res.data);
        this.setState({
          item_description: res.data.description,
          item_single_use: res.data.single_use,
        });
      });
    });
    api.get("/api/getAssetStatus").then((res) => {
      var statuses = res.data;
      statuses.forEach((status) => {
        if (parseInt(status.id) === parseInt(this.state.item_status_id)) {
          this.setState({
            item_status: status.status,
            item_status_color: status.status_color.color,
          });
        }
      });
    });
  }

  addItem() {
    let req_qty;

    /** error checking */
    let qty_filled = true;
    let errors = {};

    if (parseInt(this.state.item_single_use) === 1) {
      if (!this.state.requested_qty) {
        qty_filled = false;
        errors["requested_qty"] = "Quantity is required";
      } else if (parseInt(this.state.requested_qty) === 0) {
        qty_filled = false;
        errors["requested_qty"] = "Quantity cannot be zero or empty";
      } else if (isNaN(+this.state.requested_qty)) {
        qty_filled = false;
        errors["requested_qty"] = "Quantity must be numbers";
      } else {
        req_qty = this.state.requested_qty;
      }
      this.setState({ errors });
    } else {
      req_qty = 1;
    }

    if (qty_filled) {
      this.state.requested_items.push({
        item_code: this.state.item_code,
        requested_qty: req_qty,
      });
      this.updateList(
        this.state.item_description,
        this.state.item_code,
        req_qty,
        this.state.item_single_use
      );
      // console.log(this.state.requested_items);
      this.setReaderModal(true);
    }
  }

  deleteItem(code) {
    let deleteCode = this.state.tableData.findIndex((x) => x.ItemCode === code);
    this.setState({
      isLoading: true,
    });
    if (deleteCode > -1) {
      this.state.tableData.splice(deleteCode, 1);
      this.state.requested_items.splice(deleteCode, 1);
      // console.log("lalu");
    }
    this.updateList();
  }

  updateList(desc, code, qty, single_use) {
    if (code === undefined) {
      this.setState({
        isLoading: false,
      });
    } else {
      this.state.tableData.push({
        Description: desc,
        ItemCode: code,
        Quantity: qty,
        SingleUse: single_use,
      });
    }

    if (this.state.tableData.some((element) => parseInt(element.SingleUse) === 0)) {
      this.setState({ hasMultipleUse: true });
    }

    this.validateField("requested_items", this.state.requested_items);
  }

  handleCheck(e) {
    this.setState(
      {
        [e.target.name]: e.target.checked,
      },
      () => {
        this.validateForm();
      }
    );
  }

  validateField(field, value) {
    let purposeValid = this.state.purposeValid;
    let requestItemsValid = this.state.requestItemsValid;
    let errors = {};

    switch (field) {
      case "purpose":
        if (value.length > 0) {
          purposeValid = value.length > 0;
        } else {
          errors[field] = "Cannot be empty";
        }
        break;
      case "returned_date":
        if (this.state.hasMultipleUse === true) {
          let re = /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
          if (value !== "" && !value.match(re)) {
            errors[field] = "Invalid date format: " + value;
          } else if (value < currentDate()) {
            errors[field] = "Date must not be in the past!";
          } else {
            // returnDateValid = value !== "" && !value.match(re);
          }
        }
        break;
      case "requested_items":
        if (value.length > 0) {
          requestItemsValid = value.length > 0;
        } else {
          errors[field] = "Cannot be empty";
        }
        break;
      default:
        break;
    }

    // eslint-disable-next-line
    if (field == undefined) {
      if (!this.state.purpose) {
        errors["purpose"] = "This field is required.";
      }
      if (this.state.hasMultipleUse === true) {
        if (!this.state.returned_date) {
          errors["returned_date"] = "This field is required.";
        }
      }
      if (!this.state.isChecked_1) {
        errors["isChecked_1"] = "Please read this to proceed";
      }
      if (!this.state.isChecked_2) {
        errors["isChecked_2"] = "Please accept the terms to proceed";
      }
      if (this.state.requested_items.length <= 0) {
        errors["requested_items"] = "Please add items.";
      } else {
        requestItemsValid = true;
      }
    }

    this.setState(
      {
        errors: errors,
        purposeValid: purposeValid,
        requestItemsValid: requestItemsValid,
      },
      () => {
        this.validateForm();
      }
    );
  }

  validateForm() {
    this.setState({
      requestFormValid:
        this.state.purposeValid &&
        this.state.requestItemsValid &&
        this.state.isChecked_1 &&
        this.state.isChecked_2,
    });
    return this.state.requestFormValid;
  }

  render() {
    const { tableData, isLoading } = this.state;
    const tableFields = [
      { key: "Description" },
      { key: "ItemCode" },
      { key: "Quantity" },
      // { key: "SingleUse" },
      { key: "Action" },
    ];
    return (
      <div className="container">
        <div className="justify-content-center">
          <h4>Request Form</h4>
          <hr />
          <CForm className="p-0">
            <CFormGroup>
              {/* <CCol>  row className="mt-2"*/}
              <CLabel htmlFor="purpose-input">
                Purpose <span className="required-text">*</span>
              </CLabel>
              <CInput
                type="text"
                id="purpose-input"
                name="purpose"
                placeholder="Purpose"
                onChange={this.handleChange}
                invalid={this.state.errors["purpose"] ? true : false}
              />
              <CInvalidFeedback>
                {this.state.errors["purpose"]}
              </CInvalidFeedback>
              {/* </CCol> */}
            </CFormGroup>
            {/* {this.state.hasMultipleUse == true && ( */}
            <CFormGroup>
              {/* <CCol>  row className="mt-2"*/}
              <CLabel htmlFor="date-input">Expected Return Date</CLabel>
              <CInput
                type="date"
                id="date-input"
                name="returned_date"
                placeholder="date"
                min={currentDate()}
                onChange={this.handleChange}
                invalid={this.state.errors["returned_date"] ? true : false}
              />
              <CInvalidFeedback>
                {this.state.errors["returned_date"]}
              </CInvalidFeedback>
              {/* </CCol> */}
            </CFormGroup>
            {/* )} */}
            <div style={{ paddingBottom: 10 }}>
              <CRow className="justify-content-center">
                <CButton color="dark" onClick={this.setQrReader}>
                  <CIcon content={freeSet.cilQrCode} size="xl" /> Scan Qr Code
                </CButton>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <CButton color="dark" onClick={this.setCodeInput}>
                  <CIcon content={freeSet.cilKeyboard} size="2xl" /> Enter Code
                </CButton>
              </CRow>
            </div>
            {tableData.length <= 0 && (
              <div>
                <p className="justify-content-center">
                  Please scan item QR Code to start.
                </p>
                <CFormText>
                  <span className="error-text">
                    {this.state.errors["requested_items"]}
                  </span>
                </CFormText>
              </div>
            )}
            {tableData.length > 0 && (
              <div>
                <CDataTable
                  items={tableData}
                  fields={tableFields}
                  loading={isLoading === true}
                  scopedSlots={{
                    Action: (item) => (
                      <td className="delete">
                        <CIcon
                          color="danger"
                          name="cil-x"
                          onClick={this.deleteItem.bind(this, item.ItemCode)}
                        />
                      </td>
                    ),
                  }}
                />
                <CFormGroup variant="custom-checkbox" inline>
                  <CInputCheckbox
                    custom
                    id="inline-checkbox1"
                    name="isChecked_1"
                    value={this.state.isChecked_1}
                    onChange={this.handleCheck}
                  />
                  <CLabel variant="custom-checkbox" htmlFor="inline-checkbox1">
                    If any items are lost under my use, I accept all damages
                    imposed on me.
                    <CFormText>
                      <span className="error-text">
                        {this.state.errors["isChecked_1"]}
                      </span>
                    </CFormText>
                  </CLabel>
                </CFormGroup>
                <CFormGroup variant="custom-checkbox" inline>
                  <CInputCheckbox
                    custom
                    id="inline-checkbox2"
                    name="isChecked_2"
                    value={this.state.isChecked_2}
                    onChange={this.handleCheck}
                  />
                  <CLabel variant="custom-checkbox" htmlFor="inline-checkbox2">
                    I declare that I have read, understand, and agree to the
                    above statement.
                    <CFormText>
                      <span className="error-text">
                        {this.state.errors["isChecked_2"]}
                      </span>
                    </CFormText>
                  </CLabel>
                </CFormGroup>
              </div>
            )}
          </CForm>

          <div className="mt-4">
            <CButton
              className="float-right"
              color="success"
              onClick={this.handleRequest}
            >
              Submit
            </CButton>
          </div>
        </div>
        <CModal
          show={this.state.readerModal}
          onClose={this.setReaderModal.bind(this, true)}
          closeOnBackdrop={false}
          className=""
        >
          <CModalHeader closeButton></CModalHeader>
          <CModalBody>
            {this.state.qr_reader && (
              <QrReader
                delay={300}
                style={{ width: "100%" }}
                onError={this.handleErrorQr}
                onScan={this.handleReadQr}
              />
            )}
            {this.state.codeInput && (
              <CFormGroup>
                <CLabel htmlFor="code-input">Item Code</CLabel>
                <CInputGroup>
                  <CInput
                    type="text"
                    id="code-input"
                    name="item_code"
                    value={this.state.item_code}
                    placeholder="Enter Item Code"
                    onChange={this.handleChange}
                  />
                  <CInputGroupAppend>
                    <CButton color="secondary" onClick={this.getAsset}>
                      <CIcon content={freeSet.cilSearch} />
                    </CButton>
                  </CInputGroupAppend>
                </CInputGroup>
              </CFormGroup>
            )}

            {this.state.detail_id && (
              <div>
                <CCol>
                  Item Code<h5>{this.state.item_code}</h5>
                </CCol>
                <CCol>
                  Status
                  <h5>
                    {" "}
                    <CBadge
                      color={this.state.item_status_color}
                      className="p-1"
                    >
                      {this.state.item_code && this.state.item_status
                        ? this.state.item_status
                        : "Loading..."}
                    </CBadge>
                  </h5>
                </CCol>
                {this.state.item_single_use === 1 && (
                  <CCol>
                    <CFormGroup>
                      <CLabel htmlFor="quantity-input">Quantity</CLabel>
                      <CInput
                        type="quantity"
                        id="quantity-input"
                        name="requested_qty"
                        placeholder="Please enter quantity to request"
                        onChange={this.handleChange}
                        invalid={
                          this.state.errors["requested_qty"] ? true : false
                        }
                      />
                      <CInvalidFeedback>
                        {this.state.errors["requested_qty"]}
                      </CInvalidFeedback>
                    </CFormGroup>
                  </CCol>
                )}
                <CButton
                  className="float-right m-2"
                  color={
                    this.state.item_status === "Available"
                      ? "success"
                      : "secondary"
                  }
                  onClick={this.addItem.bind(this)}
                  disabled={
                    this.state.item_status === "Available" ? false : true
                  }
                >
                  Add
                </CButton>
              </div>
            )}
          </CModalBody>
        </CModal>
      </div>
    );
  }
}

export default RequestAssets;
