import React, { Component } from "react";
import { CCol, CRow, CNav, CNavItem, CNavLink, CProgress } from "@coreui/react";
import api from "../../../services/api";
import Swal from "sweetalert2";
import TypeTaxExemptionForm from "./TypeTaxExemptionForm";
import DonorInformationForm from "./DonorInformationForm";
import DonationReceiptForm from "./DonationReceiptForm";
import PostageForm from "./PostageForm";

class RequestTaxRequestForm extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      typeId: 0,
      types: [],
      type: "",
      dataType: "",
      nameDataType: "",
      name: "",
      donorId: "",
      phoneNumber: "",
      email: "",
      addressLine1: "",
      addressLine2: "",
      city: "",
      postalCode: "",
      stateId: 0,
      arrayState: -1,
      states: [],
      filePath: null,
      filePaths: [],
      amount: 0,
      totalAmount: 0,
      refDonation: [],
      reference: "",
      uploadForm: false,
      receiptCount: 0,
      receiptData: [],
      shippingAddress1: "",
      shippingAddress2: "",
      shippingCity: "",
      shippingPostalCode: "",
      shippingStateId: 0,
      shippingId: 0,
      postStatus: 0,
      postId: 2,
      posts: [],
      postagePath: null,
      activeKey: 1,
      isLoading: false,
      trackingUrl: null,
      urlOrigin: window.location.origin,
      agreement: false,
      donationInformationValid: false,
      nameValid: false,
      donorIdValid: null,
      phoneNumberValid: null,
      emailValid: null,
      addressLine1Valid: false,
      cityValid: false,
      stateIdValid: false,
      postalCodeValid: false,
      filePathValid: false,
      amountValid: false,
      referenceValid: false,
      uploadReceiptValid: false,
      agreementValid: false,
      shippingFormValid: false,
      shippingAddress1Valid: false,
      shippingCityValid: false,
      shippingStateIdValid: false,
      shippingPostalCodeValid: false,
      submitFormValid: false,
    };

    this.changeType = this.changeType.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.handleShippingChecked = this.handleShippingChecked.bind(this);
    this.handlePostStatus = this.handlePostStatus.bind(this);
    this.handleCheckedBox = this.handleCheckedBox.bind(this);
    this.nextButton = this.nextButton.bind(this);
    this.previousButton = this.previousButton.bind(this);
    this.uploadForm = this.uploadForm.bind(this);
    this.uploadReceipt = this.uploadReceipt.bind(this);
    this.deleteReceipt = this.deleteReceipt.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getAllState();
    this.getAllTypeOfTax();
    this.getAllPostage();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  setForm() {
    this.setState({
      typeId: 0,
      type: "",
      dataType: "",
      nameDataType: "",
      name: "",
      donorId: "",
      phoneNumber: "",
      email: "",
      addressLine1: "",
      addressLine2: "",
      city: "",
      postalCode: "",
      stateId: 0,
      arrayState: -1,
      filePath: null,
      filePaths: [],
      amount: 0,
      totalAmount: 0,
      refDonation: [],
      reference: "",
      uploadForm: false,
      receiptCount: 0,
      receiptData: [],
      shippingAddress1: "",
      shippingAddress2: "",
      shippingCity: "",
      shippingPostalCode: "",
      shippingStateId: 0,
      shippingId: 0,
      postStatus: 0,
      postId: 2,
      postagePath: null,
      activeKey: 1,
      isLoading: false,
      trackingUrl: null,
      donationInformationValid: false,
      nameValid: false,
      donorIdValid: null,
      phoneNumberValid: null,
      emailValid: null,
      addressLine1Valid: false,
      cityValid: false,
      stateIdValid: false,
      postalCodeValid: false,
      filePathValid: false,
      amountValid: false,
      uploadReceiptValid: false,
      referenceValid: false,
      shippingFormValid: false,
      shippingAddress1Valid: false,
      shippingCityValid: false,
      shippingStateIdValid: false,
      shippingPostalCodeValid: false,
    });
  }

  getAllState() {
    api.get("/api/allState").then((response) => {
      this.setState({
        states: response.data,
      });
    });
  }

  getAllTypeOfTax() {
    api.get("/api/getTypeTax").then((response) => {
      this.setState({
        types: response.data,
      });
    });
  }

  getAllPostage() {
    api.get("/api/getPostage").then((response) => {
      this.setState({
        posts: response.data,
      });
    });
  }

  changeType = (event) => {
    event.stopPropagation();
    const indexType = this.state.types.findIndex(
      (x) => x.id === parseInt(event.target.value)
    );
    this.setState({
      typeId: event.target.value,
      type: this.state.types[indexType].type,
      dataType: this.state.types[indexType].dataType,
      nameDataType: this.state.types[indexType].name,
    });
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    this.validateField(event.target.name, event.target.value);
  };

  handleCheckedBox = (event) => {
    this.setState({
      [event.target.name]: event.target.checked,
    });
    this.validateField(event.target.name, event.target.checked);
  };

  handlePostStatus = (event) => {
    const indexPosts = this.state.posts.findIndex(
      (x) => x.id === parseInt(event.target.value)
    );
    this.setState({
      postStatus: this.state.posts[indexPosts].confirmed,
      postId: event.target.value,
    });
    if (!this.state.posts[indexPosts].confirmed) {
      this.setState({
        shippingId: this.state.posts[indexPosts].confirmed,
      });
    }
  };

  handleShippingChecked = (event) => {
    this.setState({
      shippingId: event.target.value,
    });

    if (parseInt(event.target.value) === 1) {
      this.setState({
        shippingId: event.target.value,
        shippingAddress1: this.state.addressLine1,
        shippingAddress2: this.state.addressLine2,
        shippingCity: this.state.city,
        shippingPostalCode: this.state.postalCode,
        shippingStateId: this.state.stateId,
      });
    }
    if (parseInt(event.target.value) === 0) {
      this.setState({
        shippingId: event.target.value,
        shippingAddress1: "",
        shippingAddress2: "",
        shippingCity: "",
        shippingPostalCode: "",
        shippingStateId: "",
      });
    }
  };

  handleFile = (event) => {
    this.setState({
      [event.target.name]: event.target.files[0],
    });
    this.validateField(event.target.name, event.target.files[0]);
  };

  validateField(fieldName, value) {
    let nameValid = this.state.nameValid;
    let donorIdValid = this.state.donorIdValid;
    let phoneNumberValid = this.state.phoneNumberValid;
    let emailValid = this.state.emailValid;
    let addressLine1Valid = this.state.addressLine1Valid;
    let cityValid = this.state.cityValid;
    let stateIdValid = this.state.stateIdValid;
    let postalCodeValid = this.state.postalCodeValid;
    let filePathValid = this.state.filePathValid;
    let amountValid = this.state.amountValid;
    let referenceValid = this.state.referenceValid;
    let shippingAddress1Valid = this.state.shippingAddress1Valid;
    let shippingCityValid = this.state.shippingCityValid;
    let shippingStateIdValid = this.state.shippingStateIdValid;
    let shippingPostalCodeValid = this.state.shippingPostalCodeValid;
    let agreementValid = this.state.agreementValid;
    switch (fieldName) {
      case "name":
        nameValid = value.length > 0;
        break;
      case "donorId":
        if (this.state.type === "Personal") {
          const ValidRegex =
            /^(([[1-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01]))-([0-9]{2})-([0-9]{4})$/;
          donorIdValid = this.validateRegex(ValidRegex, value);
          break;
        }
        donorIdValid = value.length > 0;
        break;
      case "phoneNumber":
        if (this.state.type !== "Personal") {
          const ValidRegexCompany = /^[-0-9]+$/;
          const dashed = "-";
          phoneNumberValid =
            value.length >= 10 &&
            this.validateRegex(ValidRegexCompany, value) &&
            value.includes(dashed);
          break;
        }
        const ValidRegexPersonal = /^(01)[0-46-9][-][0-9]{7,8}$/;
        phoneNumberValid =
          value.length >= 10 && this.validateRegex(ValidRegexPersonal, value);
        break;
      case "email":
        const validRegex =
          /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        emailValid = this.validateRegex(validRegex, value);
        break;
      case "addressLine1":
        addressLine1Valid = value.length > 0;
        break;
      case "city":
        cityValid = value.length > 0;
        break;
      case "stateId":
        stateIdValid = value.length > 0;
        break;
      case "postalCode":
        postalCodeValid = value.length > 0;
        break;
      case "filePath":
        filePathValid = value != null;
        break;
      case "amount":
        amountValid = value > 0;
        break;
      case "reference":
        referenceValid = value.length > 0;
        break;
      case "shippingAddress1":
        shippingAddress1Valid = value.length > 0;
        break;
      case "shippingCity":
        shippingCityValid = value.length > 0;
        break;
      case "shippingStateId":
        shippingStateIdValid = value.length > 0;
        break;
      case "shippingPostalCode":
        shippingPostalCodeValid = value.length > 0;
        break;
      case "agreement":
        agreementValid = value;
        break;
      default:
        break;
    }
    this.setState(
      {
        nameValid: nameValid,
        donorIdValid: donorIdValid,
        phoneNumberValid: phoneNumberValid,
        emailValid: emailValid,
        addressLine1Valid: addressLine1Valid,
        cityValid: cityValid,
        stateIdValid: stateIdValid,
        postalCodeValid: postalCodeValid,
        filePathValid: filePathValid,
        amountValid: amountValid,
        referenceValid: referenceValid,
        shippingAddress1Valid: shippingAddress1Valid,
        shippingCityValid: shippingCityValid,
        shippingStateIdValid: shippingStateIdValid,
        shippingPostalCodeValid: shippingPostalCodeValid,
        agreementValid: agreementValid,
      },
      () => {
        this.validateForm();
      }
    );
  }

  validateForm() {
    this.setState({
      donationInformationValid:
        this.state.nameValid &&
        this.state.donorIdValid &&
        this.state.phoneNumberValid &&
        this.state.emailValid &&
        this.state.addressLine1Valid &&
        this.state.cityValid &&
        this.state.stateIdValid &&
        this.state.postalCodeValid,
      uploadReceiptValid:
        this.state.filePathValid &&
        this.state.amountValid &&
        this.state.referenceValid,
      shippingFormValid:
        this.state.shippingAddress1Valid &&
        this.state.shippingCityValid &&
        this.state.shippingStateIdValid &&
        this.state.shippingPostalCodeValid,
      submitFormValid: this.state.agreementValid,
    });
  }

  validateRegex(regex, value) {
    return regex.test(value);
  }

  nextButton() {
    if (this.state.stateId > 0) {
      const number = this.state.states.findIndex(
        (x) => x.id === parseInt(this.state.stateId)
      );
      this.setState({
        arrayState: number,
      });
    }
    this.setState({
      activeKey: this.state.activeKey + 1,
      iconActive: this.state.iconActive + 1,
      progress: this.state.progress + 25,
    });
  }

  previousButton() {
    this.setState({
      activeKey: this.state.activeKey - 1,
      iconActive: this.state.iconActive - 1,
      progress: this.state.progress - 25,
    });
  }

  setActiveKey(key) {
    this.setState({
      activeKey: key,
      iconActive: key,
      progress: (key - 1) * 25,
    });
  }

  uploadForm() {
    this.setState({
      uploadForm: !this.state.uploadForm,
      filePath: null,
      amount: 0,
      reference: "",
      filePathValid: false,
      amountValid: false,
      uploadReceiptValid: false,
    });
    const file = document.querySelector("#filePath");
    file.value = "";
  }

  uploadReceipt() {
    let file = this.state.filePath;
    let amount =
      parseFloat(this.state.totalAmount) + parseFloat(this.state.amount);
    this.state.filePaths.push({
      file: file,
      amount: this.state.amount,
      reference: this.state.reference,
    });
    this.setState({
      totalAmount: parseFloat(amount).toFixed(2),
    });
    this.receiptList(file.name, this.state.amount, this.state.reference);
    this.uploadForm();
  }

  deleteReceipt(id) {
    let deleteId = this.state.receiptData.findIndex((x) => x.Id === id);
    this.setState({
      isLoading: true,
    });
    let totalAmount;
    if (deleteId > -1) {
      totalAmount =
        parseFloat(this.state.totalAmount) -
        parseFloat(this.state.filePaths[deleteId].amount);
      this.state.receiptData.splice(deleteId, 1);
      this.state.filePaths.splice(deleteId, 1);
    }
    this.setState({
      totalAmount: parseFloat(totalAmount).toFixed(2),
    });
    this.receiptList();
  }

  receiptList(file, amount, reference) {
    //update Receipt Table when delete receipt
    if (file === undefined) {
      this.setState({
        isLoading: false,
      });
    } else {
      //update Table when add receipt
      if (reference === "") {
        reference = "-";
      }
      this.state.receiptData.push({
        Id: this.state.receiptCount,
        PaymentReceipt: file,
        Reference: reference,
        Amount: parseFloat(amount).toFixed(2),
      });
      this.setState({
        receiptCount: this.state.receiptCount + 1,
      });
    }
  }

  submitForm() {
    Swal.fire({
      title: "Loading...",
      showConfirmButton: false,
    });
    api.get("/sanctum/csrf-cookie").then(() => {
      let formData = new FormData();
      this.state.filePaths.forEach((file) => {
        formData.append("filePath[]", file.file);
        formData.append("amount[]", file.amount);
        formData.append("reference[]", file.reference);
      });
      formData.append("name", this.state.name);
      formData.append("phoneNumber", this.state.phoneNumber);
      formData.append("email", this.state.email);
      formData.append(
        "address",
        this.state.addressLine1 + " " + this.state.addressLine2
      );
      formData.append("city", this.state.city);
      formData.append("stateId", this.state.stateId);
      formData.append("postalCode", this.state.postalCode);
      formData.append("shippingId", this.state.shippingId);
      formData.append(
        "shippingAddress",
        this.state.shippingAddress1 + " " + this.state.shippingAddress2
      );
      formData.append("shippingCity", this.state.shippingCity);
      formData.append("shippingStateId", this.state.shippingStateId);
      formData.append("shippingPostalCode", this.state.shippingPostalCode);
      formData.append("donorId", this.state.donorId);
      formData.append("totalAmount", this.state.totalAmount);
      formData.append("typeId", this.state.typeId);
      formData.append("type", this.state.type);
      formData.append("postId", this.state.postId);
      formData.append("postagePath", this.state.postagePath);
      formData.append(
        "trackingUrl",
        this.state.urlOrigin + "/#/tax/track-tax-exemption/"
      );
      api
        .post("/api/storeTax", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then((response) => {
          this.getTrackingUrl(response.data.trackingNumber);
          Swal.fire({
            icon: "success",
            title: "Thank You !",
            html:
              "Your request was submitted successfully <br> Your tracking number is <strong>" +
              response.data.trackingNumber +
              "</strong>. <br>You can track your tax exemption at <br> <a class='track-link' href='" +
              this.state.trackingUrl +
              "'" +
              ">Track My Tax Exemption</a>",
            showConfirmButton: true,
            confirmButtonColor: "#225841",
          });
          this.setForm();
        })
        .catch((error) => {
          Swal.fire({
            title: "Error",
            text: error.response.data.errors,
            icon: "error",
            confirmButtonColor: "#39f",
          });
        });
    });
  }

  getTrackingUrl(trackingNumber) {
    this.setState({
      trackingUrl:
        this.state.urlOrigin + "/#/tax/track-tax-exemption/" + trackingNumber,
    });
  }

  render() {
    const { activeKey } = this.state;
    return (
      <div className="containter contentForm">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            <h4>Request Tax Exemption Form</h4>

            <div>
              <h6>
                Thank you for your willingness to contribute in helping Cinta
                Syria Malaysia. For the purpose of obtaining and processing tax
                exemption receipts, you are invited to fill in the following
                information
              </h6>
            </div>
            <hr></hr>
          </CCol>
        </CRow>
        <div className="formLink d-none d-sm-block">
          <CNav justified variant="pills">
            <CNavItem>
              <CNavLink
                className="text-left"
                href="#"
                onClick={this.setActiveKey.bind(this, 1)}
                disabled={activeKey < 1}
              >
                1. Type
              </CNavLink>
              <CProgress
                value={activeKey < 1 ? 0 : 100}
                className="mb-3"
                style={{ height: "5px" }}
              />
            </CNavItem>
            <CNavItem>
              <CNavLink
                className="text-left"
                onClick={this.setActiveKey.bind(this, 2)}
                disabled={activeKey < 2}
              >
                2. Donor Information
              </CNavLink>
              <CProgress
                value={activeKey < 2 ? 0 : 100}
                className="mb-3"
                style={{ height: "5px" }}
              />
            </CNavItem>
            <CNavItem>
              <CNavLink
                className="text-left"
                onClick={this.setActiveKey.bind(this, 3)}
                disabled={activeKey < 3}
              >
                3. Donation
              </CNavLink>
              <CProgress
                value={activeKey < 3 ? 0 : 100}
                className="mb-3"
                style={{ height: "5px" }}
              />
            </CNavItem>
            <CNavItem>
              <CNavLink
                className="text-left"
                onClick={this.setActiveKey.bind(this, 4)}
                disabled={activeKey < 4}
              >
                4. Others
              </CNavLink>
              <CProgress
                value={activeKey < 4 ? 0 : 100}
                className="mb-3"
                style={{ height: "5px" }}
              />
            </CNavItem>
          </CNav>
        </div>
        <div className="justify-content-center pt-3">
          {activeKey === 1 && (
            <TypeTaxExemptionForm
              changeType={this.changeType}
              typeId={this.state.typeId}
              types={this.state.types}
              nextButton={this.nextButton}
            />
          )}
          {activeKey === 2 && (
            <DonorInformationForm
              handleChange={this.handleChange}
              nameDataType={this.state.nameDataType}
              name={this.state.name}
              dataType={this.state.dataType}
              donorId={this.state.donorId}
              phoneNumber={this.state.phoneNumber}
              email={this.state.email}
              addressLine1={this.state.addressLine1}
              addressLine2={this.state.addressLine2}
              city={this.state.city}
              stateId={this.state.stateId}
              states={this.state.states}
              postalCode={this.state.postalCode}
              previousButton={this.previousButton}
              nextButton={this.nextButton}
              donationInformationValid={this.state.donationInformationValid}
              emailValid={this.state.emailValid}
              phoneNumberValid={this.state.phoneNumberValid}
              donorIdValid={this.state.donorIdValid}
              type={this.state.type}
            />
          )}
          {activeKey === 3 && (
            <DonationReceiptForm
              handleChange={this.handleChange}
              handleFile={this.handleFile}
              uploadForm={this.uploadForm}
              isLoading={this.state.isLoading}
              deleteReceipt={this.deleteReceipt}
              receiptData={this.state.receiptData}
              totalAmount={this.state.totalAmount}
              previousButton={this.previousButton}
              nextButton={this.nextButton}
              uploadFormModal={this.state.uploadForm}
              filePath={this.state.filePath}
              reference={this.state.reference}
              amount={this.state.amount}
              uploadReceipt={this.uploadReceipt}
              uploadReceiptValid={this.state.uploadReceiptValid}
            />
          )}
          {activeKey === 4 && (
            <PostageForm
              posts={this.state.posts}
              postStatus={this.state.postStatus}
              postagePath={this.state.postagePath}
              handleChange={this.handleChange}
              handleFile={this.handleFile}
              handlePostStatus={this.handlePostStatus}
              addressLine1={this.state.addressLine1}
              addressLine2={this.state.addressLine2}
              city={this.state.city}
              arrayState={this.state.arrayState}
              states={this.state.states}
              postalCode={this.state.postalCode}
              handleChecked={this.handleShippingChecked}
              shippingId={this.state.shippingId}
              shippingAddress1={this.state.shippingAddress1}
              shippingAddress2={this.state.shippingAddress2}
              shippingCity={this.state.shippingCity}
              shippingStateId={this.state.shippingStateId}
              shippingPostalCode={this.state.shippingPostalCode}
              previousButton={this.previousButton}
              nextButton={this.nextButton}
              setForm={this.setForm.bind(this)}
              submitForm={this.submitForm}
              shippingFormValid={this.state.shippingFormValid}
              submitFormValid={this.state.submitFormValid}
              handleCheckedBox={this.handleCheckedBox}
              agreement={this.state.agreement}
            />
          )}
        </div>
      </div>
    );
  }
}

export default RequestTaxRequestForm;
