import React, { Component } from "react";
import { CFormGroup, CLabel, CFormText, CButton, CSelect } from "@coreui/react";

class TypeTaxExemptionForm extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <h4>Type of Tax Exemption</h4>
        All fields with an asterisk (<span className="required-text">*</span>)
        are mandatory
        <hr></hr>
        <CFormGroup>
          <CLabel htmlFor="type">
            Please select type of Tax Exemption{" "}
            <span className="required-text">*</span>
          </CLabel>
          <CSelect
            custom
            name="typeId"
            id={this.props.typeId}
            value={this.props.typeId}
            onChange={this.props.changeType}
            required
          >
            <option value="0" disabled>
              Please select
            </option>
            {this.props.types.map((type) => (
              <option value={type.id} key={type.id}>
                {type.type}
              </option>
            ))}
          </CSelect>
          <CFormText>Please select 1 before proceed to the next step</CFormText>
        </CFormGroup>
        <CButton
          color="success"
          className="float-right"
          onClick={this.props.nextButton}
          disabled={this.props.typeId <= 0}
        >
          Next
        </CButton>
      </div>
    );
  }
}

export default TypeTaxExemptionForm;
