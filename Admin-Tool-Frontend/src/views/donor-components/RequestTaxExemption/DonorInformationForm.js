import React, { Component } from "react";
import {
  CFormGroup,
  CLabel,
  CInput,
  CCol,
  CButton,
  CSelect,
  CFormText,
  CRow,
  CValidFeedback,
  CInvalidFeedback,
} from "@coreui/react";

class DonorInformationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        {" "}
        <h4>Donor Information</h4>
        All fields with an asterisk (<span className="required-text">*</span>)
        are mandatory
        <hr></hr>
        <CFormGroup>
          <CLabel htmlFor="name">
            {this.props.nameDataType} <span className="required-text">*</span>
          </CLabel>
          <CInput
            type="text"
            id="name"
            name="name"
            placeholder={this.props.nameDataType}
            value={this.props.name}
            onChange={this.props.handleChange}
          />
        </CFormGroup>
        <CFormGroup>
          <CLabel htmlFor="donorId">
            {this.props.dataType} <span className="required-text">*</span>
          </CLabel>
          <CInput
            id="donorId"
            name="donorId"
            placeholder={this.props.dataType}
            value={this.props.donorId}
            onChange={this.props.handleChange}
            valid={this.props.donorIdValid === true}
            invalid={this.props.donorIdValid === false}
          />
          {this.props.type === "Personal" &&
            this.props.donorIdValid === null && (
              <CFormText>
                {this.props.dataType} with dashed , 990112-10-5678
              </CFormText>
            )}
          <CValidFeedback>Valid {this.props.dataType}</CValidFeedback>
          <CInvalidFeedback>
            {this.props.type === "Personal"
              ? "please enter valid " +
                this.props.dataType +
                " with dashed, 991111-11-1111"
              : "Invalid " + this.props.dataType}
          </CInvalidFeedback>
        </CFormGroup>
        <CRow className="my-0">
          <CCol sm="6">
            <CFormGroup>
              <CLabel htmlFor="phoneNumber">
                {this.props.type === "Personal"
                  ? "Phone"
                  : "Company Phone Number"}{" "}
                <span className="required-text">*</span>
              </CLabel>
              <CInput
                id="phoneNumber"
                name="phoneNumber"
                placeholder="Phone"
                value={this.props.phoneNumber}
                onChange={this.props.handleChange}
                valid={this.props.phoneNumberValid === true}
                invalid={this.props.phoneNumberValid === false}
              />
              {this.props.phoneNumberValid === null && (
                <CFormText>
                  Phone number with dashed "-",{" "}
                  {this.props.type === "Personal"
                    ? "013-123456789"
                    : "03-123456789"}
                </CFormText>
              )}
              <CValidFeedback>Valid Phone Number</CValidFeedback>
              <CInvalidFeedback>
                Please enter a valid phone number in Phone number with dashed
                "-",{" "}
                {this.props.type === "Personal"
                  ? "013-123456789"
                  : "03-123456789"}{" "}
                format
              </CInvalidFeedback>
            </CFormGroup>
          </CCol>
          <CCol sm="6">
            <CFormGroup>
              <CLabel htmlFor="email">
                Email <span className="required-text">*</span>
              </CLabel>
              <CInput
                id="email"
                name="email"
                placeholder="Email"
                value={this.props.email}
                invalid={this.props.emailValid === false}
                valid={this.props.emailValid === true}
                onChange={this.props.handleChange}
              />
              <CValidFeedback>Valid Email</CValidFeedback>
              <CInvalidFeedback>
                Please enter a valid email address in the email@abc.com
              </CInvalidFeedback>
            </CFormGroup>
          </CCol>
        </CRow>
        <h5>Address</h5>
        <CFormGroup>
          <CLabel htmlFor="address">
            Street Line 1 <span className="required-text">*</span>
          </CLabel>
          <CInput
            id="addressLine1"
            placeholder="Street Line 1"
            name="addressLine1"
            value={this.props.addressLine1}
            onChange={this.props.handleChange}
          />
        </CFormGroup>
        <CFormGroup>
          <CLabel htmlFor="address">Street Line 2</CLabel>
          <CInput
            id="addressLine2"
            placeholder="Street Line 2"
            name="addressLine2"
            value={this.props.addressLine2}
            onChange={this.props.handleChange}
          />
        </CFormGroup>
        <CRow className="my-0">
          <CCol sm="4">
            <CFormGroup>
              <CLabel htmlFor="city">
                City <span className="required-text">*</span>
              </CLabel>
              <CInput
                id="city"
                name="city"
                placeholder="City"
                value={this.props.city}
                onChange={this.props.handleChange}
              />
            </CFormGroup>
          </CCol>
          <CCol sm="4">
            <CFormGroup>
              <CLabel htmlFor="stateId">
                State <span className="required-text">*</span>
              </CLabel>
              <CSelect
                name="stateId"
                id={this.props.stateId}
                value={this.props.stateId}
                onChange={this.props.handleChange}
              >
                <option key="0" value="0" disabled>
                  Please select
                </option>
                {this.props.states.map((state) => (
                  <option value={state.id} key={state.id.toString()}>
                    {state.state}
                  </option>
                ))}
              </CSelect>
            </CFormGroup>
          </CCol>
          <CCol sm="4">
            <CFormGroup>
              <CLabel htmlFor="postalCode">
                Postal Code <span className="required-text">*</span>
              </CLabel>
              <CInput
                id="postalCode"
                placeholder="Postal Code"
                name="postalCode"
                value={this.props.postalCode}
                onChange={this.props.handleChange}
                required
              />
            </CFormGroup>
          </CCol>
        </CRow>
        <CButton
          color="secondary"
          onClick={this.props.previousButton}
          className="float-left"
        >
          Previous
        </CButton>
        <CButton
          color="success"
          onClick={this.props.nextButton}
          className="float-right nextButton"
          disabled={!this.props.donationInformationValid}
        >
          Next
        </CButton>
      </div>
    );
  }
}

export default DonorInformationForm;
