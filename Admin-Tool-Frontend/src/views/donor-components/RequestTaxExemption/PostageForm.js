import React, { Component } from "react";
import {
  CFormGroup,
  CLabel,
  CInput,
  CCol,
  CInputFile,
  CButton,
  CRow,
  CInputRadio,
  CSelect,
  CTooltip,
  CCallout,
  CFormText,
  CInputCheckbox,
} from "@coreui/react";

class PostageForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <h4>Others</h4>
        All fields with an asterisk (<span className="required-text">*</span>)
        are mandatory
        <hr></hr>
        <CLabel>
          Do you want us to post your tax exemption receipt ?{" "}
          <span className="required-text">*</span>
        </CLabel>
        <CRow className="my-0">
          <CCol>
            {this.props.posts.map((post) => (
              <CTooltip
                content="Requests for postage will be charged RM 5.00"
                placement="top"
                key={"tooptips" + post.postStatus}
              >
                <CFormGroup variant="custom-radio" inline>
                  <CInputRadio
                    custom
                    id={post.id}
                    name="postId"
                    value={post.id}
                    key={post.id.toString()}
                    defaultChecked={parseInt(post.confirmed) === 0}
                    onClick={this.props.handlePostStatus}
                  />
                  <CLabel variant="custom-checkbox" htmlFor={post.id}>
                    {post.postStatus}
                  </CLabel>
                </CFormGroup>
              </CTooltip>
            ))}
          </CCol>
        </CRow>
        {parseInt(this.props.postStatus) === 1 && (
          <div>
            <CRow>
              <CCol md={6}>
                {" "}
                <CCallout color="success">
                  <h6>Please make the postage fee RM 5.00 payment to:</h6>
                  <p>
                    Bank: Bank Rakyat <br></br>
                    Name: Persatuan Cinta Syria Malaysia<br></br>
                    Account Number: 11-394-100122-0
                  </p>
                </CCallout>
              </CCol>
              <CCol md={6}>
                <CFormGroup>
                  <CLabel>Postage Receipt </CLabel>
                  <CInputFile
                    id="postagePath"
                    name="postagePath"
                    type="file"
                    onChange={this.props.handleFile}
                  />
                  <CFormText>
                    The receipt can be uploaded after submitting a tax exemption
                    request
                  </CFormText>
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow className="my-0">
              <CCol>
                <CLabel>
                  Shipping Address same as below address ?{" "}
                  <span className="required-text">*</span>
                </CLabel>
                <CFormGroup variant="custom-radio" inline>
                  <CInputRadio
                    custom
                    id="shippingId1"
                    name="shippingId"
                    value="1"
                    onChange={this.props.handleChecked}
                  />
                  <CLabel variant="custom-checkbox" htmlFor="shippingId1">
                    Yes
                  </CLabel>
                </CFormGroup>
                <CFormGroup variant="custom-radio" inline>
                  <CInputRadio
                    custom
                    id="shippingId2"
                    name="shippingId"
                    defaultChecked
                    value="0"
                    onChange={this.props.handleChecked}
                  />
                  <CLabel variant="custom-checkbox" htmlFor="shippingId2">
                    No
                  </CLabel>
                </CFormGroup>
              </CCol>
            </CRow>
            <CCallout>
              <h5>Address</h5>
              <p>
                {this.props.addressLine1},{this.props.addressLine2},{" "}
                {this.props.city}
                <br></br>
                {this.props.states[this.props.arrayState].state},{" "}
                {this.props.postalCode}
              </p>
            </CCallout>
            {parseInt(this.props.shippingId) === 0 && (
              <div>
                <h5>Shipping Address</h5>
                <CFormGroup>
                  <CLabel htmlFor="address">
                    Street Line 1 <span className="required-text">*</span>
                  </CLabel>
                  <CInput
                    id="shippingAddress1"
                    placeholder="Street Address Line 1"
                    name="shippingAddress1"
                    value={this.props.shippingAddress1}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
                <CFormGroup>
                  <CLabel htmlFor="address">Street Line 2</CLabel>
                  <CInput
                    id="shippingAddress2"
                    placeholder="Street Address Line 2"
                    name="shippingAddress2"
                    value={this.props.shippingAddress2}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
                <CRow className="my-0">
                  <CCol sm="4">
                    <CFormGroup>
                      <CLabel htmlFor="city">
                        City <span className="required-text">*</span>
                      </CLabel>
                      <CInput
                        id="shippingCity"
                        name="shippingCity"
                        placeholder="City"
                        value={this.props.shippingCity}
                        onChange={this.props.handleChange}
                      />
                    </CFormGroup>
                  </CCol>
                  <CCol sm="4">
                    <CFormGroup>
                      <CLabel htmlFor="shippingStateId">
                        State <span className="required-text">*</span>
                      </CLabel>
                      <CSelect
                        name="shippingStateId"
                        id={this.props.shippingStateId}
                        value={this.props.shippingStateId}
                        onChange={this.props.handleChange}
                      >
                        <option value="0">Please select</option>
                        {this.props.states.map((state) => (
                          <option value={state.id} key={state.state}>
                            {" "}
                            {state.state}
                          </option>
                        ))}
                      </CSelect>
                    </CFormGroup>
                  </CCol>
                  <CCol sm="4">
                    <CFormGroup>
                      <CLabel htmlFor="postalCode">
                        Postal Code <span className="required-text">*</span>
                      </CLabel>
                      <CInput
                        id="shippingPostalCode"
                        placeholder="Postal Code"
                        name="shippingPostalCode"
                        value={this.props.shippingPostalCode}
                        onChange={this.props.handleChange}
                      />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </div>
            )}
          </div>
        )}
        <div className="my-3">
          <CFormGroup variant="custom-checkbox" inline>
            <CInputCheckbox
              custom
              id="agreement"
              name="agreement"
              onChange={this.props.handleCheckedBox}
              checked={this.props.agreement}
            />
            <CLabel variant="custom-checkbox" htmlFor="agreement">
              I declare that I agree to allow CSM to use my data for analysis{" "}
              <span className="required-text">*</span>
            </CLabel>
          </CFormGroup>
        </div>
        <div style={{ paddingTop: 20 }}>
          <CButton
            color="secondary"
            onClick={this.props.previousButton}
            className="float-left"
          >
            Previous
          </CButton>
          <div className="float-right">
            <CButton
              type="cancel"
              color="secondary"
              onClick={this.props.setForm}
            >
              {" "}
              Cancel
            </CButton>{" "}
            <CButton
              className="submitButton"
              color="success"
              onClick={this.props.submitForm}
              disabled={
                (this.props.shippingId === 0 &&
                  !this.props.shippingFormValid &&
                  this.props.postStatus === 1) ||
                !this.props.submitFormValid
              }
            >
              Submit
            </CButton>
          </div>
        </div>
      </div>
    );
  }
}

export default PostageForm;
