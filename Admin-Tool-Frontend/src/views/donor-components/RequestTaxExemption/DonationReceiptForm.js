import React, { Component } from "react";
import {
  CFormGroup,
  CLabel,
  CInput,
  CCol,
  CInputFile,
  CButton,
  CRow,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CDataTable,
  CContainer,
  CFormText,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

class DonationReceiptForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  delete(id) {
    this.props.deleteReceipt(id);
  }

  render() {
    const fields = [
      { key: "PaymentReceipt", _style: { width: "80%" } },
      { key: "Amount", _style: { width: "19%" } },
      {
        key: "Action",
        label: "",
        _style: { width: "1%" },
      },
    ];
    return (
      <div>
        <h4>Donation</h4>
        Miminum of total amount for donation is RM 500.00{" "}
        <span className="required-text">*</span>
        <hr></hr>
        <CRow style={{ paddingBottom: 10 }}>
          <CCol>
            <CButton color="dark" block onClick={this.props.uploadForm}>
              <CIcon name="cil-cloud-upload" color="success" />
              Upload Receipt
            </CButton>
          </CCol>
        </CRow>
        {this.props.receiptData.length > 0 && (
          <>
            <CDataTable
              items={this.props.receiptData}
              fields={fields}
              itemsPerPage={5}
              pagination
              bordered
              loading={this.props.isLoading === true}
              scopedSlots={{
                PaymentReceipt: (item) => (
                  <td>
                    {item.PaymentReceipt}
                    <p>Reference: {item.Reference}</p>
                  </td>
                ),
                Action: (item) => (
                  <td>
                    <CIcon
                      color="danger"
                      name="cil-x"
                      onClick={this.delete.bind(this, item.Id)}
                    />
                  </td>
                ),
              }}
            />
            <CContainer>
              <hr></hr>
              <CRow>
                <CCol md="7">
                  <h5 className="float-right">Total Amount:</h5>
                </CCol>
                <CCol md="3 offset-2">
                  <CRow>
                    <h5 className="float-right pr-30">
                      RM {this.props.totalAmount}
                    </h5>
                  </CRow>
                  <CRow>
                    {this.props.totalAmount > 0 &&
                      this.props.totalAmount < 500 && (
                        <span className="required-text">
                          Minimum request is RM 500.00
                        </span>
                      )}
                  </CRow>
                </CCol>
              </CRow>
            </CContainer>
          </>
        )}
        {this.props.receiptData.length <= 0 && <p>Please Upload Receipt</p>}
        <div style={{ paddingTop: 20 }}>
          <CButton
            color="secondary"
            onClick={this.props.previousButton}
            className="float-left"
          >
            Previous
          </CButton>
          <CButton
            color="success"
            onClick={this.props.nextButton}
            className="float-right nextButton"
            disabled={this.props.totalAmount < 500}
          >
            Next
          </CButton>
        </div>
        <CModal
          show={this.props.uploadFormModal}
          onClose={this.props.uploadForm}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>Upload Receipt</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <p>
              All fields with an asterisk (
              <span className="required-text">*</span>) are mandatory
            </p>
            <CFormGroup>
              <CLabel>
                Receipt <span className="required-text">*</span>
              </CLabel>
              <CInputFile
                id="filePath"
                name="filePath"
                type="file"
                onChange={this.props.handleFile}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>
                Amount <span className="required-text">*</span>
              </CLabel>
              <CInput
                id="amount"
                type="number"
                name="amount"
                value={this.props.amount}
                onChange={this.props.handleChange}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel>
                Reference <span className="required-text">*</span>
              </CLabel>
              <CInput
                id="reference"
                type="text"
                name="reference"
                value={this.props.reference}
                onChange={this.props.handleChange}
              />
              <CFormText>
                Please state the current campaign, if unsure please state "umum"
              </CFormText>
            </CFormGroup>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              className="uploadButton"
              onClick={this.props.uploadReceipt}
              disabled={!this.props.uploadReceiptValid}
            >
              Upload
            </CButton>{" "}
            <CButton color="secondary" onClick={this.props.uploadForm}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default DonationReceiptForm;
