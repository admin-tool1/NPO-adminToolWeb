import React, { Component } from "react";
import {
  CCol,
  CRow,
  CLabel,
  CFormGroup,
  CInput,
  CInputGroup,
  CInputGroupAppend,
  CInputFile,
  CButton,
  CCard,
  CCardHeader,
  CCardBody,
  CBadge,
  CDataTable,
  CCallout,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import dateFormat from "dateformat";
import RequestButton from "../RequestButton";
import api from "../../../services/api";
import Swal from "sweetalert2";

class TrackTaxExemption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accordion: true,
      currentTrackingNumber: this.props.match.params.id,
      trackingNumber: "",
      requestID: "",
      requesterName: "",
      requesterPhoneNumber: "",
      requesterEmail: "",
      amountDonation: 0,
      typeDonation: "",
      statusPostageId: 0,
      statusPostage: false,
      statusTax: "",
      colorStatusTax: "",
      trackHistory: [],
      tracking: false,
      postagePaymentStatus: "",
      postagePaymentColor: "",
      postageAddress: "",
      postageCity: "",
      postageState: "",
      postagePostalCode: "",
      uploadModal: false,
      filePath: null,
      error: false,
      errorMessage: "",
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.displayTax();
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  uploadForm() {
    this.setState({
      uploadModal: !this.state.uploadModal,
    });
  }

  uploadReceipt() {
    let formData = new FormData();
    formData.append("postagePath", this.state.filePath);
    formData.append("taxId", this.state.requestID);
    api
      .post("/api/uploadReceipt", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "Thank You !",
          text: "Your receipt was uploaded successfully",
          showConfirmButton: true,
          confirmButtonColor: "#225841",
        });
        this.uploadForm();
        this.setForm();
        this.displayTax();
      });
  }

  setForm() {
    this.setState({
      filePath: null,
    });
  }

  //display when user click on the link
  displayTax() {
    if (this.state.currentTrackingNumber !== undefined) {
      api
        .get("/api/trackTax/" + this.state.currentTrackingNumber)
        .then((response) => {
          this.setState(
            {
              error: false,
              tracking: true,
              trackingNumber: this.state.currentTrackingNumber,
              requestID: response.data.tax.id,
              requesterName: response.data.tax.name,
              requesterPhoneNumber: response.data.tax.phoneNumber,
              requesterEmail: response.data.tax.email,
              amountDonation: response.data.tax.amount,
              typeDonation: response.data.tax.tax_exemptions_type.type,
              statusPostageId: parseInt(
                response.data.tax.tax_exemptions_postage.confirmed
              ),
              statusTax: response.data.tax.tax_exemptions_status.status,
              colorStatusTax:
                response.data.tax.tax_exemptions_status.status_color.color,
              trackHistory: response.data.trackHistory,
            },
            () => {
              if (this.state.statusPostageId) {
                this.setState({
                  statusPostage: true,
                  postageAddress: response.data.tax.postage_addresses.address,
                  postageCity: response.data.tax.postage_addresses.city,
                  postageState:
                    response.data.tax.postage_addresses.addresses_state.state,
                  postagePostalCode:
                    response.data.tax.postage_addresses.postalCode,
                });

                if (response.data.tax.tax_exemptions_postage_receipt !== null) {
                  this.setState({
                    postagePaymentStatus:
                      response.data.tax.tax_exemptions_postage_receipt
                        .tax_exemptions_status.status,
                    postagePaymentColor:
                      response.data.tax.tax_exemptions_postage_receipt
                        .tax_exemptions_status.status_color.color,
                  });
                }
              }
            }
          );
        })
        .catch((error) => {
          this.setState({
            trackingNumber: this.state.currentTrackingNumber,
            errorMessage: error.response.data.error,
            error: true,
            tracking: false,
          });
        });
    }
  }
  searchTax() {
    window.location.hash =
      "/tax/track-tax-exemption/" + this.state.trackingNumber;
  }

  render() {
    const ColoredLine = ({ color }) => (
      <hr
        style={{
          color: color,
          backgroundColor: color,
          height: 1,
        }}
      />
    );

    const fields = ["Date", "Remark"];

    const ListOfHistory = [];
    this.state.trackHistory.forEach((tax) => {
      ListOfHistory.push({
        Date: dateFormat(tax.created_at, "dd/mm/yyyy"),
        Status: tax.tax_exemptions_status.status,
        Remark: tax.remark,
      });
    });

    const {
      trackingNumber,
      requestID,
      requesterName,
      requesterPhoneNumber,
      requesterEmail,
      amountDonation,
      typeDonation,
      statusPostage,
      statusTax,
      colorStatusTax,
      tracking,
      postageAddress,
      postageCity,
      postageState,
      postagePostalCode,
      postagePaymentStatus,
      postagePaymentColor,
      error,
      errorMessage,
    } = this.state;

    return (
      <div className="containter TrackingSystem">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            <h4>Track Tax Exemption</h4>
          </CCol>
        </CRow>
        <ColoredLine color="info" />
        <CFormGroup row className="justify-content-center">
          <CCol md="8">
            <CLabel>Tracking Number</CLabel>
            <CInputGroup>
              <CInput
                type="text"
                id="trackingNumber"
                name="trackingNumber"
                value={this.state.trackingNumber}
                onChange={this.handleChange}
              />
              <CInputGroupAppend>
                <CButton
                  type="button"
                  className="trackingTaxButton"
                  color="success"
                  onClick={this.searchTax.bind(this)}
                >
                  <CIcon
                    className="trackingSearchIcon"
                    name="cil-magnifying-glass"
                  />
                </CButton>
              </CInputGroupAppend>
            </CInputGroup>
          </CCol>
        </CFormGroup>
        <ColoredLine color="info" />
        {error === true && (
          <div>
            <CRow className="justify-content-center">
              <CCol md="6">
                <div className="clearfix text-center">
                  <CIcon className="sadIcon" name="cil-sad" />
                  <h4 className="pt-3">Oops! {errorMessage}</h4>
                  <p className="text-muted">
                    Tracking number, <strong>{trackingNumber}</strong> was not
                    found.
                  </p>
                </div>
              </CCol>
            </CRow>
            <ColoredLine color="info" />
          </div>
        )}
        {tracking === true && (
          <div className="progressTrack">
            <h4 className="pb-3">
              Request ID: {requestID}
              {"  "}
              <CBadge color={colorStatusTax}>{statusTax}</CBadge>
            </h4>
            <CRow>
              <CCol md={7}>
                <CCard className="taxExemptionProgress">
                  <CCardHeader>
                    <h5>Tax Exemption Progress</h5>
                  </CCardHeader>
                  <CCardBody>
                    <CDataTable
                      items={ListOfHistory}
                      fields={fields}
                      bordered
                      itemsPerPage={10}
                      pagination
                      scopedSlots={{
                        Remark: (item) => (
                          <td>
                            Status:{item.Status}
                            <br></br>
                            {item.Remark}
                          </td>
                        ),
                      }}
                    />
                  </CCardBody>
                </CCard>
              </CCol>
              <CCol md={5}>
                <CCard className="taxExemptionDetails">
                  <CCardHeader>
                    <h5>Tax Exemption Information </h5>
                  </CCardHeader>
                  <CCardBody>
                    <CCol>
                      <CRow>
                        <p>
                          <strong>Name:</strong>
                          <br></br>
                          {requesterName}
                        </p>
                      </CRow>
                      <CRow>
                        <p>
                          <strong>Phone Number:</strong>
                          <br></br>
                          {requesterPhoneNumber}
                        </p>
                      </CRow>
                      <CRow>
                        <p>
                          <strong>Email:</strong>
                          <br></br>
                          {requesterEmail}
                        </p>
                      </CRow>
                      <CRow>
                        <p>
                          <strong>Donation Type:</strong>
                          <br></br>
                          {typeDonation}
                        </p>
                      </CRow>
                      <CRow>
                        <p>
                          <strong>Amount:</strong>
                          <br></br>
                          {amountDonation}
                        </p>
                      </CRow>
                    </CCol>
                  </CCardBody>
                </CCard>
                {statusPostage === true && (
                  <CCard className="taxExemptionShipping">
                    <CCardHeader>
                      <h5>Shipping Information</h5>
                    </CCardHeader>
                    <CCardBody>
                      <CCol>
                        <CRow>
                          <p>
                            <strong>Address:</strong>
                            <br></br>
                            {postageAddress}, {postageCity}
                          </p>
                        </CRow>
                        <CRow>
                          <p>
                            <strong>State:</strong>
                            <br></br>
                            {postageState}
                          </p>
                        </CRow>
                        <CRow>
                          <p>
                            <strong>Postal Code:</strong>
                            <br></br>
                            {postagePostalCode}
                          </p>
                        </CRow>

                        <CRow>
                          <p>
                            <strong>Payment Status: </strong>{" "}
                            <CBadge color={postagePaymentColor}>
                              {postagePaymentStatus}
                            </CBadge>
                          </p>
                          {postagePaymentStatus === "Not Paid" && (
                            <>
                              <CCallout color="success">
                                <h6>
                                  Please make the postage fee RM5.00 payment to:
                                </h6>
                                <p>
                                  Bank: BANK RAKYAT <br></br>
                                  Name: Persatuan Cinta Syria Malaysia<br></br>
                                  Account No.: 11-394-100122-0
                                </p>
                              </CCallout>
                              <CButton
                                block
                                size="sm"
                                color="success"
                                className="uploadReceiptButton"
                                onClick={this.uploadForm.bind(this)}
                              >
                                Upload Receipt
                              </CButton>
                            </>
                          )}
                        </CRow>
                      </CCol>
                    </CCardBody>
                  </CCard>
                )}
              </CCol>
            </CRow>
            <ColoredLine color="info" />
          </div>
        )}
        <CRow className="justify-content-center">
          <CCol>
            <RequestButton />
          </CCol>
        </CRow>

        <CModal
          show={this.state.uploadModal}
          onClose={this.uploadForm.bind(this)}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>Upload Receipt</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CFormGroup>
              <CLabel>Receipt:</CLabel>
              <CInputFile
                id="filePath"
                name="filePath"
                type="file"
                onChange={(event) =>
                  this.setState({
                    filePath: event.target.files[0],
                  })
                }
              />
            </CFormGroup>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              className="uploadButton"
              onClick={this.uploadReceipt.bind(this)}
            >
              Upload
            </CButton>{" "}
            <CButton color="secondary" onClick={this.uploadForm.bind(this)}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default TrackTaxExemption;
