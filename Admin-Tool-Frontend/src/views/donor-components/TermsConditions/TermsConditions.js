import React, { Component } from "react";
import { CCol, CRow } from "@coreui/react";
import api from "../../../services/api";
import RequestButton from "../RequestButton";

class TermPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      terms: [],
      no: 0,
    };
  }

  componentDidMount() {
    this.getAllTerms();
  }

  getAllTerms() {
    api.get("/api/getAllTerms").then((response) => {
      this.setState({
        terms: response.data.term,
      });
    });
  }

  render() {
    const ColoredLine = ({ color }) => (
      <hr
        style={{
          color: color,
          backgroundColor: color,
          height: 1,
        }}
      />
    );
    return (
      <div className="containter">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            <h4>Terms & Conditions</h4>
            <ColoredLine color="info" />
            {this.state.terms.map((term) => (
              <div key={term.Key}>
                <p>
                  <strong>{term.No}</strong> {term.Term}
                </p>
              </div>
            ))}
            <ColoredLine color="info" />
            <RequestButton />
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default TermPage;
