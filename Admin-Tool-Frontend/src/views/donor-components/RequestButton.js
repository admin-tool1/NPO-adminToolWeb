import React from "react";
import { CButton } from "@coreui/react";

const RequestButton = () => {
  return (
    <CButton color="success" variant="outline" to="/tax/request-tax-exemption">
      Request Tax Exemption
    </CButton>
  );
};

export default RequestButton;
