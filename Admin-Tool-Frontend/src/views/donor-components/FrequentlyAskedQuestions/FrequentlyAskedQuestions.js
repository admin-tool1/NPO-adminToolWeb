import React, { Component } from "react";
import { CCol, CRow } from "@coreui/react";
import RequestButton from "../RequestButton";
import api from "../../../services/api";

class TaxRequestForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      faqs: [],
    };
  }

  componentDidMount() {
    this.getAllFaqs();
  }

  getAllFaqs() {
    api.get("/api/getAllFaqs").then((response) => {
      this.setState({
        faqs: response.data.faq,
      });
    });
  }
  render() {
    const ColoredLine = ({ color }) => (
      <hr
        style={{
          color: color,
          backgroundColor: color,
          height: 1,
        }}
      />
    );

    return (
      <div className="containter">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            <h4>Frequently Asked Questions</h4>
            <ColoredLine color="info" />

            {this.state.faqs.map((faq) => (
              <div key={faq.Id}>
                <p>
                  <strong>{faq.Question}</strong>
                </p>
                <p className="text-muted">
                  <span dangerouslySetInnerHTML={{ __html: faq.Answer }} />
                </p>
              </div>
            ))}
            <ColoredLine color="info" />
            <RequestButton />
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default TaxRequestForm;
