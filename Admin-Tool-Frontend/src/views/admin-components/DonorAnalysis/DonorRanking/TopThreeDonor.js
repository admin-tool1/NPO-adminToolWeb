import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CRow,
  CCol,
  CCardFooter,
  CButton,
} from "@coreui/react";

class TopThreeDonor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      donors: [],
      css: [
        "c-avatar c-avatar-lg first",
        "c-avatar c-avatar-lg second",
        "c-avatar c-avatar-lg third",
      ],
      numbering: ["1st", "2nd", "3rd"],
    };
  }

  topTenDonorPage(type) {
    window.location.hash = "/admin/donor-analysis/" + type;
  }

  render() {
    if (this.props.TopThreeDonor.length !== 0) {
      let number = 0;
      this.props.TopThreeDonor.forEach((donor) => {
        this.state.donors.push({
          key: this.props.TypeDonor + number,
          name: donor.tax_exemption.name,
          frequently: donor.frequently,
          recently: donor.recently,
          monetary: donor.monetary,
          css: this.state.css[number],
          numbering: this.state.numbering[number],
        });
        number++;
      });
    }

    return (
      <div>
        <CRow className="justify-content-center topThreeDonor">
          {this.state.donors.length < 3 && <></>}
          {this.state.donors.length === 3 && (
            <>
              {this.state.donors.map((item) => {
                return (
                  <CCol xl={3} key={item.key}>
                    <CCard className="shadow p-3 mb-5 bg-white rounded">
                      <CCardBody>
                        <div className="top">
                          <div className={item.css}>{item.numbering}</div>
                        </div>
                        <div className="topDetails text-center">
                          <p>{item.name}</p>
                        </div>
                      </CCardBody>
                      <CRow>
                        <CCol>
                          <div className="text-value-lg text-center">
                            {item.recently}
                          </div>
                          <div className="text-uppercase text-muted small">
                            Recently
                          </div>
                        </CCol>
                        <div className="c-vr"></div>
                        <CCol>
                          <div className="text-value-lg text-center">
                            {item.frequently}
                          </div>
                          <div className="text-uppercase text-muted small">
                            Frequenlty
                          </div>
                        </CCol>
                        <div className="c-vr"></div>
                        <CCol>
                          <div className="text-value-lg text-center">
                            {item.monetary}
                          </div>
                          <div className="text-uppercase text-muted small">
                            Monetary
                          </div>
                        </CCol>
                      </CRow>
                    </CCard>
                  </CCol>
                );
              })}
            </>
          )}
        </CRow>
        <CCardFooter>
          <CButton
            block
            color="link"
            className="text-right m-0 p-0"
            onClick={this.topTenDonorPage.bind(this, this.props.type)}
          >
            <h5 className="m-0 p-0">See More</h5>
          </CButton>
        </CCardFooter>
      </div>
    );
  }
}

export default TopThreeDonor;
