import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CCardFooter,
  CCol,
  CButton,
  CRow,
} from "@coreui/react";
import TopThreeDonor from "./TopThreeDonor";
import NoData from "./NoData";
import Loader from "../../../../containers/Loader";
import api from "../../../../services/api";

class DonorAnalysis extends Component {
  constructor() {
    super();
    this.state = {
      bestDonorList: [],
      topThreeBestDonor: [],
      highMDonorList: [],
      topThreeHighMDonor: [],
      lowMDonorList: [],
      topThreeLowMDonor: [],
      oldDonorList: [],
      topThreeOldDonor: [],
      loading: true,
    };
  }

  componentDidMount() {
    this.donorAlgorithm();
    this.getDonorRanking();
  }

  getDonorRanking() {
    api.get("/api/getDonorRanking").then((response) => {
      this.setState({
        bestDonorList: response.data.bestDonor,
        topThreeBestDonor: response.data.topThreeBestDonor,
        topThreeHighMDonor: response.data.topThreeHighMDonor,
        topThreeLowMDonor: response.data.topThreelowMDonor,
        oldDonorList: response.data.oldDonor,
        loading: false,
      });
    });
  }

  donorAlgorithm() {
    api.get("/api/donorAnalysisAlgorithm").then(() => {});
  }

  topTenDonorPage(type) {
    window.location.hash = "/admin/donor-analysis/" + type;
  }

  render() {
    if (this.state.loading === true) {
      return <Loader />;
    }
    if (this.state.loading === false) {
      return (
        <div>
          <CCard className="donorRanking">
            <CCardHeader>
              <h3>Donor Ranking</h3>
            </CCardHeader>
            <CCardBody>
              <CRow className="topDonor">
                <CCol>
                  <CCard>
                    <h5 className="donorTitle text-center">
                      BEST DONOR RANKING
                    </h5>
                    <p className="textDescriptionDonorRanking text-muted text-center">
                      The donor has the highest for recently, frequently and
                      monetary.
                    </p>
                    {this.state.topThreeBestDonor === null && <NoData />}
                    {this.state.topThreeBestDonor !== null && (
                      <>
                        <TopThreeDonor
                          TopThreeDonor={this.state.topThreeBestDonor}
                          TypeDonor="BestDonor"
                          type="best-donors"
                        />
                      </>
                    )}
                  </CCard>
                  <CCard>
                    <h5 className="donorTitle text-center">
                      NEW DONOR RANKING
                    </h5>
                    <p className="textDescriptionDonorRanking text-muted text-center">
                      The donor that high in recently and monetary but low in
                      frequently
                    </p>
                    {this.state.topThreeHighMDonor === null && <NoData />}
                    {this.state.topThreeHighMDonor !== null && (
                      <>
                        <TopThreeDonor
                          TopThreeDonor={this.state.topThreeHighMDonor}
                          TypeDonor="HighMDonor"
                          type="high-monetary"
                        />
                      </>
                    )}
                  </CCard>
                  <CCard>
                    <h5 className="donorTitle text-center">
                      LOYAL DONOR RANKING
                    </h5>
                    <p className="textDescriptionDonorRanking text-muted text-center">
                      The donor that has high in recently and frequently but low
                      in monetary
                    </p>
                    {this.state.topThreeLowMDonor === null && <NoData />}
                    {this.state.topThreeLowMDonor !== null && (
                      <>
                        <TopThreeDonor
                          TopThreeDonor={this.state.topThreeLowMDonor}
                          TypeDonor="LowMDonor"
                          type="low-monetary"
                        />
                      </>
                    )}
                  </CCard>
                  <CCard>
                    <h5 className="donorTitle text-center">
                      OLD DONORS RANKING
                    </h5>
                    <p className="textDescriptionDonorRanking text-muted text-center">
                      The donor that low in recently but average/high in
                      frequently and monetary. The donor that has been long do
                      not do any donation.
                    </p>
                    {this.state.oldDonorList === null && <NoData />}
                    {this.state.oldDonorList !== null && (
                      <CCardFooter>
                        <CButton
                          block
                          color="link"
                          className="text-right m-0 p-0"
                          onClick={this.topTenDonorPage.bind(
                            this,
                            "old-donors"
                          )}
                        >
                          <h5 className="m-0 p-0">See More</h5>
                        </CButton>
                      </CCardFooter>
                    )}
                  </CCard>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </div>
      );
    }
  }
}

export default DonorAnalysis;
