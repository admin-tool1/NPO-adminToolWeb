import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CCol,
  CRow,
  CInputGroup,
  CInputGroupAppend,
  CLabel,
  CButton,
} from "@coreui/react";
import { RangeDatePicker } from "@y0c/react-datepicker";
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { startOfMonth } from "date-fns";
import dateFormat from "dateformat";
import Chart from "../DonorStatistic/BarChart/Charts";
import WidgetsDropDopwn from "./widgets/WidgetsDropdown";
import Loader from "../../../../containers/Loader";
import api from "../../../../services/api";

class DonorStatistic extends Component {
  constructor() {
    super();
    this.state = {
      startDate: startOfMonth(new Date()),
      endDate: new Date(),
      labels: [],
      quantity: [],
      totalMoneyByState: [],
      totalMoney: [],
      totalTax: [],
      loader: true,
    };

    this.handleDate = this.handleDate.bind(this);
  }

  componentDidMount() {
    this.getDonorState();
  }

  handleDate =
    () =>
    (...args) => {
      this.setState({
        startDate: args[0],
        endDate: args[1],
      });
    };

  getDonorState() {
    let result = new Date(this.state.endDate);
    result.setDate(result.getDate() + 1);

    const startDate = dateFormat(this.state.startDate, "yyyy-mm-dd");
    const endDate = dateFormat(result, "yyyy-mm-dd");
    api
      .get("/api/getDonorStateByDate/" + startDate + "/" + endDate)
      .then((response) => {
        this.setState({
          labels: response.data.labels,
          quantity: response.data.quantity,
          totalMoneyByState: response.data.totalMoneyByState,
          totalMoney: response.data.totalMoney,
          totalTax: response.data.totalTax,
          loader: false,
        });
      });
  }

  render() {
    if (this.state.loader === true) {
      return <Loader />;
    }
    if (this.state.loader === false) {
      return (
        <div>
          <CCard>
            <CCardHeader>
              <h3>Donor Statistic</h3>
            </CCardHeader>
            <CCardBody>
              <CCol className="mb-4 ml-2">
                <CLabel>Please Select Date:</CLabel>
                <CInputGroup>
                  <RangeDatePicker
                    initialStartDate={this.state.startDate}
                    initialEndDate={this.state.endDate}
                    onChange={this.handleDate()}
                  />
                  <CInputGroupAppend>
                    <CButton
                      type="button"
                      color="dark"
                      onClick={this.getDonorState.bind(this)}
                    >
                      Generate
                    </CButton>
                  </CInputGroupAppend>
                </CInputGroup>
              </CCol>
              <div className="justify-content-center">
                <WidgetsDropDopwn
                  totalTax={this.state.totalTax}
                  totalMoney={this.state.totalMoney}
                />
              </div>

              <CCard className="donorStateCard">
                <CCardHeader>
                  <CRow>
                    <CCol sm={8}>
                      <h5> Total Tax Exemption by State</h5>
                    </CCol>
                  </CRow>
                </CCardHeader>
                <CCardBody className="donorStateChart">
                  <Chart
                    labels={this.state.labels}
                    quantity={this.state.quantity}
                    data="Total Tax Exemption"
                  />
                </CCardBody>
              </CCard>

              <CCard className="donorStateCard">
                <CCardHeader>
                  <CRow>
                    <CCol sm={8}>
                      <h5> Total Money of Tax Exemption by State</h5>
                    </CCol>
                  </CRow>
                </CCardHeader>
                <CCardBody className="donorStateChart">
                  <Chart
                    labels={this.state.labels}
                    quantity={this.state.totalMoneyByState}
                    data="Total Money"
                  />
                </CCardBody>
              </CCard>
            </CCardBody>
          </CCard>
        </div>
      );
    }
  }
}

export default DonorStatistic;
