import React, { Component } from "react";
import api from "../../../services/api";
import {
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CFormGroup,
  CLabel,
  CInput,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
  CSelect,
} from "@coreui/react";

class StatusOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusList: [],
      currentStatus: "",
      currentColor: "",
      currentId: "",
    };
  }

  componentDidMount() {
    this.getStatus();
  }

  getStatus() {
    api.get("api/statusTaxExemptions").then((response) => {
      this.setState({
        statusList: response.data.status,
      });
    });
  }

  handleStatus(id, status, color) {
    this.props.handleChangeStatus(id, status, color);
  }

  render() {
    if (this.props.taxDetail) {
      return (
        <>
          <CButton
            variant="outline"
            color="dark"
            onClick={this.props.updateStatusDetails}
          >
            Update Status
          </CButton>
          <CModal
            alignment="center"
            show={this.props.updateStatusModal}
            onClose={this.props.updateStatusDetails}
            closeOnBackdrop={false}
            className="updateStatusModal"
          >
            <CModalHeader closeButton>
              <CModalTitle>Update Status</CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CCol>
                <CLabel>Status:</CLabel>
                <CFormGroup>
                  <CDropdown className="m-1 btn-group" name="statusId">
                    <CDropdownToggle
                      value={this.props.changeStatusId}
                      color={this.props.changeStatusColor}
                    >
                      {this.props.changeStatus}
                    </CDropdownToggle>
                    <CDropdownMenu>
                      {this.state.statusList.map((item) => (
                        <CDropdownItem
                          value={item.Id}
                          key={item.Id}
                          onClick={this.handleStatus.bind(
                            this,
                            item.Id,
                            item.Status,
                            item.Color
                          )}
                        >
                          {item.Status}
                        </CDropdownItem>
                      ))}
                    </CDropdownMenu>
                  </CDropdown>
                </CFormGroup>
                <CFormGroup>
                  <CLabel htmlFor="remark">Remark:</CLabel>
                  <CInput
                    id="remark"
                    name="remark"
                    value={this.props.remark}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CModalBody>
            <CModalFooter>
              <CButton
                color="secondary"
                onClick={this.props.updateStatusDetails}
              >
                Cancel
              </CButton>
              <CButton
                className="saveUpdateStatus"
                color="info"
                onClick={this.props.updateStatus}
              >
                Save
              </CButton>
            </CModalFooter>
          </CModal>
        </>
      );
    } else {
      return (
        <>
          <CButton
            variant="outline"
            color="dark"
            disabled={this.props.pushArray === 0}
            onClick={this.props.updateModal}
          >
            Update Status
          </CButton>
          <CModal
            alignment="center"
            show={this.props.updateStatusModal}
            onClose={this.props.updateModal}
            closeOnBackdrop={false}
            className="updateStatusModal"
          >
            <CModalHeader closeButton>
              <CModalTitle>Update Status</CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CCol>
                <CFormGroup>
                  <CLabel>Status:</CLabel>
                  <CSelect
                    custom
                    name="statusId"
                    id={this.props.statusId}
                    value={this.props.statusId}
                    onChange={this.props.handleChange}
                  >
                    <option value="0">Please Select</option>
                    {this.state.statusList.map((item) => (
                      <option value={item.Id} key={item.Id}>
                        {item.Status}
                      </option>
                    ))}
                  </CSelect>
                </CFormGroup>
                <CFormGroup>
                  <CLabel htmlFor="remark">Remark:</CLabel>
                  <CInput
                    id="remark"
                    name="remark"
                    value={this.props.remark}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CModalBody>
            <CModalFooter>
              <CButton color="secondary" onClick={this.props.updateModal}>
                Cancel
              </CButton>
              <CButton color="success" onClick={this.props.updateStatus}>
                Save
              </CButton>
            </CModalFooter>
          </CModal>
        </>
      );
    }
  }
}

export default StatusOption;
