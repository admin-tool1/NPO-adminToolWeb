import React, { Component } from "react";
import {
  CFormGroup,
  CLabel,
  CInput,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
} from "@coreui/react";
import EmailContent from "../SunEditor/ContentForm";

class DeclineModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <CModal
          alignment="center"
          show={this.props.declineModal}
          onClose={this.props.setDeclineModal}
          closeOnBackdrop={false}
          className="updateStatusModal"
        >
          <CModalHeader closeButton>
            <CModalTitle>Decline Tax Request</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CCol>
              <CFormGroup>
                <CLabel>To:</CLabel>
                <CInput
                  id="email"
                  name="email"
                  value={this.props.email}
                  onChange={this.props.handleChange}
                  readOnly
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel htmlFor="remark">Reason:</CLabel>
                <EmailContent
                  content={this.props.reason}
                  handleContent={this.props.handleContent}
                />
              </CFormGroup>
            </CCol>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.props.setDeclineModal}>
              Cancel
            </CButton>
            <CButton color="success" onClick={this.props.declineTax}>
              Decline Tax and Send Email
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default DeclineModal;
