import React, { Component } from "react";
import { CRow, CCol, CButton, CLink, CBadge } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Swal from "sweetalert2";

class PostageDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  statusPostage(status) {
    Swal.fire({
      title: "Are you sure?",
      html: status ? "Approve the payment" : "Decline the payment",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        this.props.updateStatusPostage(status, this.props.requestId);
      }
    });
  }

  render() {
    if (this.props.postStatus === false) {
      return (
        <div>
          <p className="text-muted">
            The data is unavailable since the donor did not request for physical
            receipts
          </p>
        </div>
      );
    }
    return (
      <div>
        <CRow>
          <strong>Payment Status:</strong>
        </CRow>
        <CRow className="py-1">
          <CBadge color={this.props.statusPostageColor}>
            {this.props.statusPaymentPostage}
          </CBadge>
        </CRow>
        <CRow>
          <strong>Payment Receipt:</strong>
        </CRow>
        <CRow className="py-1">
          {this.props.postageReceipt === null && <p>Data Not Available</p>}
          {this.props.postageReceipt != null && (
            <>
              <CButton color="light" size="sm">
                <CLink
                  href={this.props.postageReceipt}
                  style={this.props.LinkStyle}
                  target="_blank"
                  onMouseEnter={this.props.Enter}
                  onMouseLeave={this.props.Leave}
                >
                  <CIcon name="cil-file" className="mfe-2" />
                  Receipt
                </CLink>
              </CButton>
              {this.props.isApprove === false && (
                <div className="px-3">
                  <CButton
                    className="approvePostage mx-1"
                    variant="outline"
                    color="success"
                    size="sm"
                    onClick={this.statusPostage.bind(this, true)}
                  >
                    <CIcon name="cil-check-alt" />
                  </CButton>
                  <CButton
                    className="declinePostage mx-1"
                    variant="outline"
                    color="danger"
                    size="sm"
                    onClick={this.statusPostage.bind(this, false)}
                  >
                    <CIcon name="cil-x" />
                  </CButton>
                </div>
              )}
            </>
          )}
        </CRow>
        <CRow>
          <strong>Address:</strong>
        </CRow>
        <CRow className="py-1">
          {this.props.addressShipping}, {this.props.cityShipping}
        </CRow>
        <CRow>
          <CCol>
            <CRow>
              <strong>State:</strong>
            </CRow>
            <CRow className="py-1">{this.props.stateShipping}</CRow>
          </CCol>
          <CCol>
            <CRow>
              <strong>PostalCode:</strong>
            </CRow>
            <CRow className="py-1">{this.props.postalCodeShipping}</CRow>
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default PostageDetail;
