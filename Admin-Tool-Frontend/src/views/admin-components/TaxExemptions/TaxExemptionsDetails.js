import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CRow,
  CCol,
  CButton,
  CLink,
  CBadge,
  CDataTable,
  CCardFooter,
} from "@coreui/react";
import api from "src/services/api";
import StatusOption from "./StatusOption";
import dateFormat from "dateformat";
import CIcon from "@coreui/icons-react";
import PostageDetail from "./PostageDetail";
import SendEmail from "./SendEmail";
import DeclineModal from "./DeclineModal";
import Swal from "sweetalert2";
import Loader from "../../../containers/Loader";

const LinkStyle = {
  textDecoration: "none",
  color: "#768192",
  fontWeight: 600,
};

const fontStyle = {
  fontSize: 15,
};

const fontStyle1 = {
  fontSize: 16,
};
class TaxExemption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestId: "",
      requestDate: "",
      name: "",
      donorId: "",
      phoneNumber: "",
      email: "",
      amount: "",
      statusId: "",
      status: "",
      typeDonation: "",
      receiptDonate: [],
      ispostStatus: "",
      address: "",
      city: "",
      postalCode: "",
      state: "",
      addressShippng: "",
      cityShipping: "",
      postalCodeShipping: "",
      stateShipping: "",
      isPersonal: true,
      taxDetail: false,
      loading: true,
      postStatus: false,
      colorLists: [],
      colorId: "",
      statusColor: "",
      updateStatusDetail: false,
      remark: "",
      trackingNumber: "",
      trackHistory: [],
      postageReceipt: null,
      isApprove: false,
      statusPaymentPostage: false,
      statusPostageColor: "",
      declineModal: false,
      reason: "",
      receipt: null,
      changeStatus: "",
      changeStatusColor: "",
      changeStatusId: "",
      content: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
    this.toggleHover = this.toggleHover.bind(this);
    this.toggleOutHover = this.toggleOutHover.bind(this);
    this.updateStatusDetails = this.updateStatusDetails.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
    this.sendEmail = this.sendEmail.bind(this);
    this.declineTax = this.declineTax.bind(this);
    this.setDeclineModal = this.setDeclineModal.bind(this);
    this.updateStatusPostage = this.updateStatusPostage.bind(this);
    this.handleContent = this.handleContent.bind(this);
  }

  componentDidMount() {
    this.getTaxDetail();
    this.getStatusColor();
  }

  getStatusColor() {
    api.get("/api/statusColor").then((response) => {
      this.setState({
        colorLists: response.data,
      });
    });
  }

  toggleHover = (e) => {
    e.target.style.color = "#3c4b64";
  };

  toggleOutHover = (e) => {
    e.target.style.color = "#768192";
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFile = (event) => {
    this.setState({
      [event.target.name]: event.target.files[0],
    });
  };

  handleContent(content) {
    this.setState({
      content: content,
    });
  }

  updateCurrentStatus() {
    this.setState({
      changeStatusColor: this.state.statusColor,
      changeStatusId: this.state.statusId,
      changeStatus: this.state.status,
    });
  }

  handleChangeStatus(id, status, color) {
    this.setState({
      changeStatusColor: color,
      changeStatusId: id,
      changeStatus: status,
    });
  }

  updateStatusPostage(status, id) {
    const data = {
      status: status,
    };
    api.put("/api/updatePostageStatus/" + id, data).then(() => {
      Swal.fire({
        title: "Done!",
        text: "Status Payment Postade Updated",
        icon: "success",
        timer: 2000,
        showConfirmButton: false,
      }).then(() => {
        this.getTaxDetail();
      });
    });
  }

  setDeclineModal = () => {
    this.setState({
      declineModal: !this.state.declineModal,
    });
  };

  declineTax = () => {
    this.setDeclineModal();
    Swal.fire({
      title: "Loading...",
      showConfirmButton: false,
    });
    const data = {
      id: this.state.requestId,
      email: this.state.email,
      reason: this.state.content,
    };
    this.resetDeclineForm();
    api
      .post("/api/declineTax", data)
      .then((response) => {
        Swal.fire({
          icon: "success",
          title: "Email Sent!",
          text: response.data.message,
          showConfirmButton: true,
          confirmButtonColor: "#225841",
        }).then((result) => {
          if (result.isConfirmed) {
            this.getTaxDetail();
          }
        });
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
        });
      });
  };

  resetDeclineForm() {
    this.setState({
      content: "",
    });
  }

  updateStatus() {
    let timerInterval;
    Swal.fire({
      title: "Loading...",
      didOpen: () => {
        Swal.showLoading();
      },
      willClose: () => {
        clearInterval(timerInterval);
      },
    });
    const data = {
      statusId: this.state.changeStatusId,
      remark: this.state.remark,
    };
    api.put("/api/updateStatusTax/" + this.state.requestId, data).then(() => {
      this.updateStatusDetails();
      Swal.fire({
        title: "Done!",
        text: "Status updated",
        icon: "success",
        timer: 2000,
        showConfirmButton: false,
      }).then(() => {
        this.getTaxDetail();
      });
    });
  }

  updateStatusDetails = () => {
    this.setState({
      updateStatusDetail: !this.state.updateStatusDetail,
    });
    this.updateStatusSetForm();
  };

  updateStatusSetForm = () => {
    this.setState({
      remark: "",
    });
  };

  getTaxDetail() {
    this.setState({
      loading: true,
    });
    api
      .get("/api/taxDetails/" + this.props.match.params.id)
      .then((response) => {
        this.setState(
          {
            taxDetail: true,
            requestId: response.data.tax.requestId,
            requestDate: dateFormat(
              response.data.tax.requestDate,
              "dd/mm/yyyy"
            ),
            name: response.data.tax.name,
            donorId: response.data.tax.donorId,
            phoneNumber: response.data.tax.phoneNumber,
            email: response.data.tax.email,
            amount: parseFloat(response.data.tax.amount).toFixed(2),
            statusId: response.data.tax.statusId,
            statusColor: response.data.tax.statusColor,
            status: response.data.tax.status,
            typeDonation: response.data.tax.typeDonation,
            receiptDonate: response.data.donationReceipt,
            ispostStatus: parseInt(response.data.tax.isPostStatus),
            address: response.data.tax.address,
            city: response.data.tax.city,
            postalCode: response.data.tax.postalCode,
            state: response.data.tax.state,
            trackHistory: response.data.trackHistory,
            trackingNumber: response.data.tax.trackingNumber,
          },
          () => {
            if (this.state.ispostStatus) {
              this.setState({
                postStatus: true,
                addressShippng: response.data.shipping.address,
                cityShipping: response.data.shipping.city,
                postalCodeShipping: response.data.shipping.postalCode,
                stateShipping: response.data.shipping.state,
                statusPaymentPostage:
                  response.data.shipping.statusPaymentPostage,
                statusPostageColor: response.data.shipping.statusPostageColor,
                postageReceipt: response.data.postageReceipt,
              });
              if (response.data.shipping.isApprove) {
                this.setState({
                  isApprove: true,
                });
              }
            }
            this.updateCurrentStatus();
          }
        );
        if (response.data.tax.typeDonation !== "Personal") {
          this.setState({
            isPersonal: false,
          });
        }
        this.setState({
          loading: false,
        });
      })
      .catch((error) => {
        console.log(error.response.data.message);
      });
  }

  sendEmail = () => {
    Swal.fire({
      title: "Loading...",
      showConfirmButton: false,
    });
    let formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("email", this.state.email);
    formData.append("receipt", this.state.receipt);
    formData.append("taxId", this.state.requestId);

    api
      .post("api/sendReceiptTaxEmail", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "Email Sent!",
          html: "An email has been sent",
          showConfirmButton: true,
          confirmButtonColor: "#225841",
        }).then((result) => {
          if (result.isConfirmed) {
            this.clearForm();
            this.getTaxDetail();
          }
        });
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          showConfirmButton: true,
          confirmButtonColor: "#5bc0de",
        });
      });
  };

  clearForm() {
    this.setState({
      receipt: null,
    });

    const file = document.querySelector("#receipt");
    file.value = "";
  }
  render() {
    const {
      requestId,
      requestDate,
      name,
      donorId,
      phoneNumber,
      email,
      amount,
      status,
      statusColor,
      typeDonation,
      postStatus,
      address,
      city,
      postalCode,
      state,
      isPersonal,
      addressShippng,
      cityShipping,
      postalCodeShipping,
      stateShipping,
      remark,
      statusPaymentPostage,
      statusPostageColor,
      postageReceipt,
      updateStatusDetail,
      taxDetail,
      receipt,
      declineModal,
      trackingNumber,
      changeStatus,
      changeStatusColor,
      changeStatusId,
      isApprove,
      trackHistory,
      receiptDonate,
    } = this.state;

    const fieldsReceipt = ["Receipt", "Reference", "Amount"];

    const fieldsHistory = ["Date", "Status", "Remark", "Document"];
    if (this.state.loading === true) return <Loader />;
    if (this.state.loading === false)
      return (
        <div>
          <CRow>
            <CCol sm={8}>
              <CCard>
                <CCardHeader>
                  <h4>Donor Details</h4>
                </CCardHeader>
                <CCardBody>
                  <div style={fontStyle1}>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>{isPersonal ? "Name" : "Company Name"}</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{name}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>
                          {isPersonal
                            ? "Identification Number"
                            : "Company Number"}
                        </strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{donorId}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        {" "}
                        <strong>Phone Number </strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{phoneNumber}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        {" "}
                        <strong>Email</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{email}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        {" "}
                        <strong> Address</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>
                        {" "}
                        {address}, {city}
                      </CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        {" "}
                        <strong> State</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{state}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        {" "}
                        <strong>Postal Code</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{postalCode}</CCol>
                    </CRow>
                  </div>
                </CCardBody>
              </CCard>
              <CCard>
                <CCardHeader>
                  <h4>Send Receipt</h4>
                </CCardHeader>
                <CCardBody>
                  <CCol>
                    <SendEmail
                      email={email}
                      name={name}
                      requestId={requestId}
                      receipt={receipt}
                      sendEmail={this.sendEmail}
                      clearForm={this.clearForm.bind(this)}
                      handleChange={this.handleChange}
                      handleFile={this.handleFile}
                    />
                  </CCol>
                </CCardBody>
              </CCard>
              <CCard className="updateStatusDetails">
                <CCardHeader>
                  <CRow>
                    <CCol>
                      <h5>History Log</h5>
                    </CCol>
                    <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                      <StatusOption
                        updateStatusDetails={this.updateStatusDetails}
                        updateStatusModal={updateStatusDetail}
                        remark={remark}
                        handleChange={this.handleChange}
                        taxDetail={taxDetail}
                        updateStatus={this.updateStatus}
                        changeStatus={changeStatus}
                        changeStatusColor={changeStatusColor}
                        changeStatusId={changeStatusId}
                        handleChangeStatus={this.handleChangeStatus}
                      />
                    </CCol>
                  </CRow>
                </CCardHeader>
                <CCardBody>
                  <CDataTable
                    items={trackHistory}
                    fields={fieldsHistory}
                    itemsPerPage={5}
                    pagination
                    scopedSlots={{
                      Date: (item) => (
                        <td>{dateFormat(item.Date, "dd/mm/yyyy")}</td>
                      ),

                      Document: (item) => (
                        <td>
                          {item.Document !== "-" ? (
                            <CLink
                              href={item.Document}
                              style={LinkStyle}
                              target="_blank"
                              onMouseEnter={this.toggleHover}
                              onMouseLeave={this.toggleOutHover}
                            >
                              <CIcon name="cil-file" className="mfe-2" />
                            </CLink>
                          ) : (
                            item.Document
                          )}
                        </td>
                      ),
                      Remark: (item) => (
                        <td>
                          {item.Remark != null ? (
                            <span
                              dangerouslySetInnerHTML={{ __html: item.Remark }}
                            />
                          ) : (
                            "-"
                          )}
                        </td>
                      ),
                    }}
                  />
                </CCardBody>
              </CCard>
            </CCol>
            <CCol sm={4}>
              <CCard>
                <CCardBody>
                  <div className="px-4 py-2" style={fontStyle}>
                    <CRow>
                      <strong>Request ID:</strong> {requestId}
                    </CRow>
                    <CRow className="py-1">
                      <CBadge color={statusColor}>{status}</CBadge>
                    </CRow>
                    <CRow>
                      <strong>Tracking Number:</strong>
                    </CRow>
                    <CRow className="py-1">{trackingNumber}</CRow>
                    <CRow>
                      <strong>Donation Type:</strong>
                    </CRow>
                    <CRow className="py-1">{typeDonation}</CRow>
                    <CRow>
                      <strong>Total Amount:</strong>
                    </CRow>
                    <CRow className="py-1">RM {amount}</CRow>
                    <CRow>
                      <strong> Request Received At:</strong>
                    </CRow>
                    <CRow className="py-1">{requestDate}</CRow>
                  </div>
                </CCardBody>
              </CCard>
              <CCard>
                <CCardHeader>
                  <h5>Attachement</h5>
                </CCardHeader>
                <CCardBody>
                  <CDataTable
                    items={receiptDonate}
                    fields={fieldsReceipt}
                    itemsPerPage={5}
                    pagination
                    scopedSlots={{
                      Receipt: (item) => (
                        <td>
                          <CButton color="light" size="sm">
                            <CLink
                              href={item.fileUrl}
                              style={LinkStyle}
                              target="_blank"
                              onMouseEnter={this.toggleHover}
                              onMouseLeave={this.toggleOutHover}
                            >
                              <CIcon name="cil-file" className="mfe-2" />
                              {item.Name}
                            </CLink>
                          </CButton>
                        </td>
                      ),
                    }}
                  />
                </CCardBody>
                <CCardFooter>
                  <h5 className="float-right">Total : RM {amount}</h5>
                </CCardFooter>
              </CCard>
              <CCard>
                <CCardHeader>
                  <h5>Postage Details</h5>
                </CCardHeader>
                <CCardBody>
                  <div className="px-4" style={fontStyle}>
                    <PostageDetail
                      postStatus={postStatus}
                      addressShipping={addressShippng}
                      cityShipping={cityShipping}
                      postalCodeShipping={postalCodeShipping}
                      stateShipping={stateShipping}
                      statusPaymentPostage={statusPaymentPostage}
                      statusPostageColor={statusPostageColor}
                      postageReceipt={postageReceipt}
                      LinkStyle={LinkStyle}
                      Enter={this.toggleHover}
                      Leave={this.toggleOutHover}
                      updateStatusPostage={this.updateStatusPostage}
                      requestId={requestId}
                      isApprove={isApprove}
                    />
                  </div>
                </CCardBody>
              </CCard>
              <CCard>
                <CCardBody>
                  <CRow>
                    <CCol sm={5}>
                      <h5>Decline Tax</h5>
                    </CCol>
                    <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                      <CButton
                        size="sm"
                        color="danger"
                        onClick={this.setDeclineModal}
                      >
                        Decline
                      </CButton>
                    </CCol>
                  </CRow>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
          <DeclineModal
            setDeclineModal={this.setDeclineModal}
            declineModal={declineModal}
            email={email}
            handleContent={this.handleContent}
            declineTax={this.declineTax}
          />
        </div>
      );
  }
}

export default TaxExemption;
