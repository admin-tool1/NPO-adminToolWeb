import React, { Component } from "react";
import {
  CCol,
  CRow,
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from ".././../../services/api";
import { freeSet } from "@coreui/icons";
import Swal from "sweetalert2";
import ModalEditUserBasicData from "./ModalEditUserBasicData";
import ModalNewEmail from "./ModalNewEmail";
import ModalShowPassword from "./ModalShowPassword";
import Loader from "../../../containers/Loader";

const userData = JSON.parse(localStorage.getItem("users"));
const name = userData.name;
const email = userData.email;

class UserDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      name: "",
      staffId: "",
      emails: [],
      email: "",
      roleId: "",
      role: "",
      inputName: "",
      inputStaffId: "",
      inputEmail: "",
      modalEditUser: false,
      modalAddEmail: false,
      emailID: "",
      inputAddEmail: "",
      inputAddPassword: "",
      isEdit: false,
      modalEditEmail: false,
      modalShowPassword: false,
      password: "",
      user_name: name,
      user_email: email,
      email_id: null,
      loading: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getUserDetails();
  }

  getUserDetails() {
    this.setState({
      loading: true,
    });
    api.get("/api/userDetails/" + this.state.id).then((response) => {
      this.setState({
        name: response.data.user.name,
        email: response.data.user.email,
        roleId: response.data.user.roleId,
        role: response.data.user.role,
        staffId: response.data.user.staffId,
        emails: response.data.email,
      });
      this.setState({
        loading: false,
      });
    });
  }

  modalEditUserForm() {
    this.setState({
      modalEditUser: !this.state.modalEditUser,
      inputName: this.state.name,
      inputEmail: this.state.email,
      inputStaffId: this.state.staffId,
    });
  }

  modalAddEmailForm() {
    this.setState({
      modalAddEmail: !this.state.modalAddEmail,
      inputAddEmail: "",
      inputAddPassword: "",
    });
  }

  modalEditEmailForm(id, email) {
    this.setState({
      modalEditEmail: !this.state.modalEditEmail,
      isEdit: !this.state.isEdit,
      emailID: id,
      inputAddEmail: email,
      inputAddPassword: "",
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  updateUser() {
    const data = {
      id: this.state.id,
      name: this.state.inputName,
      email: this.state.inputEmail,
      staffId: this.state.inputStaffId,
    };
    api
      .post("/api/updateUser", data)
      .then(() => {
        this.modalEditUserForm();
        Swal.fire({
          timer: 5000,
          icon: "success",
          title: "User Updated!",
          text: "User Successfuly Updated",
          showConfirmButton: false,
        });
        this.getUserDetails();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.error,
          icon: "error",
        });
      });
  }

  addEmail() {
    const data = {
      userId: this.state.id,
      email: this.state.inputAddEmail,
      password: this.state.inputAddPassword,
    };
    api
      .post("/api/addEmail", data)
      .then(() => {
        this.modalAddEmailForm();
        Swal.fire({
          timer: 3000,
          icon: "success",
          title: "Email Added!",
          text: "Email Successfuly Added",
          showConfirmButton: false,
        });
        this.getUserDetails();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.errors,
          icon: "error",
        });
      });
  }

  editEmail() {
    const data = {
      userId: this.state.id,
      emailId: this.state.emailID,
      email: this.state.inputAddEmail,
      password: this.state.inputAddPassword,
    };
    api
      .post("/api/editEmail", data)
      .then((response) => {
        this.modalEditEmailForm();
        Swal.fire({
          timer: 3000,
          icon: "success",
          title: "Email Updated!",
          text: response.data.message,
          showConfirmButton: false,
        });
        this.getUserDetails();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.errors,
          icon: "error",
        });
      });
  }

  deleteEmail(id, email) {
    Swal.fire({
      title: "Are you sure?",
      html: "You will lose these data: <br> <strong>Email:</strong> " + email,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete("/api/deleteEmail/" + id)
          .then(() => {
            Swal.fire({
              title: "Email deleted!",
              text: "Email deleted successfully",
              icon: "success",
              showConfirmButton: false,
              timer: 3000,
            }).then(() => {
              this.getUserDetails();
            });
          })
          .catch((error) => {
            Swal.fire({
              title: "Error",
              text: error,
              icon: "error",
              confirmButtonColor: "#5bc0de",
            });
          });
      }
    });
  }

  modalShowPasswordForm(emailId) {
    this.resetShowPasswordForm();
    this.setState({
      modalShowPassword: !this.state.modalShowPassword,
      email_id: emailId,
    });
  }

  resetShowPasswordForm() {
    this.setState({
      password: "",
      email_id: null,
    });
  }

  showPassword() {
    const data = {
      password: this.state.password,
      emailId: this.state.email_id,
    };
    api
      .post("/api/getPassword", data)
      .then((response) => {
        const id = "#password" + this.state.email_id;
        const showPassword = document.querySelector(id);
        showPassword.innerHTML = response.data.password;
        this.modalShowPasswordForm();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
        });
      });
  }

  render() {
    const {
      name,
      email,
      role,
      staffId,
      inputName,
      inputEmail,
      inputStaffId,
      inputAddEmail,
      inputAddPassword,
      user_name,
      user_email,
      password,
    } = this.state;
    const fields = [
      "No",
      {
        key: "Email",
        _style: { width: "30%" },
      },
      {
        key: "Password",
        _style: { width: "30%" },
      },
      "Action",
    ];
    if (this.state.loading === true) {
      return <Loader />;
    }
    if (this.state.loading === false) {
      return (
        <div>
          <CRow>
            <CCol>
              <CCard>
                <CCardHeader>
                  <CRow>
                    <CCol sm={8}>
                      <h4>User Details</h4>
                    </CCol>
                    <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                      <CButton
                        color="dark"
                        variant="outline"
                        onClick={this.modalEditUserForm.bind(this)}
                      >
                        <CIcon name="cil-pencil" />
                      </CButton>
                    </CCol>
                  </CRow>
                </CCardHeader>
                <CCardBody>
                  <div>
                    <CRow className="py-1">
                      <CCol sm={3}>
                        <strong>Name</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={5}>{name}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={3}>
                        <strong>Role</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={5}>{role}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={3}>
                        <strong>Staff ID</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={5}>{staffId}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={3}>
                        <strong>Email</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={5}>{email}</CCol>
                    </CRow>
                  </div>
                </CCardBody>
              </CCard>
              <CCard>
                <CCardHeader>
                  <CRow>
                    <CCol sm={8}>
                      <h4>Emails</h4>
                    </CCol>
                    <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                      <CButton
                        variant="outline"
                        color="dark"
                        onClick={this.modalAddEmailForm.bind(this)}
                      >
                        <CIcon content={freeSet.cilPlus} />
                        Email
                      </CButton>
                    </CCol>
                  </CRow>
                </CCardHeader>
                <CCardBody>
                  <CDataTable
                    fields={fields}
                    itemsPerPage={10}
                    items={this.state.emails}
                    pagination
                    scopedSlots={{
                      Password: (item) => (
                        <td id={"password" + item.Id}>
                          {/* <span id={"password" + item.Id}> */}
                          {item.Password == null ? "-" : item.Password}
                          {/* </span> */}
                          &nbsp;
                          <CButton
                            style={{ marginLeft: "10px" }}
                            size="sm"
                            color="dark"
                            variant="outline"
                            disabled={item.Password == null}
                            onClick={this.modalShowPasswordForm.bind(
                              this,
                              item.Id
                            )}
                          >
                            Show
                          </CButton>
                        </td>
                      ),
                      Action: (item) => (
                        <td>
                          <CButton
                            color="dark"
                            variant="outline"
                            onClick={this.modalEditEmailForm.bind(
                              this,
                              item.Id,
                              item.Email
                            )}
                          >
                            <CIcon name="cil-pencil" />
                          </CButton>
                          &nbsp;
                          <CButton
                            color="danger"
                            variant="outline"
                            onClick={this.deleteEmail.bind(
                              this,
                              item.Id,
                              item.Email
                            )}
                          >
                            <CIcon name="cil-trash" />
                          </CButton>
                        </td>
                      ),
                    }}
                  />
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>

          <ModalEditUserBasicData
            modalEditUser={this.state.modalEditUser}
            modalEditUserForm={this.modalEditUserForm.bind(this)}
            handleChange={this.handleChange}
            name={inputName}
            email={inputEmail}
            staffId={inputStaffId}
            updateUser={this.updateUser.bind(this)}
          />
          <ModalNewEmail
            modalAddEmail={this.state.modalAddEmail}
            modalAddEmailForm={this.modalAddEmailForm.bind(this)}
            modalEditEmail={this.state.modalEditEmail}
            modalEditEmailForm={this.modalEditEmailForm.bind(this)}
            editEmail={this.editEmail.bind(this)}
            isEdit={this.state.isEdit}
            handleChange={this.handleChange}
            addEmail={this.addEmail.bind(this)}
            inputAddEmail={inputAddEmail}
            inputAddPassword={inputAddPassword}
          />
          <ModalShowPassword
            modal={this.state.modalShowPassword}
            modalForm={this.modalShowPasswordForm.bind(this)}
            handleChange={this.handleChange}
            userName={user_name}
            userEmail={user_email}
            showPassword={this.showPassword.bind(this)}
            password={password}
          />
        </div>
      );
    }
  }
}

export default UserDetails;
