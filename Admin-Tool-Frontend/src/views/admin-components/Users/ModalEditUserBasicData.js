import React, { Component } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";

class ModalEditUserBasicData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <CModal
          alignment="center"
          show={this.props.modalEditUser}
          onClose={this.props.modalEditUserForm}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>Update User</CModalTitle>
          </CModalHeader>
          <CModalBody>
            {" "}
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="name">Name</CLabel>
                  <CInput
                    id="name"
                    name="inputName"
                    value={this.props.name}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="inputEmail">Email</CLabel>
                  <CInput
                    id="inputEmail"
                    name="inputEmail"
                    value={this.props.email}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="staffId">Staff ID</CLabel>
                  <CInput
                    id="staffId"
                    name="inputStaffId"
                    value={this.props.staffId}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.props.modalEditUserForm}>
              Cancel
            </CButton>
            <CButton color="success" onClick={this.props.updateUser}>
              Save
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default ModalEditUserBasicData;
