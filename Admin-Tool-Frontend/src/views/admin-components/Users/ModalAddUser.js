import React, { Component } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";

class ModalAddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <CModal
          alignment="center"
          show={this.props.modal}
          onClose={this.props.modalForm}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>
              {this.props.addUser ? "Add New User" : "Update User"}
            </CModalTitle>
          </CModalHeader>
          <CModalBody>
            {" "}
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="name">Name</CLabel>
                  <CInput
                    id="name"
                    name="name"
                    value={this.props.name}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="email">Email</CLabel>
                  <CInput
                    id="email"
                    name="email"
                    value={this.props.email}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="staffId">Staff ID</CLabel>
                  <CInput
                    id="staffId"
                    name="staffId"
                    value={this.props.staffId}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="roleId">Role</CLabel>
                  <CInput
                    readOnly
                    id="role"
                    name="role"
                    value={this.props.role}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.props.modalForm}>
              Cancel
            </CButton>
            <CButton
              color="success"
              onClick={
                this.props.addUser
                  ? this.props.confirmAdd
                  : this.props.updateData
              }
            >
              {this.props.addUser ? "Add" : "Save"}
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default ModalAddUser;
