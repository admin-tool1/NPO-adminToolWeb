import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CCol,
  CRow,
} from "@coreui/react";
import api from "src/services/api";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import Swal from "sweetalert2";
import ModalAddUser from "./ModalAddUser";

class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      id: "",
      staffId: "",
      deleteModal: false,
      isLoading: false,
      modal: false,
      name: "",
      email: "",
      role: "Staff",
      addUser: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
  }

  componentDidMount() {
    this.loadUsers();
  }

  loadUsers() {
    api.get("/api/allUsers").then((response) => {
      this.setState({
        users: response.data.users,
      });
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  resetForm() {
    this.setState({
      id: "",
      name: "",
      staffId: "",
      email: "",
      role: "Staff",
    });
  }
  detailPage(id) {
    window.location.hash = "/admin/users/user-details/" + id;
  }

  updateData() {
    const data = {
      id: this.state.id,
      name: this.state.name,
      email: this.state.email,
      role: this.state.role,
      staffId: this.state.staffId,
    };
    api
      .post("/api/updateUser", data)
      .then(() => {
        this.setState({
          isLoading: !this.state.isLoading,
        });
        this.modalForm();
        Swal.fire({
          timer: 6500,
          icon: "success",
          title: "User Updated!",
          showConfirmButton: false,
        });
        this.resetForm();
        this.loadUsers();
        this.setState({
          isLoading: !this.state.isLoading,
        });
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.error,
          icon: "error",
        });
      });
  }

  deleteData(id, name, staffId) {
    Swal.fire({
      title: "Are you sure?",
      html:
        "You will lose these data: <br> <strong>Staff ID:</strong> " +
        staffId +
        "<br> <strong>Name:</strong> " +
        name,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete("/api/deleteUser/" + id)
          .then(() => {
            Swal.fire({
              title: "User deleted!",
              text: "The User deleted successfully",
              icon: "success",
              showConfirmButton: false,
              timer: 3000,
            }).then(() => {
              this.loadUsers();
            });
          })
          .catch((error) => {
            Swal.fire({
              title: "Error",
              text: error,
              icon: "error",
              confirmButtonColor: "#5bc0de",
            });
          });
      }
    });
  }

  modalForm(addUser, id, name, email, role, staffId) {
    this.resetForm();
    this.setState({
      addUser: addUser,
      modal: !this.state.modal,
    });
    if (addUser === false) {
      this.setState({
        id: id,
        name: name,
        staffId: staffId,
        email: email,
        role: role,
      });
    }
  }

  confirmAdd() {
    this.setState({
      modal: !this.state.modal,
    });
    Swal.fire({
      title: "Loading...",
      showConfirmButton: false,
    });
    const data = {
      name: this.state.name,
      email: this.state.email,
      role: this.state.role,
      staffId: this.state.staffId,
    };
    api
      .post("/api/addNewUser", data)
      .then((response) => {
        Swal.fire({
          timer: 6500,
          icon: "success",
          title: "User Added!",
          showConfirmButton: false,
        });
        this.resetForm();
        window.location.hash = "/admin/users/user-details/" + response.data.id;
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.errors,
          icon: "error",
        });
      });
  }

  render() {
    const fields = [
      "No",
      "StaffID",
      "Name",
      "Email",
      "Role",
      "RegisteredDate",
      "Action",
    ];

    const { name, email, staffId, role, addUser, isLoading } = this.state;

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>List of Users</h3>{" "}
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton
                  variant="outline"
                  color="dark"
                  onClick={this.modalForm.bind(this, true)}
                >
                  <CIcon content={freeSet.cilPlus} />
                  User
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            <CDataTable
              tableFilter
              items={this.state.users}
              fields={fields}
              loading={isLoading === true}
              bordered
              itemsPerPage={10}
              pagination
              hover
              clickableRows
              onRowClick={(item) => this.detailPage(item.Id)}
              scopedSlots={{
                Action: (item) => (
                  <td onClick={this.disableOnRowClick}>
                    <CButton
                      color="dark"
                      variant="outline"
                      onClick={this.modalForm.bind(
                        this,
                        false,
                        item.Id,
                        item.Name,
                        item.Email,
                        item.Role,
                        item.StaffID
                      )}
                    >
                      <CIcon name="cil-pencil" />
                    </CButton>
                    &nbsp;
                    <CButton
                      color="danger"
                      variant="outline"
                      onClick={this.deleteData.bind(
                        this,
                        item.Id,
                        item.Name,
                        item.StaffID
                      )}
                    >
                      <CIcon name="cil-trash" />
                    </CButton>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCard>
        <ModalAddUser
          modal={this.state.modal}
          modalForm={this.modalForm.bind(this)}
          addUser={addUser}
          name={name}
          email={email}
          staffId={staffId}
          role={role}
          confirmAdd={this.confirmAdd.bind(this)}
          updateData={this.updateData.bind(this)}
          handleChange={this.handleChange}
        />
      </div>
    );
  }
}

export default Users;
