import React, { Component } from "react";
import {
  CRow,
  CCol,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CButton,
  CBadge,
  CTooltip,
} from "@coreui/react";
import api from "src/services/api";
import Swal from "sweetalert2";
import Loader from "src/containers/Loader";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import { FiDollarSign } from "react-icons/fi";

class AssetLog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredLogs: [],
      statusList: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    this.loadFilteredLogs();
    this.loadStatus();
  }

  loadFilteredLogs() {
    this.setState({
      isLoading: true,
    });
    api
      .get("/api/checkLogStatus")
      .then((res) => {
        this.setState({
          filteredLogs: res.data,
          isLoading: false,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  loadStatus() {
    api.get("/api/getAssetStatus").then((res) => {
      this.setState({
        statusList: res.data,
      });
    });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  verificationRequest(action, item) {
    Swal.fire({
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const data = { log_id: item.LogId, verify: action };
    api
      .put("/api/verifyReturned", data)
      .then((res) => {
        Swal.fire({
          title: "Done!",
          icon: "success",
          confirmButtonColor: "#5bc0de",
        }).then(() => {
          this.loadFilteredLogs();
        });
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleVerification(action, item) {
    if (action === false) {
      Swal.fire({
        title: "Are you sure?",
        html: "This action cannot be undone.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, item is missing!",
      }).then((result) => {
        if (result.isConfirmed) {
          this.verificationRequest(action, item);
        }
      });
    } else if (action === true) {
      this.verificationRequest(action, item);
    }
  }

  handleSendNotice(action, item) {
    Swal.fire({
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const data = { log_id: item.LogId, is_charged: action };
    api
      .post("/api/sendPenaltyNotice", data)
      .then(() => {
        Swal.fire({
          title: "Done!",
          icon: "success",
          confirmButtonColor: "#5bc0de",
        }).then(() => {
          this.loadFilteredLogs();
        });
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  render() {
    const { filteredLogs, statusList, isLoading } = this.state;
    const data = [];
    const fields = [
      { key: "LogId" },
      { key: "StaffName" },
      { key: "ItemCode" },
      { key: "DateTaken" },
      //   { key: "QuantityTaken" },
      { key: "DateReturned" },
      // { key: "QuantityReturned" },
      { key: "Status" },
      { key: "Action" },
    ];

    filteredLogs.forEach((log) => {
      let statusName = "";
      let colorName = "";

      statusList.forEach((status) => {
        if (parseInt(status.id) === parseInt(log.status_id)) {
          statusName = status.status;
          colorName = status.status_color.color;
        }
      });

      data.push({
        LogId: log.log_id,
        StaffName: log.staff_name,
        ItemId: log.item_id,
        ItemCode: log.item_code,
        Purpose: log.purpose,
        DateTaken: log.requested_at,
        QuantityTaken: log.requested_qty,
        DateReturned: log.returned_at,
        QuantityReturned: log.returned_qty,
        StatusId: log.status_id,
        Status: statusName,
        StatusColor: colorName,
      });
    });

    return (
      <div className="asset-table">
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>Manage Asset Usage Log</h3>
                <small>
                  Missing item will be deduct from total quantity of asset.
                </small>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {isLoading === true && <Loader />}
            {isLoading === false && (
              <CDataTable
                items={data}
                fields={fields}
                itemsPerPage={10}
                pagination
                // itemsPerPageSelect
                sorter
                hover
                scopedSlots={{
                  DateReturned: (item) => (
                    <td
                      style={
                        item.QuantityReturned == null
                          ? { color: "#db2127" }
                          : { color: "none" }
                      }
                    >
                      {item.DateReturned}
                    </td>
                  ),
                  QuantityReturned: (item) => (
                    <td
                      style={
                        item.QuantityReturned == null
                          ? { color: "#db2127" }
                          : { color: "none" }
                      }
                    >
                      {item.QuantityReturned === null
                        ? "0"
                        : item.QuantityReturned}
                    </td>
                  ),
                  Status: (item) => (
                    <td>
                      <CBadge color={item.StatusColor}>{item.Status}</CBadge>
                    </td>
                  ),
                  Action: (item) => (
                    <td onClick={this.disableOnRowClick.bind(this)}>
                      {parseInt(item.StatusId) === 3 && (
                        <div>
                          <CTooltip content="Verify" placement="top">
                            <CButton
                              color="success"
                              variant="ghost"
                              onClick={this.handleVerification.bind(
                                this,
                                true,
                                item
                              )}
                            >
                              <CIcon content={freeSet.cilCheckAlt} />
                            </CButton>
                          </CTooltip>{" "}
                          &nbsp;
                          <CTooltip content="Mark Missing" placement="top">
                            <CButton
                              color="danger"
                              variant="ghost"
                              onClick={this.handleVerification.bind(
                                this,
                                false,
                                item
                              )}
                            >
                              <CIcon name="cil-x" />
                            </CButton>
                          </CTooltip>
                        </div>
                      )}

                      {parseInt(item.StatusId) === 4 && (
                        <div>
                          <CTooltip content="To Be Charged" placement="top">
                            <CButton
                              color="success"
                              variant="ghost"
                              onClick={this.handleSendNotice.bind(
                                this,
                                true,
                                item
                              )}
                            >
                              <FiDollarSign size={16} />
                            </CButton>
                          </CTooltip>
                          &nbsp;
                          <CTooltip content="No Charges" placement="top">
                            <CButton
                              color="danger"
                              variant="ghost"
                              onClick={this.handleSendNotice.bind(
                                this,
                                false,
                                item
                              )}
                            >
                              <CIcon content={freeSet.cilBan} />
                            </CButton>
                          </CTooltip>
                          &nbsp;
                          <CTooltip content="Item Missing" placement="top">
                            <CButton
                              color="danger"
                              variant="ghost"
                              onClick={this.handleVerification.bind(
                                this,
                                false,
                                item
                              )}
                            >
                              <CIcon content={freeSet.cilFlagAlt} />
                            </CButton>
                          </CTooltip>
                        </div>
                      )}
                      {parseInt(item.StatusId) === 5 && (
                        <div>
                          <CTooltip content="To Be Charged" placement="top">
                            <CButton
                              color="success"
                              variant="ghost"
                              onClick={this.handleSendNotice.bind(
                                this,
                                true,
                                item
                              )}
                            >
                              <FiDollarSign size={16} />
                            </CButton>
                          </CTooltip>
                          &nbsp;
                          <CTooltip content="No Charges" placement="top">
                            <CButton
                              color="danger"
                              variant="ghost"
                              onClick={this.handleSendNotice.bind(
                                this,
                                false,
                                item
                              )}
                            >
                              <CIcon content={freeSet.cilBan} />
                            </CButton>
                          </CTooltip>
                        </div>
                      )}
                    </td>
                  ),
                }}
              />
            )}
          </CCardBody>
        </CCard>{" "}
      </div>
    );
  }
}

export default AssetLog;
