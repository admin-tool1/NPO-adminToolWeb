import React, { Component } from "react";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCol,
  CForm,
  CLabel,
  CSelect,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CFormGroup,
  CInputRadio,
  CInvalidFeedback,
} from "@coreui/react";

class AssetModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <CModal
        show={this.props.data.modal}
        onClose={this.props.setModal.bind(this, null, null)}
      >
        <CModalHeader closeButton>
          <CModalTitle>
            {this.props.data.isAddMode
              ? "New Asset Details"
              : this.props.data.item_code
              ? this.props.data.item_code
              : this.props.data.isUpdateQty
              ? "Update Quantity"
              : "Edit Details"}
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <CInputGroup className="mb-4">
              <CInput
                type="hidden"
                name="item_id"
                value={this.props.data.item_id}
                onChange={this.props.handleInputChange}
              />
            </CInputGroup>
            {this.props.data.isAddMode && (
              <div>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>
                      Type of Asset <span className="required-text">*</span>
                    </CLabel>
                  </CCol>
                  <CCol sm="8" className="p-0">
                    <CSelect
                      custom
                      name="category_id"
                      value={this.props.data.category_id}
                      onChange={this.props.handleChange}
                    >
                      <option value="" disabled>
                        Please select
                      </option>
                      {this.props.data.categories.map((cate) => {
                        let department = "";
                        for (
                          let index = 0;
                          index < this.props.data.departments.length;
                          index++
                        ) {
                          const dept = this.props.data.departments[index];
                          if (parseInt(dept.id) === parseInt(cate.department_id)) {
                            department = dept.department;
                          }
                        }
                        return (
                          <option key={cate.category} value={cate.id}>
                            {department} - {cate.category}
                          </option>
                        );
                      })}
                    </CSelect>
                    <div></div>
                    <span className="error-text" style={{ fontSize: "12px" }}>
                      {this.props.errorList["category_id"] &&
                        "The type of asset field is required."}
                    </span>
                  </CCol>
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>
                      Type of Use <span className="required-text">*</span>
                    </CLabel>
                  </CCol>
                  <CCol sm="8" className="p-0">
                    <CFormGroup variant="custom-radio" inline>
                      <CInputRadio
                        custom
                        id="inline-radio1"
                        name="item_single_use"
                        value="true"
                        checked={this.props.data.item_single_use === "true"}
                        onChange={this.props.handleChange}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="inline-radio1">
                        Single Use
                      </CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-radio" inline>
                      <CInputRadio
                        custom
                        id="inline-radio2"
                        name="item_single_use"
                        // defaultChecked
                        value="false"
                        checked={this.props.data.item_single_use === "false"}
                        onChange={this.props.handleChange}
                      />
                      <CLabel variant="custom-checkbox" htmlFor="inline-radio2">
                        Multiple Use
                      </CLabel>
                    </CFormGroup>
                    <div></div>
                    <span className="error-text" style={{ fontSize: "12px" }}>
                      {this.props.errorList["single_use"] &&
                        "The type of use field is required."}
                    </span>
                  </CCol>
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>
                      Description <span className="required-text">*</span>
                    </CLabel>
                  </CCol>
                  <CCol sm="8" className="p-0">
                    <CInput
                      type="text"
                      name="item_description"
                      value={this.props.data.item_description || ""}
                      onChange={this.props.handleChange}
                      invalid={
                        this.props.errorList["description"] ? true : false
                      }
                    />
                    <CInvalidFeedback>
                      <span className="error-text">
                        {this.props.errorList["description"]}
                      </span>
                    </CInvalidFeedback>
                  </CCol>
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Brand</CLabel>
                  </CCol>
                  <CInput
                    type="text"
                    name="item_brand"
                    value={this.props.data.item_brand || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Unit Price</CLabel>
                  </CCol>
                  <CInputGroupPrepend>
                    <CInputGroupText>RM</CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    type="number"
                    placeholder="0.00"
                    // step=".1"
                    name="item_price"
                    value={this.props.data.item_price || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>
                      Quantity <span className="required-text">*</span>
                    </CLabel>
                  </CCol>
                  <CCol sm="8" className="p-0">
                    <CInput
                      type="number"
                      min={1}
                      max={9999}
                      name="item_quantity"
                      value={this.props.data.item_quantity || ""}
                      onChange={this.props.handleChange}
                      invalid={this.props.errorList["quantity"] ? true : false}
                    />
                    <CInvalidFeedback>
                      <span className="error-text">
                        {this.props.errorList["quantity"]}
                      </span>
                    </CInvalidFeedback>
                  </CCol>
                </CInputGroup>
                {/* <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Minimum Quantity</CLabel>
                  </CCol>
                  <CInput
                    type="number"
                    min={1}
                    max={9999}
                    name="item_min_quantity"
                    value={this.props.data.item_min_quantity}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup> */}
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>
                      Year <span className="required-text">*</span>
                    </CLabel>
                  </CCol>
                  <CCol sm="8" className="p-0">
                    <CInput
                      type="number"
                      min={2010}
                      max={9999}
                      name="item_year"
                      value={this.props.data.item_year || ""}
                      onChange={this.props.handleChange}
                      invalid={
                        this.props.errorList["year"] ? true : false
                      }
                    />
                    <CInvalidFeedback>
                      <span className="error-text">
                        {this.props.errorList["year"]}
                      </span>
                    </CInvalidFeedback>
                  </CCol>
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Location</CLabel>
                  </CCol>
                  <CInput
                    type="text"
                    name="item_location"
                    value={this.props.data.item_location || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Remark</CLabel>
                  </CCol>
                  <CInput
                    type="text"
                    name="item_remark"
                    value={this.props.data.item_remark || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
                {/* <CInputGroup show="false" className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Status</CLabel>
                  </CCol>
                  <CSelect
                    custom
                    id="selectStatus"
                    name="status_id"
                    value={this.props.data.status_id}
                    onChange={this.props.handleChange}
                  >
                    <option value="0">Please select</option>
                    {this.props.data.statuses.map((status) => {
                      return (
                        <option key={status.status} value={status.id}>
                          {status.status}
                        </option>
                      );
                    })}
                  </CSelect>
                </CInputGroup> */}
              </div>
            )}

            {/* EDIT DETAILS */}

            {
              // eslint-disable-next-line
              this.props.data.isEditItem != undefined &&
                !this.props.data.isEditItem &&
                !this.props.data.isUpdateQty && (
                  <div>
                    <CInputGroup className="mb-4">
                      <CCol sm="3" className="p-1">
                        <CLabel>
                          Description <span className="required-text">*</span>
                        </CLabel>
                      </CCol>
                      <CCol sm="8" className="p-0">
                        <CInput
                          type="text"
                          name="edit_description"
                          value={this.props.data.edit_description || ""}
                          onChange={this.props.handleChange}
                          invalid={
                            this.props.errorList["description"] ? true : false
                          }
                        />
                        <CInvalidFeedback>
                          <span className="error-text">
                            {this.props.errorList["description"]}
                          </span>
                        </CInvalidFeedback>
                      </CCol>
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CCol sm="3" className="p-1">
                        <CLabel>Brand</CLabel>
                      </CCol>
                      <CInput
                        type="text"
                        name="edit_brand"
                        value={this.props.data.edit_brand || ""}
                        onChange={this.props.handleChange}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CCol sm="3" className="p-1">
                        <CLabel>Unit Price</CLabel>
                      </CCol>
                      <CInputGroupPrepend>
                        <CInputGroupText>RM</CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="number"
                        placeholder="0.00"
                        name="edit_price"
                        value={this.props.data.edit_price || ""}
                        onChange={this.props.handleChange}
                      />
                    </CInputGroup>
                  </div>
                )
            }

            {/* EDIT ITEM */}

            {this.props.data.isEditItem && (
              <div>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Location</CLabel>
                  </CCol>
                  <CInput
                    type="text"
                    name="item_location"
                    value={this.props.data.item_location || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Remark</CLabel>
                  </CCol>
                  <CInput
                    type="text"
                    name="item_remark"
                    value={this.props.data.item_remark || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>

                <CInputGroup show="false" className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Status</CLabel>
                  </CCol>
                  <CSelect
                    custom
                    id="selectStatus"
                    name="status_id"
                    value={this.props.data.status_id}
                    onChange={this.props.handleChange}
                  >
                    <option value="" disabled>
                      Please select
                    </option>
                    {this.props.data.statuses.map((status) => {
                      return (
                        <option key={status.status} value={status.id}>
                          {status.status}
                        </option>
                      );
                    })}
                  </CSelect>
                </CInputGroup>
              </div>
            )}

            {!this.props.data.isEditItem && this.props.data.isUpdateQty && (
              <div>
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Quantity</CLabel>
                  </CCol>
                  <CInput
                    type="number"
                    min={1}
                    max={9999}
                    name="edit_quantity"
                    value={this.props.data.edit_quantity || ""}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
              </div>
            )}
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton
            color="success"
            onClick={
              this.props.data.isAddMode
                ? this.props.handleAdd
                : this.props.data.isEditItem
                ? this.props.handleUpdate
                : this.props.data.isUpdateQty
                ? this.props.handleUpdateQuantity
                : this.props.handleUpdateDetails
            }
          >
            {this.props.data.isAddMode ? "Add" : "Save"}
          </CButton>{" "}
          <CButton
            color="secondary"
            onClick={this.props.setModal.bind(this, null, null)}
          >
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    );
  }
}

export default AssetModal;
