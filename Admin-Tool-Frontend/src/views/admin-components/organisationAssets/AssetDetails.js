import React, { Component } from "react";
import {
  CRow,
  CCol,
  CBadge,
  CButton,
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from "src/services/api";
import swal from "sweetalert2";
import Loader from "src/containers/Loader";
import { MdFileDownload } from "react-icons/md";
import Modal from "./AssetModal";
import Log from "./AssetLog";

class AssetDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statuses: [],
      items: [],
      items_ids: [],
      colorList: [],
      item_id: "",
      usage_logs: [],
      errorList: [],

      /** ASSET DETAILS */
      detail_id: "",
      category_id: "",
      category_name: "",
      department_id: "",
      department_name: "",
      item_price: "",
      item_quantity: "",
      item_description: "",
      item_brand: "",
      item_single_use: "",

      /** ASSET DETAILS (FOR EDITTING) */
      edit_description: "",
      edit_brand: "",
      edit_price: "",
      edit_quantity: "",

      /** ASSET ITEM (ONLY ONE IS SET TO UPDATE) */
      status_id: "",
      status: "",
      qrcode_id: "",
      qrcode_url: "",
      item_code: "",
      item_year: "",
      item_location: "",
      item_remark: "",

      /** OTHER */
      modal: false,
      isAddMode: false,
      isEditItem: false, // false for details, true for items
      isUpdateQty: false,
      isLoadingDetails: false,
      isLoadingItems: false,
      isDeleteAsset: false,
    };

    this.setModal = this.setModal.bind(this);
    this.handleDownload = this.handleDownload.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleUpdateDetails = this.handleUpdateDetails.bind(this);
    this.handleUpdateQuantity = this.handleUpdateQuantity.bind(this);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);
    this.handleDeleteAsset = this.handleDeleteAsset.bind(this);
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
  }

  componentDidMount() {
    this.loadDetails();
    this.loadItems();
    this.loadStatuses();
    this.loadColor();
  }

  loadItems() {
    this.setState({
      isLoadingItems: true,
    });
    api.get("/api/itemsByDetail/" + this.props.match.params.id).then((res) => {
      let arr_logs = [];
      res.data.forEach((item) => {
        item.asset_log_formatted.forEach((log) => {
          console.log(log);
          arr_logs.push(log);
        });
      });
      this.setState({
        items: res.data,
        usage_logs: arr_logs,
        isLoadingItems: false,
      });

      let arr_id = [];
      res.data.forEach((data) => {
        arr_id.push(data.id);
      });
      this.setState({ items_ids: arr_id });
    });
  }

  loadDetails() {
    this.setState({
      isLoadingDetails: true,
    });
    api.get("/api/assetDetail/" + this.props.match.params.id).then((res) => {
      this.setState({
        detail_id: res.data.id,
        category_id: res.data.asset_category.id,
        category_name: res.data.asset_category.category,
        department_id: res.data.asset_department.id,
        department_name: res.data.asset_department.department,
        item_price: res.data.unit_price,
        item_quantity: res.data.total_quantity,
        item_description: res.data.description,
        item_brand: res.data.brand,
        item_single_use: res.data.single_use,
        isLoadingDetails: false,
      });
    });
  }

  loadStatuses() {
    api.get("/api/getAssetStatus").then((res) => {
      this.setState({
        statuses: res.data,
      });
    });
  }

  loadColor() {
    api.get("/api/statusColor").then((res) => {
      this.setState({
        colorList: res.data,
      });
    });
  }

  setModal(item, action) {
    this.setState((prevState) => ({ modal: !prevState.modal }));
    this.setState({ errorList: [] });
    if (item) {
      this.setState({
        item_id: item.ItemID,
        item_code: item.Code,
        item_location: item.Location,
        item_remark: item.Remark,
        status_id: item.StatusID,
        isEditItem: true,
      });
    } else if (action === "details") {
      this.setState({
        edit_description: this.state.item_description,
        edit_brand: this.state.item_brand,
        edit_price: this.state.item_price,
        isEditItem: false,
        isUpdateQty: false,
      });
    } else if (action === "quantity") {
      this.setState({
        edit_quantity: this.state.item_quantity,
        isEditItem: false,
        isUpdateQty: true,
      });
    } else {
      this.setState({
        item_id: "",
        item_code: "",
        item_location: "",
        item_remark: "",
        status_id: "",
        edit_description: "",
        edit_brand: "",
        edit_price: "",
        edit_quantity: "",
        isEditItem: false,
        isUpdateQty: false,
      });
    }
  }

  handleDownload(id, name) {
    api
      .get("/api/downloadQrcode/" + id, { responseType: "blob" })
      .then((res) => {
        return res.data;
      })
      .then((blob) => {
        const a = document.createElement("a");
        a.href = window.URL.createObjectURL(blob);
        a.download = name + ".png";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleUpdate() {
    swal.fire({
      title: "Updating...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const data = {
      location: this.state.item_location,
      remark: this.state.item_remark,
      status_id: this.state.status_id,
    };
    api
      .put("/api/assetItem/" + this.state.item_id, data)
      .then((res) => {
        if (res.data.status === 200) {
          swal
            .fire({
              title: "Done!",
              text: "Item details updated successfully!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.setModal();
              this.loadItems();
            });
        } else {
          console.log(res.data.errors);
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        this.setModal();
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleUpdateDetails() {
    swal.fire({
      title: "Updating...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const data = {
      description: this.state.edit_description,
      brand: this.state.edit_brand,
      unit_price: this.state.edit_price,
    };
    api
      .put("/api/assetDetail/" + this.state.detail_id, data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          swal
            .fire({
              title: "Done!",
              text: "Asset details updated successfully!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.setModal();
              this.loadDetails();
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        this.setModal();
        console.log(error.response.data); //  {"message":"The given data was invalid.","errors":{"description":["The description field is required."]}}
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleUpdateQuantity() {
    this.setModal();
    swal.fire({
      title: "Updating...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const data = {
      total_quantity: this.state.edit_quantity,
    };
    api
      .put("/api/assetDetail/" + this.state.detail_id, data)
      .then(() => {
        swal
          .fire({
            title: "Done!",
            text: "Asset details updated successfully!",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            this.loadDetails();
            this.loadItems();
          });
      })
      .catch((error) => {
        console.log(error.response);
        swal.fire({
          title: "Error",
          text: error.response.data,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleDeleteItem(id, code) {
    swal
      .fire({
        title: "Are you sure?",
        html: "You won't be able to undo this!<br>Item to delete: " + code,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          api.delete("/api/assetItem/" + id).then(() => {
            swal
              .fire({
                title: "Deleted!",
                text: code + " has been deleted.",
                icon: "success",
                confirmButtonColor: "#5bc0de",
              })
              .then(() => {
                this.loadItems();
                this.loadDetails();
              });
          });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }

  handleDeleteAsset() {
    swal
      .fire({
        title: "Are you sure?",
        html: "You won't be able to revert this action. <br> You will lose every QR codes and usage logs in this asset!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          swal.fire({
            title: "Deleting...",
            showConfirmButton: false,
            didOpen: () => {
              swal.showLoading();
            },
          });
          api
            .delete("/api/assetDetail/" + this.props.match.params.id)
            .then(() => {
              swal
                .fire({
                  title: "Deleted!",
                  text: "Asset has been deleted. You'll be redirect to the Assets main page soon",
                  icon: "success",
                  showConfirmButton: false,
                  timer: 2000,
                })
                .then(() => {
                  this.setState({
                    isDeleteAsset: true,
                  });
                });
            });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  overviewPage() {
    window.location.hash = "/admin/assets";
  }

  render() {
    if (this.state.isDeleteAsset) {
      this.overviewPage();
    }

    const fields = ["Code", "Year", "Location", "Remark", "Status", "Action"];
    const data = [];
    var status_name = "";
    var qrcode_path = "";
    var colorName = "";
    this.state.items.forEach((item) => {
      this.state.statuses.forEach((status) => {
        if (parseInt(status.id) === parseInt(item.status_id)) {
          status_name = status.status;
          colorName = status.status_color.color;
        }
      });

      data.push({
        ItemID: item.id,
        Code: item.code,
        Year: item.year,
        Location: item.location,
        Remark: item.remark,

        StatusID: item.status_id,
        Status: status_name,
        StatusColor: colorName,

        QRCodeID: item.qrcode_id,
        QRCode: qrcode_path,
      });
    });

    return (
      <div>
        <CRow>
          <CCol>
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol sm={3}>
                    <h3>Details</h3>
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <CButton
                      color="dark"
                      variant="outline"
                      onClick={this.setModal.bind(this, null, "details")}
                    >
                      <CIcon name="cil-pencil" /> Edit Details
                    </CButton>
                    &nbsp;
                    <CButton
                      color="dark"
                      variant="outline"
                      onClick={this.setModal.bind(this, null, "quantity")}
                    >
                      Update Quantity
                    </CButton>
                    &nbsp;
                    <CButton
                      color="danger"
                      variant="outline"
                      onClick={this.handleDeleteAsset}
                    >
                      <CIcon name="cil-trash" />
                    </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                {this.state.isLoadingDetails === true && <Loader />}
                {this.state.isLoadingDetails === false && (
                  <div style={{ fontSize: 16 }}>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Department</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{this.state.department_name}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Category</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{this.state.category_name}</CCol>
                    </CRow>

                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Description</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{this.state.item_description}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Quantity</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>{this.state.item_quantity}</CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Unit Price</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>
                        RM{" "}
                        {this.state.item_price == null
                          ? "-"
                          : this.state.item_price}
                      </CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Brand</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>
                        {" "}
                        {this.state.item_brand == null
                          ? "-"
                          : this.state.item_brand}
                      </CCol>
                    </CRow>
                    <CRow className="py-1">
                      <CCol sm={4}>
                        <strong>Type of Use</strong>
                      </CCol>
                      <strong>:</strong>
                      <CCol sm={4}>
                        {" "}
                        {parseInt(this.state.item_single_use) === 1
                          ? "Single Use"
                          : "Multiple Use"}
                      </CCol>
                    </CRow>
                  </div>
                )}
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <CCard>
          <CCardHeader>
            <h3>List of Items</h3>
          </CCardHeader>
          <CCardBody>
            {this.state.isLoadingItems === true && <Loader />}
            {this.state.isLoadingItems === false && (
              <CDataTable
                items={data}
                fields={fields}
                // itemsPerPage={10}
                // pagination
                // itemsPerPageSelect
                sorter
                hover
                scopedSlots={{
                  Action: (item) => (
                    <td onClick={this.disableOnRowClick}>
                      <CButton
                        color="dark"
                        variant="outline"
                        onClick={this.handleDownload.bind(
                          this,
                          item.QRCodeID,
                          item.Code
                        )}
                      >
                        <MdFileDownload />
                      </CButton>{" "}
                      &nbsp;
                      <CButton
                        color="info"
                        variant="outline"
                        onClick={this.setModal.bind(this, item, "item")}
                      >
                        <CIcon name="cil-pencil" />
                      </CButton>{" "}
                      &nbsp;
                      <CButton
                        color="danger"
                        variant="outline"
                        onClick={this.handleDeleteItem.bind(
                          this,
                          item.ItemID,
                          item.Code
                        )}
                      >
                        <CIcon name="cil-trash" />
                      </CButton>
                    </td>
                  ),
                  Location: (item) => (
                    <td>{item.Location != null ? item.Location : "-"}</td>
                  ),
                  Remark: (item) => (
                    <td>{item.Remark != null ? item.Remark : "-"}</td>
                  ),
                  Status: (item) => (
                    <td>
                      <CBadge color={item.StatusColor} className="p-1">
                        {item.Status}
                      </CBadge>
                    </td>
                  ),
                }}
              />
            )}
          </CCardBody>
        </CCard>
        {this.state.items_ids != null && (
          <Log
            items_ids={this.state.items_ids}
            usage_logs={this.state.usage_logs}
          />
        )}

        <Modal
          data={this.state}
          errorList={this.state.errorList}
          setModal={this.setModal}
          handleChange={this.handleChange}
          handleUpdate={this.handleUpdate}
          handleUpdateDetails={this.handleUpdateDetails}
          handleUpdateQuantity={this.handleUpdateQuantity}
        />
      </div>
    );
  }
}

export default AssetDetails;
