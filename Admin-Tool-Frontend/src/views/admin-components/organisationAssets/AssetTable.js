import React, { Component } from "react";
import {
  CRow,
  CCol,
  CBadge,
  CButton,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CModal,
  CModalTitle,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CFormGroup,
  CInputFile,
  CInvalidFeedback,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import { MdFileDownload } from "react-icons/md";
import Loader from "src/containers/Loader";
import api from "src/services/api";
import swal from "sweetalert2";

import Modal from "./AssetModal";

const getBadge = (status) => {
  switch (status) {
    case 1:
      return ["In Stock", "success"];
    case 2:
      return ["Low in Stock", "warning"];
    case 3:
      return ["Out of Stock", "danger"];
    case 4:
      return ["Error!", "info"];
    default:
      return "primary";
  }
};

class AssetTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],
      departments: [],
      categories: [],
      errorImport: [],
      errorList: [],
      errorAlert: [],

      modal: false,
      uploadForm: false,
      isAddMode: true,
      isLoading: true,

      item_id: "",
      category_id: "",
      category_name: "",
      department_id: "",
      status_id: "",
      qrcode_id: "",
      item_code: "",
      item_year: "",
      item_price: "",
      item_quantity: "",
      item_min_quantity: "",
      item_description: "",
      item_brand: "",
      item_location: "",
      item_remark: "",
      item_single_use: "true",

      filePath: null,
    };

    this.loadAssets = this.loadAssets.bind(this);
    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.downloadTemplate = this.downloadTemplate.bind(this);
    this.setUploadForm = this.setUploadForm.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
    this.detailPage = this.detailPage.bind(this);
  }

  componentDidMount() {
    this.updateReorder();
    this.loadAssets();
    this.loadDepartments();
    this.loadCategories();
  }

  loadAssets() {
    this.setState({
      isLoading: true,
    });
    api.get("/api/assetDetail").then((res) => {
      this.setState({
        details: res.data,
        isLoading: false,
      });
    });
  }

  loadDepartments() {
    api.get("/api/getDepartment").then((res) => {
      this.setState({
        departments: res.data,
      });
    });
  }

  loadCategories() {
    api.get("/api/getCategory").then((res) => {
      this.setState({
        categories: res.data,
      });
    });
  }

  updateReorder() {
    api.get("/api/stockIndicator");
  }

  setModal() {
    this.setState((prevState) => ({ modal: !prevState.modal }));
    this.setState({ errorList: [] });

    // action, id, status
    // Edit Asset Modal
    // if (action === false) {
    //   this.setState({ isAddMode: false });
    //   this.setState({ item_id: id });
    //   this.setState({ status_id: status });
    // }
    // // Add Asset Modal
    // else {
    this.setState({
      isAddMode: true,
      item_id: "",
      category_id: "",
      item_single_use: "true",
      item_description: "",
      item_brand: "",
      item_price: "",
      item_quantity: "",
      item_year: "",
      item_location: "",
      item_remark: "",
    });
    // }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleAdd() {
    swal.fire({
      title: "Adding...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let item_single_use = this.state.item_single_use === "true" ? true : false;
    var data = {
      category_id: this.state.category_id,
      unit_price: this.state.item_price,
      description: this.state.item_description,
      year: this.state.item_year,
      quantity: this.state.item_quantity,
      min_quantity: this.state.item_min_quantity,
      brand: this.state.item_brand,
      location: this.state.item_location,
      remark: this.state.item_remark,
      single_use: item_single_use,
      status_id: 1,
    };

    api
      .post("/api/assetItem", data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          swal
            .fire({
              title: "Done!",
              text: "Asset added successfully!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.setModal();
              this.loadAssets();
            });
        } else {
          console.log(res.data.errors);
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  downloadTemplate() {
    api
      .get("/api/exportTemplate", { responseType: "blob" }) //
      .then((res) => {
        return res.data;
      })
      .then((blob) => {
        const a = document.createElement("a");
        a.href = window.URL.createObjectURL(blob);
        a.download = "asset-template.xlsx";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      });
  }

  setUploadForm() {
    this.setState({
      uploadForm: !this.state.uploadForm,
      filePath: null,
      errorImport: [],
      errorAlert: [],
    });
  }

  handleFileUpload() {
    if (!this.state.filePath) {
      let error = [];
      error["file"] = "The file field is required.";
      this.setState({ errorImport: error });
    } else {
      swal.fire({
        title: "Importing...",
        showConfirmButton: false,
      });

      // Create an object of formData
      const formData = new FormData();

      // Update the formData object
      formData.append("file", this.state.filePath, this.state.filePath.name);

      // Details of the uploaded file
      // console.log(formData);;

      // Request made to the backend api
      // Send formData object
      api
        .post("api/importAsset", formData)
        .then((res) => {
          if (res.data.status === 200) {
            swal
              .fire({
                title: "Done!",
                text: res.data.message,
                icon: "success",
                confirmButtonColor: "#5bc0de",
              })
              .then(() => {
                this.setUploadForm();
                this.loadAssets();
              });
          } else {
            swal.close();
            if (res.data.errors["file"]) {
              this.setState({ errorImport: res.data.errors });
            } else {
              this.setState({ errorAlert: res.data.errors });
            }
          }
        })
        .catch((error) => {
          console.log(error);
          swal.fire({
            title: "Error",
            text: error,
            icon: "error",
            confirmButtonColor: "#5bc0de",
          });
        });
    }
  }

  importAlert() {
    if (this.state.errorAlert) {
      return this.state.errorAlert.map((error, index) => (
        <span className="error-text" key={index}>
          Row {error["row"]}: {error["errors"][0]}
          <br />
        </span>
      ));
    }
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  detailPage(item) {
    window.location.hash = window.location.hash + "/details/" + item.DetailID;
  }

  render() {
    const { isLoading } = this.state;
    const fields = [
      "Department",
      "Category",
      "Description",
      "Quantity",
      "ReorderPoint",
      "TypeOfUse",
      "Status",
    ];
    const listOfDetails = [];
    var stockStatus = "";
    var leadTime = 1; // min_quantity per month, therefore lead time in month

    this.state.details.forEach((detail) => {
      var reorderPoint = detail.min_quantity * leadTime;

      if (detail.total_quantity > reorderPoint) {
        stockStatus = 1;
      } else if (
        detail.total_quantity > 0 &&
        detail.total_quantity < reorderPoint
      ) {
        stockStatus = 2;
      } else if (parseInt(detail.total_quantity) === 0) {
        stockStatus = 3;
      } else {
        stockStatus = 4;
      }

      listOfDetails.push({
        DetailID: detail.id,
        Quantity: detail.total_quantity,
        ReorderPoint: reorderPoint, //detail.min_quantity
        UnitPrice: detail.unit_price,
        Description: detail.description,
        Brand: detail.brand,
        TypeOfUse: detail.single_use,
        Status: stockStatus,

        CategoryID: detail.asset_category.id,
        Category: detail.asset_category.category,
        DepartmentID: detail.asset_department.id,
        Department: detail.asset_department.department,
      });
    });

    return (
      <div className="asset-table">
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>List of Assets</h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton
                  color="dark"
                  variant="outline"
                  onClick={this.downloadTemplate}
                >
                  <MdFileDownload /> Template
                </CButton>
                &nbsp;
                <CButton
                  color="dark"
                  variant="outline"
                  onClick={this.setUploadForm}
                >
                  <CIcon content={freeSet.cilCloudUpload} /> Import Assets
                </CButton>
                &nbsp;
                <CButton color="dark" variant="outline" onClick={this.setModal}>
                  <CIcon content={freeSet.cilPlus} /> Asset
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {isLoading === true && <Loader />}
            {isLoading === false && (
              <CDataTable
                items={listOfDetails}
                fields={fields}
                itemsPerPage={20}
                pagination
                itemsPerPageSelect
                sorter
                hover
                clickableRows
                onRowClick={(item) => this.detailPage(item)}
                tableFilter
                scopedSlots={{
                  Quantity: (item) => (
                    <td>
                      {item.Quantity}
                      <span style={{ color: "#db2127" }}>
                        {item.Status === 2
                          ? ` (-${item.ReorderPoint - item.Quantity})`
                          : ""}
                      </span>
                    </td>
                  ),
                  TypeOfUse: (item) => (
                    <td>
                      {parseInt(item.TypeOfUse) === 1 ? "Single Use" : "Multiple Use"}
                    </td>
                  ),
                  Status: (item) => (
                    <td>
                      <CBadge
                        color={getBadge(item.Status)[1]}
                        className="text-center pt-1"
                        style={{ width: "100px", height: "25px" }}
                      >
                        <h6>{getBadge(item.Status)[0]}</h6>
                      </CBadge>
                    </td>
                  ),
                }}
              />
            )}
          </CCardBody>
        </CCard>
        <Modal
          data={this.state}
          errorList={this.state.errorList}
          setModal={this.setModal}
          handleChange={this.handleChange}
          handleAdd={this.handleAdd}
        />
        <CModal
          show={this.state.uploadForm}
          onClose={this.setUploadForm}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>Upload File</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CFormGroup>
              <CInputFile
                type="file"
                accept=".xlsx"
                name="filePath"
                onChange={(event) =>
                  this.setState({
                    filePath: event.target.files[0],
                  })
                }
                onClick={(e) => (e.target.value = null)}
                invalid={this.state.errorImport ? true : false}
              />
              <CInvalidFeedback>
                {this.state.errorImport["file"]}
                {this.importAlert()}
              </CInvalidFeedback>
            </CFormGroup>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              className="uploadButton"
              onClick={this.handleFileUpload}
            >
              Upload
            </CButton>{" "}
            <CButton color="secondary" onClick={this.setUploadForm}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default AssetTable;
