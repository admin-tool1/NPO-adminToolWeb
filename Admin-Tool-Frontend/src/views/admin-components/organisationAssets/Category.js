import React, { Component } from "react";
import {
  CButton,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CRow,
  CCol,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CLabel,
  CSelect,
  CInvalidFeedback,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import Loader from "src/containers/Loader";
import api from "src/services/api";
import swal from "sweetalert2";

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      departments: [],
      showAddModal: false,
      isAddMode: true,
      category_id: "",
      category_name: "",
      category_prefix: "",
      department_id: "",
      isLoading: true,
      department_name: "",
      isPrefixUsed: true,
      errorList: [],
    };

    this.loadCategories = this.loadCategories.bind(this);
    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handlePrefix = this.handlePrefix.bind(this);
  }

  componentDidMount() {
    this.setState({
      departments: this.props.departments,
      department_id: this.props.departmentId,
      department_name: this.props.departmentName,
      categories: this.props.categories,
      isLoading: false,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      departments: nextProps.departments,
      department_id: nextProps.departmentId,
      department_name: nextProps.departmentName,
      categories: nextProps.categories,
    });
  }

  loadCategories() {
    // to make sure reload department after making changes
    this.props.loadDepartments();

    this.setState({
      isLoading: true,
    });
    api.get("/api/categoriesByDept/" + this.state.department_id).then((res) => {
      this.setState({
        categories: res.data,
        isLoading: false,
      });
    });
  }

  setModal(action, id, name, prefix, department_id) {
    this.setState((prevState) => ({ showAddModal: !prevState.showAddModal }));
    var modal = document.getElementById("cateModal");

    // Edit Category Modal
    if (action === false) {
      modal.innerHTML = "Edit Category";
      this.setState({ isAddMode: false });
      this.setState({ category_id: id });
      this.setState({ category_name: name });
      this.setState({ category_prefix: prefix });
      this.setState({ department_id: department_id });
      this.setState({ isPrefixUsed: true });
      this.setState({ errorList: [] });
    }

    // Add New Category Modal
    else {
      modal.innerHTML = "Add   Category";
      this.setState({ isAddMode: true });
      this.setState({ category_id: "" });
      this.setState({ category_name: "" });
      this.setState({ category_prefix: "" });
      // this.setState({ department_id: "" });
      this.setState({ isPrefixUsed: false });
      this.setState({ errorList: [] });
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleAdd() {
    swal.fire({
      title: "Adding...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const data = {
      department_id: this.state.department_id,
      category: this.state.category_name,
      prefix: this.state.category_prefix,
    };

    api
      .post("/api/addCategory", data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          this.setModal();
          swal
            .fire({
              title: "Done!",
              html: "Category <b>" + data.category + " </b>is created!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.loadCategories();
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleDelete(id, name) {
    swal
      .fire({
        title: "Are you sure?",
        html: "You will lose every <b>" + name + "</b> assets!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          swal.fire({
            title: "Deleting...",
            showConfirmButton: false,
            didOpen: () => {
              swal.showLoading();
            },
          });
          api
            .delete("/api/deleteCategory/" + id)
            .then(() => {
              swal
                .fire({
                  html: "Category <b>" + name + "</b> deleted!",
                  icon: "success",
                  confirmButtonColor: "#5bc0de",
                })
                .then(() => {
                  this.loadCategories();
                });
            })
            .catch((error) => {
              console.log(error.data);
              swal.fire({
                title: "Error",
                html: "Please delete assets under <b>" + name + "</b> first.",
                icon: "error",
                confirmButtonColor: "#5bc0de",
              });
            });
        }
      });
  }

  handleUpdate() {
    swal.fire({
      title: "Updating...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const data = {
      category: this.state.category_name,
      prefix: this.state.category_prefix,
      department_id: this.state.department_id,
    };

    api
      .put("/api/updateCategory/" + this.state.category_id, data)
      .then((res) => {
        console.log(res.data);
        swal.close();
        if (res.data.status === 200) {
          this.setModal();
          swal
            .fire({
              title: "Done!",
              text: "Category updated succesfully!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.loadCategories();
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handlePrefix() {
    const data = {
      category: this.state.category_name,
    };
    api.post("/api/categoryAcronym", data).then((res) => {
      this.setState({ category_prefix: res.data });
    });
  }

  render() {
    const { isLoading } = this.state;
    const fields = ["Category", "Code", "Action"];
    const list = [];

    if (this.state.categories) {
      this.state.categories.forEach((category) => {
        list.push({
          CategoryID: category.id,
          Category: category.category,
          Code: category.prefix,
          DepartmentID: category.department_id,
        });
      });
    }

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>List of Categories <small style={{fontSize:12}}>({this.state.department_name})</small></h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton
                  color="dark"
                  variant="outline"
                  onClick={this.setModal.bind(this, true)}
                >
                  <CIcon content={freeSet.cilPlus} /> Category
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {isLoading === true && <Loader />}
            {isLoading === false && (
              <CDataTable
                fields={fields}
                items={list}
                scopedSlots={{
                  Action: (item) => (
                    <td>
                      <CButton
                        color="info"
                        variant="outline"
                        onClick={this.setModal.bind(
                          this,
                          false,
                          item.CategoryID,
                          item.Category,
                          item.Code,
                          item.DepartmentID
                        )}
                      >
                        <CIcon name="cil-pencil" />
                      </CButton>{" "}
                      &nbsp;
                      <CButton
                        color="danger"
                        variant="outline"
                        onClick={this.handleDelete.bind(
                          this,
                          item.CategoryID,
                          item.Category
                        )}
                      >
                        <CIcon name="cil-trash" />
                      </CButton>
                    </td>
                  ),
                }}
              ></CDataTable>
            )}
          </CCardBody>
        </CCard>

        <CModal show={this.state.showAddModal} onClose={this.setModal}>
          <CModalHeader closeButton>
            <CModalTitle id="cateModal">Category Details</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CForm>
              <CInputGroup className="mb-4">
                <CInput
                  type="hidden"
                  name="category_id"
                  value={this.state.category_id}
                />
              </CInputGroup>
              {!this.state.isAddMode && (
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Department</CLabel>
                  </CCol>
                  <CSelect
                    custom
                    name="department_id"
                    value={this.state.department_id}
                    onChange={this.handleChange}
                  >
                    <option value="" disabled>
                      Please select
                    </option>
                    {this.state.departments.map((dept) => {
                      return (
                        <option key={dept.department} value={dept.id}>
                          {dept.department}
                        </option>
                      );
                    })}
                  </CSelect>
                </CInputGroup>
              )}
              <CInputGroup className="mb-4">
                <CCol sm="3" className="p-1">
                  <CLabel>Name</CLabel>
                </CCol>
                <CCol sm="9" className="p-0">
                  <CInput
                    type="text"
                    placeholder="e.g. Buku"
                    name="category_name"
                    value={this.state.category_name}
                    onChange={this.handleChange}
                    invalid={this.state.errorList["category"] ? true : false}
                  />
                  <CInvalidFeedback>
                    <span className="error-text">
                      {this.state.errorList["category"]}
                    </span>
                  </CInvalidFeedback>
                </CCol>
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CCol sm="3" className="p-1">
                  <CLabel>Code</CLabel>
                </CCol>
                <CCol sm="5" className="p-0">
                  <CInput
                    disabled
                    type="text"
                    placeholder="e.g. BUK"
                    name="category_prefix"
                    value={this.state.category_prefix}
                    onChange={this.handleChange}
                    invalid={this.state.errorList["prefix"] ? true : false}
                  />

                  <CInvalidFeedback>
                    <span className="error-text">
                      {this.state.errorList["prefix"]}
                    </span>
                  </CInvalidFeedback>
                </CCol>
                &nbsp;&nbsp;&nbsp;
                {
                  // eslint-disable-next-line eqeqeq
                  this.state.isPrefixUsed == false && (
                    <CButton
                      variant="outline"
                      color="success"
                      onClick={this.handlePrefix}
                    >
                      Generate Code
                    </CButton>
                  )
                }
              </CInputGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              onClick={
                this.state.isAddMode ? this.handleAdd : this.handleUpdate
              }
            >
              {this.state.isAddMode ? "Add" : "Save"}
            </CButton>{" "}
            <CButton color="secondary" onClick={this.setModal}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default Category;
