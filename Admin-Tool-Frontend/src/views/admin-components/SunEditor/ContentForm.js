// Javascript Version

// import React, { useRef, useEffect, Component } from "react";
import React, { Component } from "react";
import SunEditor from "suneditor-react";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File

class ContentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <SunEditor
          onChange={this.props.handleContent}
          setContents={this.props.content}
          setOptions={{
            height: 200,
            buttonList: [
              ["undo", "redo"],
              ["bold", "underline", "italic", "strike"],
              ["outdent", "indent"],
              ["align", "horizontalRule", "list"],
              ["link"],
              ["fullScreen", "codeView"],
              ["preview"],
            ],
          }}
        />
      </div>
    );
  }
}

// const EmailForm = (props) => {
//   /**
//    * @type {React.MutableRefObject<SunEditor>} get type definitions for editor
//    */
//   const editor = useRef();

//   // The sunEditor parameter will be set to the core suneditor instance when this function is called
//   const getSunEditorInstance = (sunEditor) => {
//     editor.current = sunEditor;
//     console.log("editor.current : ", editor.current);
//   };

//   handleChange(content){
//     console.log(content); //Get Content Inside Editor
// }

//   return (
//     <div>
//       <p> My Other Contents </p>
//       <SunEditor getSunEditorInstance={getSunEditorInstance} onChange={handleChange} />
//     </div>
//   );
// };
export default ContentForm;
