import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CInput,
  CInputGroup,
  CInvalidFeedback,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from "@coreui/react";
import React, { Component } from "react";
import api from "src/services/api";
import swal from "sweetalert2";
import Loader from "src/containers/Loader";

class StatusAsset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusList: [],
      colorList: [],
      errorList: [],
      isLoading: false,
      isEdit: false,
      showModal: false,
      statusId: "",
      status: "",
      colorId: "",
      color: "",
      colorDefault: "",
      colorDefaultId: "",
    };

    this.disableOnRowClick = this.disableOnRowClick.bind(this);
    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
  }

  componentDidMount() {
    this.loadStatus();
    this.loadColor();
  }

  loadStatus() {
    this.setState({
      isLoading: true,
    });
    api.get("/api/getAssetStatus").then((res) => {
      this.setState({
        statusList: res.data,
        isLoading: false,
      });
    });
  }

  loadColor() {
    api.get("/api/statusColor").then((res) => {
      this.setState({
        colorList: res.data,
        colorDefault: res.data[0].color,
        colorDefaultId: res.data[0].id,
      });
    });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  setModal(isEdit, item) {
    this.setState((prevState) => ({ showModal: !prevState.showModal }));
    this.setState({ colorId: this.state.colorDefaultId, errorList: [] });
    if (isEdit === true || isEdit === "true") {
      this.setState({
        isEdit: isEdit,
        statusId: item.Id,
        status: item.Status,
        color: item.Color,
        colorId: item.ColorId,
      });
    } else {
      this.setState({
        isEdit: false,
        statusId: "",
        status: "",
      });
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleColorChange(id, color) {
    this.setState({
      colorId: id,
      color: color,
      colorDefault: color,
    });
  }

  handleAdd() {
    swal.fire({
      title: "Adding...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const data = { status: this.state.status, color_id: this.state.colorId };
    api
      .post("/api/addAssetStatus", data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          this.setModal();
          swal
            .fire({
              title: "Done!",
              text: res.data.message,
              icon: "success",
              confirmButtonColor: "#228541",
            })
            .then(() => {
              this.loadStatus();
              this.setState({ status: "", isEdit: false });
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        // this.setModal();
        swal.fire({
          title: "Oops...",
          text: error,
          icon: "error",
          confirmButtonColor: "#3399ff",
        });
      });
  }

  handleUpdate() {
    swal.fire({
      title: "Updating...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const data = { status: this.state.status, color_id: this.state.colorId };
    api
      .put("/api/updateAssetStatus/" + this.state.statusId, data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          this.setModal();
          swal
            .fire({
              title: "Done!",
              text: res.data.message,
              icon: "success",
              confirmButtonColor: "#228541",
            })
            .then(() => {
              this.loadStatus();
              this.setState({ status: "", isEdit: false });
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        this.setModal();
        swal.fire({
          title: "Oops...",
          text: error,
          icon: "error",
          confirmButtonColor: "#3399ff",
        });
      });
  }

  handleDelete(id, status) {
    console.log(id === 1);
    swal
      .fire({
        title: "Are you sure?",
        html: "You won't be able to undo this!<br>Status to delete: " + status,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          swal.fire({
            title: "Deleting...",
            showConfirmButton: false,
            didOpen: () => {
              swal.showLoading();
            },
          });
          api
            .delete("/api/deleteAssetStatus/" + id)
            .then(() => {
              swal
                .fire({
                  title: "Deleted!",
                  text: status + " has been deleted.",
                  icon: "success",
                  confirmButtonColor: "#5bc0de",
                })
                .then(() => {
                  this.loadStatus();
                });
            })
            .catch((error) => {
              swal.fire({
                title: "Error",
                text: error.response.data.errors,
                icon: "error",
              });
            });
        }
      });
  }

  render() {
    const { statusList, isEdit } = this.state;
    const items = [];
    const fields = [
      { key: "No" },
      { key: "Status" },
      { key: "Color" },
      { key: "Action" },
    ];
    let number = 0;
    statusList.forEach((status) => {
      number = number + 1;

      items.push({
        Id: status.id,
        No: number,
        Status: status.status,
        ColorId: status.status_color.id,
        Color: status.status_color.color,
      });
    });

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol>
                <h3>List of Asset Status</h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton
                  color="dark"
                  variant="outline"
                  onClick={this.setModal.bind(this, false, null)}
                >
                  <CIcon content={freeSet.cilPlus} /> Status
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {this.state.isLoading === true && <Loader />}
            {this.state.isLoading === false && (
              <CDataTable
                items={items}
                fields={fields}
                scopedSlots={{
                  Action: (item) => (
                    <td onClick={this.disableOnRowClick}>
                      <CButton
                        color="info"
                        variant="outline"
                        onClick={this.setModal.bind(this, true, item)}
                      >
                        <CIcon name="cil-pencil" />
                      </CButton>{" "}
                      &nbsp;
                      {item.Id > 5 && (
                        <CButton
                          color="danger"
                          variant="outline"
                          onClick={this.handleDelete.bind(
                            this,
                            item.Id,
                            item.Status
                          )}
                        >
                          <CIcon name="cil-trash" />
                        </CButton>
                      )}
                    </td>
                  ),
                  Color: (item) => (
                    <td>
                      {" "}
                      <CBadge color={item.Color}>{item.Color}</CBadge>
                    </td>
                  ),
                }}
              />
            )}
          </CCardBody>
        </CCard>
        <CModal
          show={this.state.showModal}
          onClose={this.setModal.bind(this, false, null)}
        >
          <CModalHeader closeButton>
            <CModalTitle>
              {isEdit ? "Update Asset Status" : "New Asset Status"}
            </CModalTitle>
          </CModalHeader>
          <CModalBody>
            <div className="px-4">
              <CInputGroup className="mb-4">
                <CCol sm="3" className="p-1">
                  <CLabel>Status:</CLabel>
                </CCol>
                <CCol sm="8" className="p-0">
                  <CInput
                    name="status"
                    value={this.state.status}
                    onChange={this.handleChange}
                    invalid={this.state.errorList["status"] ? true : false}
                  />
                  <CInvalidFeedback>
                    <span className="error-text">
                      {this.state.errorList["status"]}
                    </span>
                  </CInvalidFeedback>
                </CCol>
              </CInputGroup>
              <CInputGroup>
                <CCol sm="3" className="p-1">
                  <CLabel>Color:</CLabel>
                </CCol>
                <CDropdown>
                  <CDropdownToggle
                    name="colorStatus"
                    value={
                      isEdit ? this.state.colorId : this.state.colorDefaultId
                    }
                    color={isEdit ? this.state.color : this.state.colorDefault}
                  >
                    {isEdit ? this.state.color : this.state.colorDefault}
                  </CDropdownToggle>

                  <CDropdownMenu>
                    {this.state.colorList.map((item) => (
                      <CDropdownItem
                        value={item.id}
                        key={item.id}
                        onClick={this.handleColorChange.bind(
                          this,
                          item.id,
                          item.color
                        )}
                      >
                        {item.color}
                      </CDropdownItem>
                    ))}
                  </CDropdownMenu>
                </CDropdown>
              </CInputGroup>
            </div>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              onClick={
                isEdit
                  ? this.handleUpdate.bind(this)
                  : this.handleAdd.bind(this)
              }
            >
              {isEdit ? "Save" : "Add"}
            </CButton>
            <CButton
              color="secondary"
              onClick={this.setModal.bind(this, false, null)}
            >
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default StatusAsset;
