import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
  CDropdownToggle,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CInput,
  CButton,
  CBadge,
} from "@coreui/react";
import api from "../../../../services/api";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import Swal from "sweetalert2";

class TaxRequest extends Component {
  constructor() {
    super();
    this.state = {
      statusList: [],
      statusModal: false,
      statusId: "",
      colorId: "",
      statusColor: "",
      status: "",
      colorList: [],
      colorDefault: "",
      colorDefaultId: "",
      isEdit: false,
    };
  }

  componentDidMount() {
    this.getStatus();
    this.getStatusColor();
  }

  getStatusColor() {
    api.get("/api/statusColor").then((response) => {
      this.setState({
        colorList: response.data,
        colorDefault: response.data[0].color,
        colorDefaultId: response.data[0].id,
      });
    });
  }
  getStatus() {
    api.get("api/statusTaxExemptions").then((response) => {
      this.setState({
        statusList: response.data.status,
      });
    });
  }

  changeColor(id, color) {
    this.setState({
      colorId: id,
      statusColor: color,
      colorDefault: color,
    });
  }

  resetForm() {
    this.setState({
      status: "",
      isEdit: false,
    });
  }
  updateData() {
    const data = {
      status: this.state.status,
      colorId: this.state.colorId,
    };
    api
      .put("api/updateStatusTaxRequest/" + this.state.statusId, data)
      .then(() => {
        this.setState({
          statusModal: !this.state.statusModal,
        });
        this.getStatus();
        this.resetForm();
      });
  }

  delete(id) {
    api
      .delete("api/deleteStatusTaxRequest/" + id)
      .then(() => {
        this.getStatus();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error.response.data.error,
          icon: "error",
          confirmButtonColor: "#228541",
        });
      });
  }
  confirmAdd() {
    const data = {
      status: this.state.status,
      colorId: this.state.colorId,
    };
    api.post("/api/addStatusTaxRequest", data).then((response) => {
      this.setState({
        statusModal: !this.state.statusModal,
      });
      this.getStatus();
      this.resetForm();
    });
  }

  statusModal(isEdit, id, status, color, colorId) {
    this.resetForm();
    this.setState({
      statusModal: !this.state.statusModal,
      colorId: this.state.colorDefaultId,
    });
    if (isEdit === true || isEdit === "true") {
      this.setState({
        isEdit: isEdit,
        status: status,
        statusColor: color,
        statusId: id,
        defaultColor: color,
        colorId: colorId,
      });
    }
  }

  render() {
    const { isEdit } = this.state;
    const fields = ["No", "Status", "Color", "Action"];
    return (
      <div>
        <CRow>
          <CCol xl={6}>
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol sm={8}>
                    <h3>List of Tax Request Status</h3>{" "}
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <CButton
                      color="dark"
                      variant="outline"
                      onClick={this.statusModal.bind(this, false)}
                    >
                      <CIcon content={freeSet.cilPlus} />
                      Status
                    </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.statusList}
                  fields={fields}
                  bordered
                  itemsPerPage={10}
                  pagination
                  scopedSlots={{
                    Action: (item) => (
                      <td>
                        <CButton
                          color="info"
                          variant="outline"
                          onClick={this.statusModal.bind(
                            this,
                            true,
                            item.Id,
                            item.Status,
                            item.Color,
                            item.ColorId
                          )}
                        >
                          <CIcon name="cil-pencil" />
                        </CButton>{" "}
                        &nbsp;
                        <CButton
                          color="danger"
                          variant="outline"
                          onClick={this.delete.bind(this, item.Id)}
                        >
                          <CIcon name="cil-trash" />
                        </CButton>
                      </td>
                    ),
                    Color: (item) => (
                      <td>
                        <CBadge color={item.Color}>{item.Color}</CBadge>
                      </td>
                    ),
                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal
          alignment="center"
          show={this.state.statusModal}
          onClose={this.statusModal.bind(this)}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>
              {isEdit
                ? "Update Tax Request Status"
                : "Add New Tax Request Status"}
            </CModalTitle>
          </CModalHeader>
          <CModalBody>
            {" "}
            <div className="px-4">
              <CRow>Status:</CRow>
              <CRow className="py-1">
                <CInput
                  id="status"
                  name="status"
                  value={this.state.status}
                  onChange={(event) =>
                    this.setState({
                      status: event.target.value,
                    })
                  }
                />
              </CRow>
              <CRow>Color:</CRow>
              <CRow className="py-1">
                <CDropdown className="m-1 btn-group">
                  {isEdit ? (
                    <CDropdownToggle
                      name="colorStatus"
                      color={this.state.statusColor}
                    >
                      {this.state.statusColor}
                    </CDropdownToggle>
                  ) : (
                    <CDropdownToggle
                      name="colorStatus"
                      value={this.state.colorDefaultId}
                      color={this.state.colorDefault}
                    >
                      {this.state.colorDefault}
                    </CDropdownToggle>
                  )}

                  <CDropdownMenu>
                    {this.state.colorList.map((item) => (
                      <CDropdownItem
                        value={item.id}
                        key={item.id}
                        onClick={this.changeColor.bind(
                          this,
                          item.id,
                          item.color
                        )}
                      >
                        {item.color}
                      </CDropdownItem>
                    ))}
                  </CDropdownMenu>
                </CDropdown>
              </CRow>
            </div>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.statusModal.bind(this)}>
              Cancel
            </CButton>
            <CButton
              color="success"
              onClick={
                isEdit ? this.updateData.bind(this) : this.confirmAdd.bind(this)
              }
            >
              {isEdit ? "Save" : "Add"}
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default TaxRequest;
