import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CButton,
  CTextarea,
} from "@coreui/react";
import api from "../../../../services/api";
import { freeSet } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import Swal from "sweetalert2";
import FAQForm from "../../SunEditor/ContentForm";

class FaqTaxRequest extends Component {
  constructor() {
    super();
    this.state = {
      FaqList: [],
      Id: "",
      question: "",
      answer: "",
      addModal: false,
      isEdit: false,
      content: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleContent = this.handleContent.bind(this);
  }

  componentDidMount() {
    this.getAllFaqs();
  }

  getAllFaqs() {
    api.get("/api/getAllFaqs").then((response) => {
      this.setState({
        FaqList: response.data.faq,
      });
    });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleContent = (content) => {
    this.setState({
      content: content,
    });
  };

  resetForm() {
    this.setState({
      id: "",
      isEdit: false,
      question: "",
      answer: "",
      content: "",
    });
  }

  addModal(isEdit, id, question, answer) {
    this.resetForm();
    this.setState({
      addModal: !this.state.addModal,
    });
    if (isEdit === true || isEdit === "true") {
      this.setState({
        isEdit: isEdit,
        content: answer,
        question: question,
        Id: id,
      });
    }
  }

  confirmAdd() {
    const data = {
      question: this.state.question,
      answer: this.state.content,
    };
    api
      .post("/api/addFaq", data)
      .then(() => {
        this.addModal();
        Swal.fire({
          icon: "success",
          title: "FAQ Added!",
          text: "FAQ added successfully",
          showConfirmButton: false,
          confirmButtonColor: "#225841",
          timer: 3000,
        });
        this.getAllFaqs();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error,
          icon: "error",
          confirmButtonColor: "#39f",
        });
      });
  }

  updateData() {
    const data = {
      question: this.state.question,
      answer: this.state.content,
    };

    api
      .put("/api/updateFaq/" + this.state.Id, data)
      .then(() => {
        this.addModal();
        Swal.fire({
          icon: "success",
          title: "FAQ updated!",
          text: "FAQ updated successfully",
          showConfirmButton: false,
          confirmButtonColor: "#225841",
          timer: 3000,
        });
        this.getAllFaqs();
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error,
          icon: "error",
          confirmButtonColor: "#39f",
        });
      });
  }

  delete(id, question, answer) {
    Swal.fire({
      title: "Are you sure?",
      html:
        "You will lose these data: <br> <strong>Question:</strong> " +
        question +
        "<br> <strong>Answer:</strong> " +
        answer,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete("/api/deleteFaq/" + id)
          .then(() => {
            Swal.fire({
              title: "FAQ deleted!",
              text: "FAQ deleted successfully",
              icon: "success",
              showConfirmButton: false,
              timer: 3000,
            }).then(() => {
              this.getAllFaqs();
            });
          })
          .catch((error) => {
            Swal.fire({
              title: "Error",
              text: error,
              icon: "error",
              confirmButtonColor: "#5bc0de",
            });
          });
      }
    });
  }

  render() {
    const { isEdit } = this.state;
    const fields = [
      "No",
      {
        key: "Question",
        _style: { width: "25%" },
      },
      {
        key: "Answer",
        _style: { width: "50%" },
      },
      {
        key: "Action",
        _style: { width: "20%" },
      },
    ];
    return (
      <div>
        <CRow>
          <CCol>
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol sm={8}>
                    <h3>List of Tax Request Frequently Ask Questions</h3>
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <CButton
                      color="dark"
                      variant="outline"
                      onClick={this.addModal.bind(this)}
                    >
                      <CIcon content={freeSet.cilPlus} /> FAQ
                    </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.FaqList}
                  fields={fields}
                  bordered
                  itemsPerPage={10}
                  pagination
                  scopedSlots={{
                    Answer: (item) => (
                      <td>
                        <span
                          dangerouslySetInnerHTML={{ __html: item.Answer }}
                        />
                      </td>
                    ),
                    Action: (item) => (
                      <td>
                        <CButton
                          color="info"
                          variant="outline"
                          onClick={this.addModal.bind(
                            this,
                            true,
                            item.Id,
                            item.Question,
                            item.Answer
                          )}
                        >
                          <CIcon name="cil-pencil" />
                        </CButton>{" "}
                        &nbsp;
                        <CButton
                          color="danger"
                          variant="outline"
                          onClick={this.delete.bind(
                            this,
                            item.Id,
                            item.Question,
                            item.Answer
                          )}
                        >
                          <CIcon name="cil-trash" />
                        </CButton>
                      </td>
                    ),
                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal
          alignment="center"
          show={this.state.addModal}
          onClose={this.addModal.bind(this)}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>{isEdit ? "Update FAQ" : "Add New FAQ"}</CModalTitle>
          </CModalHeader>
          <CModalBody>
            {" "}
            <div className="px-4">
              <CRow>Question:</CRow>
              <CRow className="py-1">
                <CTextarea
                  id="question"
                  name="question"
                  value={this.state.question}
                  onChange={this.handleChange}
                  rows="4"
                />
              </CRow>
              <CRow>Answer:</CRow>
              <CRow className="py-1">
                <FAQForm
                  handleContent={this.handleContent}
                  content={this.state.content}
                />
              </CRow>
            </div>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.addModal.bind(this)}>
              Cancel
            </CButton>
            <CButton
              color="success"
              onClick={
                isEdit ? this.updateData.bind(this) : this.confirmAdd.bind(this)
              }
            >
              {isEdit ? "Save" : "Add"}
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default FaqTaxRequest;
