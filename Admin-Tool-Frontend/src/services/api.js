import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:8000/", //https://api.csm.nadofficial.com https://api.admin.cintasyriamalaysia.com  http://localhost:8000
  withCredentials: true,
  headers: {
    Authorization: "Bearer " + localStorage.getItem("token"),
  },
});

export default apiClient;
