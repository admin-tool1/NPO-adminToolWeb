const _nav = [
  {
    _tag: "CSidebarNavItem",
    name: "Frequently Asked Questions",
    to: "/tax/frequently-asked-questions",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Terms & Conditions",
    to: "/tax/terms-and-conditions",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Request Tax Exemption",
    to: "/tax/request-tax-exemption",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Track Tax Exemption",
    to: "/tax/tracking",
  },
];

export default _nav;
