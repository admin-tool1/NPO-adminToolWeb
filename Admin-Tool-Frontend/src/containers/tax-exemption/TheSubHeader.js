import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { CHeader, CToggler, CBreadcrumbRouter } from "@coreui/react";

// routes config
import routes from "../../routes-taxExemptions";

const TheSubHeader = () => {
  const dispatch = useDispatch();
  const sidebarShow = useSelector((state) => state.sidebarShow);

  const toggleSidebarMobile = () => {
    const val = [false, "responsive"].includes(sidebarShow)
      ? true
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />{" "}
      <CBreadcrumbRouter
        className="border-0 c-subheader-nav m-0 px-0 px-md-3"
        routes={routes}
      />
    </CHeader>
  );
};

export default TheSubHeader;
