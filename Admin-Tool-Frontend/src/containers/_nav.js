import React from "react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import Roles from "../services/Roles";

const _nav = [
  /** ADMIN NAV */
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/admin/dashboard",
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon" />,
    // badge: {
    //   color: "info",
    //   text: "NEW",
    // },
    permission: [Roles.ADMIN],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Tax Exemption",
    to: "/admin/tax-exemptions",
    icon: <CIcon name="cil-book" customClasses="c-sidebar-nav-icon" />,
    permission: [Roles.ADMIN],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Assets",
    to: "/admin/assets",
    icon: <CIcon name="cil-tags" customClasses="c-sidebar-nav-icon" />,
    permission: [Roles.ADMIN],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Users",
    to: "/admin/users",
    icon: "cil-user",
    permission: [Roles.ADMIN],
  },
  /** END NAV */

  /** STAFF NAV */
  {
    _tag: "CSidebarNavTitle",
    _children: ["Assets"],
    permission: [Roles.STAFF],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Request Asset",
    to: "/staff/request-asset",
    icon: (
      <CIcon content={freeSet.cilTouchApp} customClasses="c-sidebar-nav-icon" />
    ),
    permission: [Roles.STAFF],
  },
  {
    _tag: "CSidebarNavItem",
    name: "Return Assets",
    to: "/staff/return-asset",
    icon: (
      <CIcon content={freeSet.cilTask} customClasses="c-sidebar-nav-icon" />
    ),
    permission: [Roles.STAFF],
  },
  {
    _tag: "CSidebarNavItem",
    name: "History",
    to: "/staff/request-history",
    icon: (
      <CIcon content={freeSet.cilClock} customClasses="c-sidebar-nav-icon" />
    ),
    permission: [Roles.STAFF],
  },
  {
    _tag: "CSidebarNavTitle",
    _children: ["Extras"],
    permission: [Roles.ADMIN],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "Donor Analysis",
    icon: "cib-google-analytics",
    permission: [Roles.ADMIN],
    _children: [
      {
        _tag: "CSidebarNavItem",
        name: "Statistic",
        to: "/admin/donor-statistic",
        permission: [Roles.ADMIN],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Donor Ranking",
        to: "/admin/donor-analysis",
        permission: [Roles.ADMIN],
      },
      {
        _tag: "CSidebarNavItem",
        name: "Broadcast Log",
        to: "/admin/broadcast-log",
        permission: [Roles.ADMIN],
      },
    ],
  },
  {
    _tag: "CSidebarNavTitle",
    _children: ["Configurations"],
    permission: [Roles.ADMIN],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "System Settings",
    route: "/admin/Settigs",
    icon: "cil-settings",
    permission: [Roles.ADMIN],
    _children: [
      {
        _tag: "CSidebarNavDropdown",
        name: "Tax Exemption",
        permission: [Roles.ADMIN],
        _children: [
          {
            _tag: "CSidebarNavItem",
            name: "Status",
            to: "/admin/tax-request-status",
            permission: [Roles.ADMIN],
          },
          {
            _tag: "CSidebarNavItem",
            name: "Frequently Ask Question (FAQ)",
            to: "/admin/tax-request-FAQ",
            permission: [Roles.ADMIN],
          },
          {
            _tag: "CSidebarNavItem",
            name: "Terms and Conditions",
            to: "/admin/tax-request-terms",
            permission: [Roles.ADMIN],
          },
        ],
      },
      {
        _tag: "CSidebarNavDropdown",
        name: "Assets",
        permission: [Roles.ADMIN],
        _children: [
          {
            _tag: "CSidebarNavItem",
            name: "Attributes",
            to: "/admin/asset-attributes",
            permission: [Roles.ADMIN],
          },
          {
            _tag: "CSidebarNavItem",
            name: "Status",
            to: "/admin/asset-status",
            permission: [Roles.ADMIN],
          },
        ],
      },
    ],
  },
];

export default _nav;
