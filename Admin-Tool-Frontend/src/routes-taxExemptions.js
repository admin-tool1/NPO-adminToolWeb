import React from "react";

//tax-exemption-donor-page
const TaxFAQs = React.lazy(() =>
  import(
    "./views/donor-components/FrequentlyAskedQuestions/FrequentlyAskedQuestions"
  )
);
const TaxRequest = React.lazy(() =>
  import("./views/donor-components/RequestTaxExemption/RequestTaxExemptionForm")
);

const TaxTerms = React.lazy(() =>
  import("./views/donor-components/TermsConditions/TermsConditions")
);
const TaxTracking = React.lazy(() =>
  import("./views/donor-components/TrackTaxExemption/TrackTaxExemption")
);

const routes = [
  {
    path: "/tax/frequently-asked-questions",
    name: "Frequently Asked Questions",
    component: TaxFAQs,
  },
  {
    path: "/tax/request-tax-exemption",
    name: "Request Tax Exemption",
    component: TaxRequest,
  },
  {
    path: "/tax/terms-and-conditions",
    name: "Terms and Conditions",
    component: TaxTerms,
  },
  {
    path: "/tax/tracking",
    name: "Track Tax Exemption",
    component: TaxTracking,
  },
  {
    path: "/tax/track-tax-exemption/:id",
    name: "Track Tax Exemption",
    component: TaxTracking,
  },
  /** END ROUTES */
];

export default routes;
